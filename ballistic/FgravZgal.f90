!!****f* setups/rps_galaxy/FgravZgal
!!
!! NAME FgravZgal
!!
!!  
!!
!!
!! SYNOPSIS
!!
!!  FgravZgal()
!!
!!
!! DESCRIPTION
!!
!E         For given galactic coordinates Rgal,Zgal calculate the gravitational
!E         acceleration in GALACTIC z-direction.
!E         Also galactic parameters need to be given.
!E         All quantities are given in cgs-units.
!!
!E         First the contributions of the single components are calculated,
!E         then they are added.
!!
!E         The contributions are:
!E              - a Plummer-Kuzmin stellar disk
!E              - a spherical Hernquist bulge
!E              - a Burkert-DM halo.
!E         All have analytical potentials and hence analytical 
!E         gravitational forces.
!! 
!! ARGUMENTS
!!
!E  R/Zgal      GALACTIC cylindrical coordinates, where galaxy is centred at
!E                (0,0).
!E  starmass    total stellar mass
!E  a/bstar     radial and vertical scale lengths of the stellar disk
!E  rho0DM      central density for the dark matter
!E  r0DM        scale radius for DM
!E  blgmass     total bulge mass
!E  r0Blg       scale radius of the bulge
!E  dx          resolution of the grid. Needed to set central values.
!!
!!
!E VARIABLES
!!
!E  G           gravitational constant in cgs
!E  pi          guess what that is
!E  radius      distance to the galactic center
!E   further variables are shortcuts.
!!  
!!***


function FgravZgal(Rgal,Zgal,starmass,astar,bstar,rho0DM,r0DM,blgmass,r0Blg,dx)


  implicit none


!  real,intent(in)  ::  Rgal,Zgal,starmass,astar,bstar,rho0DM,r0DM,&
!                       blgmass,r0Blg,dx
!  real,intent(out) ::  FgravZgal

  real*8  ::  Rgal,Zgal,starmass,astar,bstar,rho0DM,r0DM,&
                       blgmass,r0Blg,dx
  real*8 ::  FgravZgal

  real*8,parameter   :: G = 6.6725985000000E-08
  real*8,parameter   :: pi = 3.141592653589793238512808959406186204433
  real*8             :: Rgal2,Zgal2,root
  real*8              :: contrib_star,contrib_DM, contrib_blg
  real*8              :: radius

!E ***stellar PK-disk, calculated in substeps for clarity***

  Rgal2=(Rgal/astar)**2
  Zgal2=(Zgal/astar)**2

  root = sqrt( Zgal2+(bstar/astar)**2 )

  contrib_star = (Zgal/astar) * (1. + root ) / root

  contrib_star =  contrib_star/ ( ( Rgal2 + (1.+root)**2 )**1.5 )

  contrib_star = - G*starmass * contrib_star / astar**2



!E ***for DM halo**

  radius = sqrt(Rgal**2 + ZGal**2)

  contrib_DM =-2.*( -r0DM*atan(radius/r0DM)/radius**2 + &
                     (1.+r0DM/radius)*r0DM/( r0DM**2+radius**2) )&
             + 2.*( -r0DM*log(1.+radius/r0DM)/radius**2 + &
                     (1.+r0DM/radius)/ (r0DM+radius)  )&
             -    (  r0DM*log(1.+(radius/r0DM)**2)/radius**2 + &
                     (1.-r0DM/radius)*2.*radius/( r0DM**2 +radius**2 ))

  contrib_DM = contrib_DM * pi * G * rho0DM * r0DM**2 * Zgal / radius
  if (radius < dx ) contrib_DM = 0.


!E ***for bulge***

  contrib_blg = -G*blgmass*Zgal/( radius * (radius + r0Blg)**2 )
  if (radius < dx ) contrib_blg = 0.


  FgravZgal = contrib_star + contrib_DM + contrib_blg
  
  return
end function FgravZgal
