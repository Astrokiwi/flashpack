program ballistic_main
	use accel, only : calc_accel,load_grav_default,escape_vel
!	use accel
	implicit none
	
!	real*8 :: escape_vel
	real*8, dimension(3) :: r,v,a
	real*8 :: dt,time,max_time
	integer :: itime,max_it
	integer :: ofile,ofile2
	integer :: print_updates
	character(len=80) :: arg
	character(len=256) :: infile_name,outfile_name
	integer :: irun,idump,iclump_in,iclump,iclump_max
	logical :: looping_clumps
	integer :: dummy,stat
	
	if ( iargc()==0 ) then
		print *,"Need to specify an input run number"
		stop
	endif
	if ( iargc()>3 ) then
		print *,"Too many arguments! You probably made a mistake."
		print *,"Enter run number and dump number alone to predict positions for all clumps."
		print *,"Enter run number followed by clump number to predict positions for that clump clump."
		print *,"Please try again."
		stop
	endif
	
	call getarg(1,arg)
	read(arg,'(i)') irun
	call getarg(2,arg)
	read(arg,'(i)') idump
	if ( iargc()==3 ) then
		call getarg(3,arg)
		read(arg,'(i)') iclump_in
		iclump = iclump_in
	else
		iclump_in=-1
		iclump = 0
	endif

	! Set up graviational potential
	call load_grav_default
	
	! Read in clump data
	write(infile_name,"('../flashgalquickview/clump_data/clump_prop_',I4.4,'_',I4.4,'.dat')") irun,idump
	
	print *,"Reading from ",infile_name
	
	if ( iclump_in>-1 ) then
		open(unit=42,file=infile_name)
		do iclump=0,iclump_in-1
			read(42,*) dummy,r,v
		end do
		close(42)
		print *,"Read complete:"
		print *,"r=",r
		print *,"v=",v
	else
		print *,"Trajectories for *all* clumps!"
		iclump_max = 0
!		write(outfile_name,"('escape_vel',I4.4,'_',I4.4,'.dat')") irun,idump
!		open(unit=43,file=outfile_name)
!		do iclump=1,200
!			r = iclump * 3.08568025d18*.5*1.d3
!		    write(43,*) dummy,sqrt(r(1)**2+r(2)**2+r(3)**2)/3.08568025e18,escape_vel(r)/1.d5
!		end do
!		close(43)
!		stop
		write(outfile_name,"('escape_vel',I4.4,'_',I4.4,'.dat')") irun,idump
		open(unit=43,file=outfile_name)
		open(unit=42,file=infile_name)
		do
		   read(42, *,iostat=stat) dummy,r,v
		   r = r * 3.08568025e18 ! cm to pc
!		   print *,dummy,stat
		   if (stat /= 0) exit
		   write(43,*) dummy,sqrt(v(1)**2+v(2)**2+v(3)**2),escape_vel(r)/1.d5
!		   write(43,*) dummy,sqrt(r(1)**2+r(2)**2+r(3)**2)/3.08568025e18,escape_vel(r)/1.d5
		   iclump_max = iclump_max + 1
		end do
		close(42)
		close(43)
		open(unit=42,file=infile_name)
		iclump = 0
	endif
	
!	print *,"Initialising trajectory of clump ",iclump
!	call initialise(r,v)

	ofile = 69
	write(outfile_name,"('trajectory',I4.4,'_',I4.4,'.dat')") irun,idump
	open(unit=ofile,file=outfile_name)
	
	ofile2 = 56
	write(outfile_name,"('resting_places',I4.4,'_',I4.4,'.dat')") irun,idump
	open(unit=ofile2,file=outfile_name)
	looping_clumps = .true.
	do while (looping_clumps)
		if ( iclump_in==-1 ) then
			read(42,*) dummy,r,v
			print *,"Clump NO",dummy
		endif
		a = 0.
		v = v * 1.e5 ! convert from km/s to cm/s
		r = r * 3.08568025e18 ! parsec to cm
		
		
		time = 0.d0
		itime = 0
		max_time = 3.15d7*1.d8 ! For big gals
!		max_time = 3.15d7*1.d5 ! For small gals
	!	max_it=1e
		dt = 3.15d7*1.e4 ! For big gals
!		dt = 3.15d7*1.d4
		print_updates = 0
!		print *,"Starting integration"
		
		call calc_accel(r,a)
	!	do while (time<max_time .and. itime<max_it)
		do while (time<max_time)
			call integrate(r,v,a,dt)
			
			time = time + dt
			itime = itime + 1
			
			if ( time>max_time/100*print_updates ) then
				print *,itime,time,max_time
				print_updates = print_updates + 1
				write(ofile,"(11E15.6)") time,r,v,a,dt
			endif
		end do
	
		
		if ( time>=max_time ) then
			print *,"Reached ",max_time," seconds"
		endif
		write(ofile2,"(11E15.6)") time,r,v,a,dt
	!	if ( itime>=max_it ) then
	!		print *,"Reached ",max_it," iterations"
	!	endif
		if ( iclump_in==-1 ) then
			iclump = iclump + 1
			if ( iclump==iclump_max ) then
				looping_clumps = .false.
			endif
		else
			looping_clumps = .false.
		endif
	end do
	close(ofile)
	close(ofile2)
	close(42)

end program ballistic_main
