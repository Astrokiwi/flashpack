	subroutine initialise(r,v)
		use accel, only : load_grav_default
		implicit none
		real*8, dimension(3) :: r,v
		
		r = (/3.d18*40.,3.d18*100.,3.d18*100./)
!		v = (/300.e5,1000.e5,300.e5/)
		v = (/1.e7,2.e7,1.e7/)
!		v = (/0.,0.,0./)
		
		call load_grav_default
		
		return
	end subroutine initialise
