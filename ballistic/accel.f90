module accel
	implicit none
!	save
	
	real*8, save :: starmas,starrl,starzl,DMrho0,DMr0,Blgmas,Blgr0,dummy_dx
!	real*8 :: escape_vel
	public :: calc_accel, load_grav_default, escape_vel
!	public :: load_grav
!	private :: load
!	public

!	integer, save :: nnr,nnz
!	real, save, allocatable :: raccel(:,:),zaccel(:,:) ! Accelerations on cylindrical grid
!	real, save, allocatable :: rg(:),zg(:) ! Grid r,z

	contains
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		function escape_vel(r)
			real*8, dimension(3) :: r
			real*8 :: escape_vel
			real*8,parameter   :: G = 6.6725985000000E-08
			real*8,parameter   :: pi = 3.141592653589793238512808959406186204433
			
			real*8 :: phi
			real*8 :: rad_sph,rad_cyl,zed
			
			zed = r(2)
			rad_cyl = sqrt(r(1)**2+r(3)**2)
			rad_sph = sqrt(r(1)**2+r(2)**2+r(3)**2)
			
			phi = 0.d0
			! DM potential first
			phi = -pi*G*Dmrho0*DMr0**2 * &
				(	-2 * (1+DMr0/rad_sph)*atan(DMr0/rad_sph)			&
					+2 * (1+DMr0/rad_sph)*log(1+rad_sph/DMr0)			&
					-    (1-DMr0/rad_sph)*log(1+(rad_sph/DMr0)**2)		&
					+ pi )
			
			! Then the disc potential
			phi =	phi + G * starmas / 	&
					sqrt(rad_cyl**2 + (starrl+sqrt(zed**2+starzl**2))**2 )
			
!			print *,rad_cyl,starrl,zed,starzl
			
			escape_vel = sqrt(2.*phi)
!			escape_vel = phi
			return
		end function escape_vel
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		subroutine calc_accel(r,a)
			real*8 :: FgravZgal,FgravRgal
			real*8, dimension(3) :: r,a
			real*8 :: rad,zed
			real*8 :: radgrav,zedgrav
			integer :: irad,ized
			
			zed = abs(r(2))
			rad = sqrt(r(1)**2+r(3)**2)
			zedgrav = FgravZgal(rad,zed,starmas,starrl,starzl,DMrho0,DMr0,Blgmas,Blgr0,dummy_dx)
			radgrav = FgravRgal(rad,zed,starmas,starrl,starzl,DMrho0,DMr0,Blgmas,Blgr0,dummy_dx)

			a(2) = zedgrav*sign(1.,r(2))
			
			a(1) = (rad/r(1)) * radgrav
			a(3) = (rad/r(3)) * radgrav
			
			return
		end subroutine calc_accel

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		subroutine load_grav_default()
			real*8 :: dmmas
			real*8, parameter ::	parsec = 3.08568025e18,&
									sunmas = 1.9889225e33
		
!			! Small galaxy parameters
			starmas          = 1.0e10 ! Stellar mass etc - not dynamic, only used for gravity
			starrl           = 7.0e2
			starzl           = 1. ! Thin disc approximation... sorta
			dmr0 = 326.e3 ! DMr0 in code, dark matter scale radius - sets dmrho and t_ambient. (Setting dmmas is optional)
			dmmas = 5.e12 ! Mass of DM halo
			blgmas = 0.0 ! Bulge mass, only used in gravity
			dummy_dx = 6.17e20/8/8/8 ! Low res cutoff
			blgr0 = 1. ! Not important
			print *,"Small galaxy parameters!"

			! Large galaxy parameters
!			starmas          = 4.0e10 ! Stellar mass etc - not dynamic, only used for gravity
!			starrl           = 16.0e3
!			starzl           = 4.0e3
!			dmr0 = 100.e3 ! DMr0 in code, dark matter scale radius - sets dmrho and t_ambient. (Setting dmmas is optional)
!			dmmas = 3.e12 ! Mass of DM halo
!			blgmas = 0.0 ! Bulge mass, only used in gravity
!			dummy_dx = 3.69e23*2./8/8/8 ! Low res cutoff
!			blgr0 = 1. ! Not important
!			print *,"Large galaxy parameters!"

	        DMmas=DMmas*sunmas
	        DMrho0=DMmas / (1.6 *1.e9*parsec**3 * DMr0**3)
		    DMr0=DMr0*parsec
		    starmas=starmas*sunmas
		    starrl=starrl*parsec
		    starzl=starzl*parsec
		    Blgmas=Blgmas*sunmas
		    Blgr0=Blgr0*parsec
		end subroutine load_grav_default

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!		subroutine calc_accel_table(r,a)
!			real*8, dimension(3) :: r,a
			
!			real*8 :: rad,zed
!			real*8 :: radgrav,zedgrav
!			integer :: irad,ized
			
!			! 3D SHM
!!			a = -r
!			zed = abs(r(2))
!			rad = sqrt(r(1)**2+r(3)**2)
			
!			irad = 1
!			do while (irad<nnr .and. rg(irad)<rad)
!				irad = irad + 1
!			end do
!			ized = 1
!			do while (ized<nnz .and. zg(ized)<zed)
!				ized = ized + 1
!			end do
			
!			! Replace with interpolation
!			radgrav = raccel(irad,ized)
!			zedgrav = zaccel(irad,ized)
			
!			a(2) = zedgrav*sign(1.,r(2))
			
!			a(1) = (rad/r(1)) * radgrav
!			a(3) = (rad/r(3)) * radgrav
			
!			return
!		end subroutine calc_accel
		
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!		subroutine load_grav_default
!			nnr = 1000
!			nnz = 1000
			
!			call load
			
!		end subroutine load_grav_default
		
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!		subroutine load_grav(nnr_in,nnz_in)
!			integer :: nnr_in,nnz_in
			
!			nnr = nnr_in
!			nnz = nnz_in
			
!			call load
			
!		end subroutine load_grav

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
!		subroutine load
!			integer :: jr,jz
!			real :: dum
			
!			print *,"Loading grav tables"
			
!			allocate(raccel(nnr,nnz))
!			allocate(zaccel(nnr,nnz))
!			allocate(rg(nnr))
!			allocate(zg(nnz))
			
!			open(unit=50,file="smallgalR.tab")
!				do jr=1,nnr
!					do jz=1,nnz
!						read(50,*) rg(jr),zg(jz),raccel(jr,jz)
!					end do
!				end do
!			close(50)
!			open(unit=50,file="smallgalZ.tab")
!				do jr=1,nnr
!					do jz=1,nnz
!						read(50,*) dum,dum,zaccel(jr,jz)
!					end do
!				end do
!			close(50)
			

!			print *,"Grav tables loaded"
!		end subroutine load

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

end module accel
