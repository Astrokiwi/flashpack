! Requires a to be updated before running the first time
! Will update a from then onwards
	subroutine integrate(r,v,a,dt)
		use accel, only : calc_accel
		implicit none
		
		real, parameter :: dt_thresh = 0.1
		
		real*8, dimension(3) :: r,v,a
		real*8 :: dt
		
		real*8, dimension(3) :: a_next

		! Leapfrog!
		r = r + v*dt + 0.5d0 * a * dt**2
		call calc_accel(r,a_next)
		v = v + 0.5d0*(a+a_next) * dt
		
		dt = min(max(dt_thresh*minval(abs(v/a)),3.15d7),3.15d7*1.d6)
		
!		if ( all(abs(a*dt/v)<dt_thresh) ) then
!			dt = dt_thresh*minval(abs(v/a))
!		endif
		
		a = a_next
		
		! Euler!
!		v = v + a*dt
!		r = r + v*dt
!		call acceleration(r,a)
		

		return
	end subroutine integrate
