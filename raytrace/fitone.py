import sys
import os

import fitgauss

#
datafilename_in = sys.argv[1]
plot_fit_in = True

str = fitgauss.single_gauss_fit(datafilename_in,plot_fit_in,"")
print "*******************************************************"
print "Strength, +/-, Position, +/-, Width, +/-, Fit residual"
print str
