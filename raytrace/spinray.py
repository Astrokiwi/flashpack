# Loop around a dataset in gnuplot

import sys
import os

command = "convert -delay 8 -loop 0 "

for x in range(0,5,5):
	print x
	f = open("gnuplotscript",'w')
	f.write("set term postscript color\n") 
	f.write("set output 'angle"+str(x).rjust(3,"0")+".ps'\n")
	f.write("set view 45,"+str(x)+"\n")
#	f.write("splot 'morecellpts.dat' u 1:2:3 lt 2, 'raypts.dat' u 1:2:3 lt 1, 'cellpts.dat' u 1:2:3, 'blockpts.dat' u 2:3:4\n")
	f.write("splot 'throughcells.dat' u 1:2:3 lt 2, 'ray.dat' u 1:2:3 lt 1\n")
	f.close()
	os.system("gnuplot gnuplotscript")
	os.system("convert angle"+str(x).rjust(3,"0")+".ps -rotate 90 -resize 1920x1080 angle"+str(x).rjust(3,"0")+".png") #-alpha off 
	command+="angle"+str(x).rjust(3,"0")+".png "

command+="spin.gif"
os.system(command)
