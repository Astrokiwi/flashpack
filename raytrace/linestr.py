# Fit a line profile with 2 Gaussians, using gnuplot, for NaI lines output by williamson's raytrace
# The first Gaussian is fixed at 0 km/s for the first fit. It is allowed to move for the final fit.
# Fits are performed with gnuplot because I couldn't get 

import sys
import os
import math

for i in (16,32,64,128,256,512):
	infilename = "lineouts/linen"+str(i)+".dat"
	f = open(infilename,'r')
#	z = f.readlines()
#	print z[0:20]
	pts = [float((x.split(" "))[1]) for x in f.readlines()]
	f.close()
	
	sum = 0
	for x in pts:
		sum = sum + pow((1.-x),2)
	print i,'%.20f' % (math.sqrt(sum/len(pts))/0.01979363845865633742)
	
#	print pts
#	sys.exit()
