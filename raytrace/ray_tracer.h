#include <cstdlib>

class ray_tracer {
	public:
		ray_tracer(flash_data_t *indata);
		
		double fire_ray(ray_integrator *ray_int,ray_t *ray);
		double fire_ray(ray_integrator *ray_int);

		int find_child(int ib, double rray[3]);
		
		void set_murder(double line,int mdir);
		
		void set_trace();
	private:
		flash_data_t *flash_data;
		
		//		int find_child(int ib, double rray[3]);
		int ray_through_cells(ray_integrator *ray_int, ray_t *ray);
		int step_block(int ib, int dir, double rray[3]);
		
		bool debug_output;
		bool debug_text;
		ofstream f_through_cells;
		ofstream f_ray;
		ofstream f_rayline;
		
		double murderline;
		int murderdir;
};

//////////////////////////////////////////////////////////////


ray_tracer::ray_tracer(flash_data_t *indata)
{
	flash_data = indata;
	
	debug_text = false;
	debug_output = false;
	murderdir = -1;
}

//////////////////////////////////////////////////////////////

void ray_tracer::set_trace()
{
	debug_text = true;
	debug_output = true;
}

//////////////////////////////////////////////////////////////

void ray_tracer::set_murder(double line, int mdir)
{
	murderline = line;
	murderdir = mdir;
}

//////////////////////////////////////////////////////////////

int ray_tracer::step_block(int ib, int dir, double rray[3])
{
	bool firstMove;
	bool inspace = true;
	int ray_box;
	
	ray_box = ib;

	while ( flash_data->gid[ray_box*15+dir]==-1 ) // neighbour is different refinement? Go up a level!
	{
		if ( flash_data->gid[ray_box*15+6]>0 )
		{
			ray_box = flash_data->gid[ray_box*15+6]-1;
		}
		else
		{
			printf("ERROR! No parent\n");
			exit(0);
		}
	}
	
	//if ( ray_box==18094 || ray_box==10890 ) 
	//{
		//for ( int ii=0 ; ii<15 ; ii++ )
		//{
			//cout << ii << " " << flash_data->gid[ray_box*15+ii] << " " << flash_data->coordinates[flash_data->gid[ray_box*15+ii]*3] << " " << flash_data->coordinates[flash_data->gid[ray_box*15+ii]*3+1] << " " << flash_data->coordinates[flash_data->gid[ray_box*15+ii]*3+2] << " " << flash_data->blocksize[flash_data->gid[ray_box*15+ii]*3];
			//if ( ii==dir )
			//{
				//cout << "  ***";
			//}
			//cout << endl;
		//}
	//}
	
//	if ( flash_data->gid[ray_box*15+dir]==-32 || flash_data->gid[ray_box*15+dir]==-31 ) // -31 = reflective? boundary condition
	if ( flash_data->gid[ray_box*15+dir]<0 )
	{
		// reached edge of space
		
		//cout << "Edge of space" << dir/2 << " " << dir%2 << " " << flash_data->reflect[dir/2][dir%2] << endl;
		
		// Check to see if we're at a reflective boundary
		if ( flash_data->reflect[dir/2][dir%2]==0 )
		{
			return -1; // No reflect, we're out of bounds - ray is done!
		}
		else
		{
			// Reverse the ray, stay in the same box
			//cout << "Signal reverse";
			return -2; // Signal to reverse the ray
		}
	}
	if ( flash_data->gid[ray_box*15+dir]>0 ) // Move to a neighbour if there is one
	{
		ray_box = flash_data->gid[ray_box*15+dir]-1;
		if ( debug_text )
		{
			cout << "Choosing box" << ray_box << " " << flash_data->coordinates[ray_box*3] << " " << flash_data->coordinates[ray_box*3+1] << " " << flash_data->coordinates[ray_box*3+2] << " " << flash_data->blocksize[ray_box*3] << endl;
		}
		ray_box = find_child(ray_box,rray);
		if ( debug_text )
		{
			cout << "Moving to child" << ray_box << " " << flash_data->coordinates[ray_box*3] << " " << flash_data->coordinates[ray_box*3+1] << " " << flash_data->coordinates[ray_box*3+2] << " " << flash_data->blocksize[ray_box*3] << endl;
		}
	}
	else 
	{
		cout << "nagative" << " " << flash_data->gid[ray_box*15+dir] << endl;
		// I'm not sure what negative numbers that aren't -32 or -1 mean
		// Right now - assume that it means we've reached the edge of the simulated region
		
		return -1;
	}
	
	if ( dir%2==0 )
	{
		rray[dir/2] = flash_data->coordinates[ray_box*3+dir/2]+flash_data->blocksize[ray_box*3+dir/2]/2.;
	}
	else
	{
		rray[dir/2] = flash_data->coordinates[ray_box*3+dir/2]-flash_data->blocksize[ray_box*3+dir/2]/2.;
	}
	
	if ( debug_text )
	{
//		cout << "Jumpdir:" << dir/2 << endl;
//		printf("%.20f\n",flash_data->coordinates[ray_box*3+dir/2]-flash_data->blocksize[ray_box*3+dir/2]/2.);
	}
	
	if ( ray_box==ib )
	{
		cout << "Did not move when exiting block" << endl;
		cout << "Direction = " << dir << endl;
		cout << "Ray is in box " << ray_box << endl;
		cout << "rray = " << rray[0] << " " << rray[1] << " " << rray[2] << endl;
		exit(0);
	}
	
	return ray_box;
}

//////////////////////////////////////////////////////////////


int ray_tracer::ray_through_cells(ray_integrator *ray_int, ray_t *ray)
{
	int cellijk[3];
	double cellr[3];
	
	double cell_size[3];
	int ray_celldir;
	double rayt,cell_raytmin,facepos;
	
	double ray_start[3];
	
	bool inblock;
	
	int ray_boxdir = -1;
	
	int ii,jj,kk;
	
	if ( debug_text )
	{
		cout << "newblock" << ray->ib << " " << flash_data->blocksize[ray->ib]/1.25e22 << endl;
//		cout << flash_data->coordinates[ray->ib*3+0]/flash_data->blocksize[ray->ib*3+0] << " " << flash_data->coordinates[ray->ib*3+1]/flash_data->blocksize[ray->ib*3+0] << " " << flash_data->coordinates[ray->ib*3+2]/flash_data->blocksize[ray->ib*3+0] << " " << endl;
		cout << flash_data->coordinates[ray->ib*3+0]/1.25e22 << " " << flash_data->coordinates[ray->ib*3+1]/1.25e22 << " " << flash_data->coordinates[ray->ib*3+2]/1.25e22 << " " << endl;
//		printf("%.20f %.20f\n",flash_data->blocksize[ray->ib*3],flash_data->blocksize[ray->ib*3+1]);
	}
	

	for ( ii = 0 ; ii<3 ; ii++ )
	{
		cell_size[ii] = flash_data->blocksize[ray->ib*3+ii]/float(ncellx);
	}
	
	if ( debug_output )
	{
		for ( ii=0 ; ii<ncellx ; ii++ )
		{
			for ( jj = 0 ; jj<ncellx ; jj++ )
			{
				//for ( kk = 0 ; kk<ncellx ; kk++ )
				kk=0;
				{
					//f_through_cells << flash_data->coordinates[ray->ib*3]+cell_size[0]*(ii-3.5) << " " << flash_data->coordinates[ray->ib*3+1]+cell_size[1]*(jj-3.5) << " " << flash_data->coordinates[ray->ib*3+2]+cell_size[2]*(kk-3.5) << endl;
					double boxr[3];
					boxr[0] = flash_data->coordinates[ray->ib*3+0]+cell_size[0]*(ii-3.5);
					boxr[1] = flash_data->coordinates[ray->ib*3+1]+cell_size[1]*(jj-3.5);
					boxr[2] = flash_data->coordinates[ray->ib*3+2]+cell_size[2]*(kk-3.5);
					
					writebox(&f_through_cells,boxr,cell_size);
				}
			}
		}
	}
	
	for ( jj=0 ; jj<3 ; jj++ )
	{
		cellr[jj] = ray->r[jj]-(flash_data->coordinates[ray->ib*3+jj]-flash_data->blocksize[ray->ib*3+jj]/2.);
		cellijk[jj] = (int)floor(float(ncellx)*(cellr[jj]/flash_data->blocksize[ray->ib*3+jj]));
		
		if ( cellijk[jj]<0 && ray->v[jj]<0. )
		{
			return jj * 2;
		}
		if ( cellijk[jj]>=ncellx && ray->v[jj]>0. )
		{
			return jj*2+1;
		}
		
		// Correct for rounding-ish errors
		if ( (cellr[jj]-flash_data->blocksize[ray->ib*3+jj])<(flash_data->blocksize[ray->ib*3+jj])/(float(ncellx)*10.) && cellijk[jj]==ncellx)
		{
			cellijk[jj] = ncellx-1;
		}

		
//		printf("%d %.30f\n",jj,ray->r[jj]-flash_data->coordinates[ray->ib*3+jj]+flash_data->blocksize[ray->ib*3+jj]/2. );
		
		
/*		if ( ( cellijk[jj]<0 && ray->dir!=jj*2) || (cellijk[jj]>=8 && ray->dir!=jj*2+2) )
		{
			cout << "Ray not in cell at start of integration" << endl;
			if ( debug_text )
			{
				cout << ray->dir << endl;
				cout << jj << endl;
				cout << cellijk[jj] << endl;
				cout << cellr[jj] << endl;
				cout << ray->r[jj] << endl;
				printf("%.30f\n",ray->r[jj]);
				printf("%.30f\n",flash_data->coordinates[ray->ib*3+jj]-flash_data->blocksize[ray->ib*3+jj]/2. );
				printf("%.30f\n",ray->r[jj]-flash_data->coordinates[ray->ib*3+jj]+flash_data->blocksize[ray->ib*3+jj]/2. );
				printf("%.20f %.20f\n",flash_data->blocksize[ray->ib*3],cell_size*8.);
				printf("%.20f %.20f\n",(ray->r[jj]-flash_data->coordinates[ray->ib*3+jj]),(ray->r[jj]-flash_data->coordinates[ray->ib*3+jj])/cell_size);
				cout << ray->r[jj]/flash_data->blocksize[ray->ib*3] << endl;
				cout << flash_data->coordinates[ray->ib*3+jj]-flash_data->blocksize[ray->ib*3]/2. << " " << flash_data->coordinates[ray->ib*3+jj]+flash_data->blocksize[ray->ib*3]/2. << endl;
			}
			exit(0);
		}*/
	}
		
	// Basically repeat same procedure as for a block, but without dealing with refinements etc
	// Also, (0,0,0) is the *corner*, not the centre
	inblock = true;
	while ( inblock )
	{
		double ray_times[6];
	
		cell_raytmin = 1.e30;
		ray_celldir = -1;
		if ( debug_text )
		{
//			cout << cellr[0]/flash_data->blocksize[ray->ib*3] << " " <<  cellr[1]/flash_data->blocksize[ray->ib*3+1] << " " << cellr[2]/flash_data->blocksize[ray->ib*3+2] << endl;
			cout << cellijk[0] << " " << cellijk[1] << " " << cellijk[2] << " " << cellr[0]/flash_data->blocksize[ray->ib*3+0]*8. << " " << cellr[1]/flash_data->blocksize[ray->ib*3+1]*8. << " " << cellr[2]/flash_data->blocksize[ray->ib*3+2]*8. << " " << cellr[0]/flash_data->blocksize[ray->ib*3+0]*8.-cellijk[0] << " " << cellr[1]/flash_data->blocksize[ray->ib*3+1]*8.-cellijk[1] << " " << cellr[2]/flash_data->blocksize[ray->ib*3+2]*8.-cellijk[2] << endl;
			
			
			if ( debug_output )
			{
				double boxr[3],boxdx[3];
				for ( int kk = 0 ; kk < 3 ; kk ++ )
				{
					boxr[kk] = cellijk[kk]*flash_data->blocksize[ray->ib*3+kk]*(1./8.)+flash_data->coordinates[ray->ib*3+kk]-flash_data->blocksize[ray->ib*3+kk]*(7./16.);
					boxdx[kk] = flash_data->blocksize[ray->ib*3+kk]/8.;
				}
				writebox(&f_ray,boxr,boxdx);
//				f_ray << cellr[0] << " " << cellr[1] << " " << cellr[2] << endl;
			}
		}
		for ( int fc = 0 ; fc<6 ; fc++ )
		{
			ray_times[fc] = -1.;
//			if ( fc!=ray->dir )
			if ( (ray->v[fc/2]>0 && fc%2==1) || (ray->v[fc/2]<0 && fc%2==0) )
			{
				facepos = cell_size[fc/2]*cellijk[fc/2];
				if ( fc%2==1 )
				{
					facepos+=cell_size[fc/2];
				}
				rayt = (facepos-cellr[fc/2])/ray->v[fc/2];
				ray_times[fc] = rayt;
				
//				printf("%d %.20f %.20f %.20e %d %d %d\n",fc,cellr[fc/2],facepos,rayt,cellijk[0],cellijk[1],cellijk[2]);
	
				if ( rayt>0. && ray->v[fc/2]*pow(-1.,fc+1)>0. )
				{
					if ( rayt<cell_raytmin )
					{
						cell_raytmin = rayt;
						ray_celldir = fc;
					}
				}
				if ( debug_text )
				{
//					cout << fc << " " << rayt << " ";
				}
			}
		}
//		cout << endl;
		
		if ( ray_celldir==-1 )
		{
			inblock = false;
			printf("Did not exit cell!\n"); // Output debug info
			cout << "ray->v=" << ray->v[0] << " " << ray->v[1] << " " << ray->v[2] << endl;
			cout << "ray r (in cell)=" << cellr[0] << " " << cellr[1] << " " << cellr[2] << endl;
			cout << "ijk in block=" << cellijk[0] << " " << cellijk[1] << " " << cellijk[2] << endl;
			for ( int fc = 0 ; fc<6 ; fc++ )
			{
//				cout << "v[" << fc/2 << "]=" << ray->v[fc/2] << ", fc%2=" << fc%2 << endl;
				if ( (ray->v[fc/2]>0 && fc%2==1) || (ray->v[fc/2]<0 && fc%2==0) )
				{
					cout << fc << " is a valid direction" << endl;
					
					facepos = cell_size[fc/2]*cellijk[fc/2];
					if ( fc%2==1 )
					{
						facepos+=cell_size[fc/2];
					}
					
					cout << "facepos = " << facepos << endl;
					cout << "dr = " << (facepos-cellr[fc/2]) << " rayt = " << (facepos-cellr[fc/2])/ray->v[fc/2] << endl;
					
				}
				else
				{
					cout << fc << " is an invalid direction" << endl;
				}
			}
		}
		else
		{
			// This is where the actual radiative transfer happens
			
			// Update ray coordinate
			for ( jj=0 ; jj<3 ; jj++ )
			{
				ray_start[jj] = cellr[jj];
				cellr[jj]+=ray->v[jj]*cell_raytmin;
				ray->r[jj]+=ray->v[jj]*cell_raytmin;
//				cellijk[jj] = (int)floor(float(ncellx)*(cellr[jj]/flash_data->blocksize[ray->ib*3+jj]));
/*				if ( ray_times[jj*2]==cell_raytmin )
				{
					cellijk[jj]--;
				}
				if ( ray_times[jj*2+1]==cell_raytmin )
				{
					cellijk[jj]++;
				}*/
			}
			
//			cout << ray->r[1] << endl;
			ray_int->integrate(ray->ib,cellijk,ray_start,cellr); // Do integration (can be *any* integrator object!)
			
			cellijk[ray_celldir/2]+=(int)pow(-1.,ray_celldir+1); // Update ijk to new cell			

			// Check if we need to move in another direction
			for ( jj = 0 ; jj < 3 ; jj ++ )
			{
				if ( jj!=ray_celldir/2 )
				{
					cellijk[jj] = (int)floor(float(ncellx)*(cellr[jj]/flash_data->blocksize[ray->ib*3+jj]));
				}
			}
			
			if ( debug_output )
			{
				f_rayline << cellr[0]+flash_data->coordinates[ray->ib*3]-flash_data->blocksize[ray->ib*3]/2. << " " << cellr[1]+flash_data->coordinates[ray->ib*3+1]-flash_data->blocksize[ray->ib*3]/2. << " " << cellr[2]+flash_data->coordinates[ray->ib*3+2]-flash_data->blocksize[ray->ib*3]/2. << endl;
			}

			for ( jj=0 ; jj<3 ; jj++ )
			{
				if ( cellijk[jj]<0 )
				{
					return jj * 2;
				}
				
				if ( cellijk[jj]>=ncellx )
				{
					return jj*2+1;
				}
			}
			
			
			
//			if ( cellijk[ray_celldir/2]<0 || cellijk[ray_celldir/2]>=ncellx )
//			{
/*				if ( ray_celldir%2==0 )
				{
					ray->r[ray_celldir/2] = flash_data->coordinates[ray->ib*3+ray_celldir/2]-flash_data->blocksize[ray->ib*3]/2.;
				}
				else
				{
					ray->r[ray_celldir/2] = flash_data->coordinates[ray->ib*3+ray_celldir/2]+flash_data->blocksize[ray->ib*3]/2.;
				}*/
//				if ( debug_text )
//				{
//					cout << "dir=" << ray_celldir << endl;
//					cout << cellr[0]/flash_data->blocksize[ray->ib*3] << " " <<  cellr[1]/flash_data->blocksize[ray->ib*3+1] << " " << cellr[2]/flash_data->blocksize[ray->ib*3+2] << endl;
//					cout << ray->r[0]/flash_data->blocksize[ray->ib*3] << endl;
//					cout << (ray->r[0]-flash_data->coordinates[ray->ib*3])/flash_data->blocksize[ray->ib*3] << endl;
					
//					printf("%.20f\n",flash_data->coordinates[ray->ib*3+jj]-flash_data->blocksize[ray->ib*3]/2. );
//					printf("%.20f %.20f\n",flash_data->blocksize[ray->ib*3],cell_size*8.);
//					printf("%.20f\n",(ray->r[0]-flash_data->coordinates[ray->ib*3])/flash_data->blocksize[ray->ib*3]);
//				}
				
//				inblock = false; // Done! Let's get outta here!
//				ray_boxdir = ray_celldir;
//			}
		}
	}
	
	
	return ray_boxdir;
}


//////////////////////////////////////////////////////////////


// Fire a test ray
double ray_tracer::fire_ray(ray_integrator *ray_int)
{
	ray_t ray;
	
	ray.ib = 1635;
	ray.r[0] = -10281.9;
	ray.r[1] = -1255.8;
	ray.r[2] = 9418.51;
	
//	cout << flash_data->coordinates[ray.ib*3] << " " << flash_data->coordinates[ray.ib*3+1] << " " << flash_data->coordinates[ray.ib*3+2] << endl;
//	exit(0);
/*	ray.r[0] = flash_data->coordinates[ray.ib*3]+.1;
	ray.r[1] = flash_data->coordinates[ray.ib*3+1]+.2;
	ray.r[2] = flash_data->coordinates[ray.ib*3+2]+.3;*/
	
	ray.v[0] = 1.;
	ray.v[1] = 0.;
	ray.v[2] = 0.0;
	
//	ray.dir = 1;

	return	fire_ray(ray_int,&ray);
}

//////////////////////////////////////////////////////////////


double ray_tracer::fire_ray(ray_integrator *ray_int,ray_t *ray)
{
	bool in_space; // Still within the simulation box
	int dir; // Which direction the ray is going
	int iib;
	
	int badness_count = 0;
	
	double ray_r0[3];
	
	for ( int ii=0 ; ii<3 ; ii++)
	{
		ray_r0[ii]=ray->r[ii];
	}
	
	
	//f_all_blocks.open("allblocks.dat");
	if ( debug_output )
	{
		/*if ( omp_get_num_threads()>1 )
		{
			cout << "debug_output does not work with multiple threads!" << endl;
			exit(0);
		}*/
		f_through_cells.open("throughcells.dat");
		f_ray.open("ray.dat");
		f_rayline.open("rayline.dat");
	}

	ray->ib = find_child(ray->ib,ray->r); // Check if initial box is actually a "parent" - need to get to max refinement for this pt

	in_space = true;
	
	// Check if ray is on the wrong side of the PLANE OF MURDER
	if ( murderdir!=-1 )
	{
		if ( (ray->v[murderdir]>0. && ray_r0[murderdir]>murderline) || (ray->v[murderdir]<0. && ray_r0[murderdir]<murderline) )
		{
			in_space = false;
		}
	}
	
	while(in_space)
	{
//		printf("Loop %d, block=%d\n",badness_count,ib);
		dir = ray_through_cells(ray_int,ray);
//		ray->dir = dir;
		
		if ( dir==-1 )
		{
			printf("Failed to exit block!\n");
			cout << ray->v[0] << " " << ray->v[1] << " " << ray->v[2] << endl;
			exit(0);
		}
		
		iib = ray->ib;
		ray->ib = step_block(ray->ib,dir,ray->r);
//		cout << "Ray is now in " << ray->ib << endl;
		if ( iib==ray->ib )
		{
			cout << "Did not enter new block correctly" << endl;
			exit(0);
		}
		
		// Left the volume
		if ( ray->ib==-1 )
		{
			in_space = false;
		}
		
		// Hit a reflective boundary
		if ( ray->ib==-2 )
		{
			ray->ib = find_child(iib,ray->r); 
			ray->v[dir/2] = -ray->v[dir/2];
			//cout << "Reflect!" << iib << endl;
		}
		
		badness_count++;
		if ( badness_count>=bad_blocks )
		{
			in_space = false;
		}
		
		// Check if the ray has hit THE PLANE OF MURDER
		if ( murderdir!=-1 )
		{
			if ( (ray->r[murderdir]>murderline && ray_r0[murderdir]<murderline) || (ray->r[murderdir]<murderline && ray_r0[murderdir]>murderline) )
			{
				in_space = false;
			}
		}
	}
	
	if ( debug_output )
	{
		f_through_cells.close();
		f_ray.close();
		f_rayline.close();
	}
	
	return 0.;
}

//////////////////////////////////////////////////////////////


int ray_tracer::find_child(int ib, double rray[3])
{
	int child;
	int outb;
	
	outb = ib;

	while ( flash_data->nodetype[outb]!=1 ) // 1 = child node
	{
		if ( flash_data->gid[outb*15+7]>0 )
		{
			
			if ( rray[0]<flash_data->coordinates[outb*3] )
				child = 0;
			else
				child = 1;
			if ( rray[1]<flash_data->coordinates[outb*3+1] )
				child += 0;
			else
				child += 2;
			if ( rray[2]<flash_data->coordinates[outb*3+2] )
				child += 0;
			else
				child += 4;
			
			child+=7;
			outb = flash_data->gid[outb*15+child]-1;
		}
		else
		{
			printf("ERROR no child\n");
			exit(0);
		}
	}
	
	return outb;
}
