
class dens_integrator : public ray_integrator {
	public:
		dens_integrator(flash_data_t *indata);
	
		void integrate(int ib, int ic[3], double ray_start[3], double ray_end[3]);
		
		double getColumnDensity();
		void reset();
		void set_trace();
		
		void set_numberdens();
		
		void dilute(double di);
		void set_onlycold();
	
	private:
		double column_scal;
		double dilute_factor;
		flash_data_t *flash_data;
		bool trace_on;
		bool numberdens;
		bool only_cold;
};

dens_integrator::dens_integrator(flash_data_t *indata)
{
	flash_data = indata;
	column_scal = 0.;
	trace_on = false;
	numberdens = false;
	dilute_factor = 1.;
	only_cold = false;
}

void dens_integrator::set_onlycold()
{
	only_cold = true;
}

void dens_integrator::dilute(double di)
{
	dilute_factor = di;
}

void dens_integrator::set_numberdens()
{
	numberdens = true;
}

void dens_integrator::reset()
{
	column_scal = 0.;
}

double dens_integrator::getColumnDensity()
{
	return column_scal;
}

void dens_integrator::set_trace()
{
	trace_on = true;
}

// ib = box
// ic = ijk of initial cell
// ray_start = coords of initial point
// ray_end = coords in final point
void dens_integrator::integrate(int ib, int ic[3], double ray_start[3], double ray_end[3])
{
	double scalar;
	double dist;

	int ii;
	int cc;
	
	dist = 0.;
	for ( ii = 0 ; ii<3 ; ii++ )
	{
		dist+=pow(ray_start[ii]-ray_end[ii],2);
	}
	dist = sqrt(dist);
	
	cc = ib*(ncellx*ncellx*ncellx)+ic[2]*(ncellx*ncellx)+ic[1]*(ncellx)+ic[0];
	
	// Update this to an interpolation?
	
	if ( !only_cold || (flash_data->temp[cc]<5.e4 || flash_data->dens[cc]>sodium_dens_cut) )
	{
		scalar = flash_data->dens[cc]/dilute_factor; // Units here are g pc/cm^3, so not correct yet
		if ( numberdens )
		{
			scalar*= H_per_dens;
		}
	//	scalar = flash_data->temp[ib*(nx*nx*nx)+ic[2]*(nx*nx)+ic[1]*(nx)+ic[0]]; // Units here are g pc/cm^3, so not correct yet
	}
	else
	{
		scalar = 0.;
	}
	
	column_scal += scalar*dist;
//	column_scal = max(column_scal,scalar);
//	column_scal = min(column_scal,flash_data->temp[cc]);


//	cout << column_scal  << " " << -scalar*dist << " " << exp(-scalar*dist) << endl;
//	column_scal *= exp(-scalar*dist);

	if ( trace_on )
	{
		//cout << cc << " " << column_scal << " " << scalar << " " << dist << endl;
	}
	
	return;
}
