import shutil
import sys

#suffix = "broaden"

#print len(sys.argv)
if ( len(sys.argv)==1 ):
	suffix = ""
else:
	suffix = sys.argv[1]
f = open("width_angle"+suffix+"B_selected.dat")
choices = [int((x.split(" "))[4]) for x in f.readlines()]
f.close()

print choices

#sys.exit()
count = 0
#for count in range(0,len(choices)):
for ii in [0,3,6,9,12,15,18,21]:
	if ( choices[count]==1 ):
		file_src = "lineouts/fixedsizerot"+suffix+"B"+str(ii).rjust(4,'0')+".dat_single.gnu"
	else:
		file_src = "lineouts/fixedsizerot"+suffix+"B"+str(ii).rjust(4,'0')+".dat_double.gnu"
	file_dest = "lineouts/fixedsizerot"+suffix+"B"+str(ii).rjust(4,'0')+".dat_select.gnu"
	#print file_src,file_dest
	shutil.copyfile(file_src,file_dest)
	count = count + 1
