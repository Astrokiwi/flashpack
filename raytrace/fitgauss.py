# Fit a line profile with 2 Gaussians, using gnuplot, for NaI lines output by williamson's raytrace
# The first Gaussian is fixed at 0 km/s for the first fit. It is allowed to move for the final fit.
# Fits are performed with gnuplot because I couldn't get 

import sys
import os

#def symmetric_triple_gauss_fit(datafilename,plot_fit):
	## Guess the peaks!
	#f = open(datafilename,'r')
	#line_data = [[float(y) for y in x.split()] for x in f.readlines()]
	#f.close()
	
	#centre = 0.
	#weight_sum = 0.
	#for ii in range(2,len(line_data)-2):
		#centre = centre + line_data[ii][0]*(1-line_data[ii][1])
		#weight_sum = weight_sum + (1-line_data[ii][1])
	
	#centre = centre/weight_sum
	
	##return str(centre)
	
	#peak1 = [0,0.,0.2]
	#peak2 = [0,200.,0.2]
	
	#for ii in range(2,len(line_data)-2):
		#d1 = line_data[ii+1][1]-line_data[ii-1][1]
		#d2 = line_data[ii+1][1]-2*line_data[ii][1]+line_data[ii-1][1]
		#if ( d1!=0 ):
			#weight = (1.-line_data[ii][1])*d2/abs(d1)
			#if (weight>peak1[0] ):
				#peak2[0] = peak1[0]
				#peak2[1] = peak1[1]
				#peak2[2] = peak1[2]
				#peak1[0] = weight
				#peak1[1] = line_data[ii][0]
				#peak1[2] = 1-line_data[ii][1]
			#else:
				#if ( weight>peak2[0] ):
					#peak2[0] = weight
					#peak2[1] = line_data[ii][0]
					#peak2[2] = 1-line_data[ii][1]
	
	##print peak1
	##print peak2
	##sys.exit()
	
	#gscript ="g(x)=1-(abs(A)*exp(-(x-b)**2/c**2)+abs(D)*exp(-(x-e)**2/f**2)+abs(G)*exp(-(x-h)**2/i**2))\n"
	#gscript+="A="+str(peak1[2])+";D="+str(peak2[2])+";b="+str(peak1[1])+";e="+str(peak2[1])+";c=50.;f=50.;\n"
	#if ( abs(peak1[1]-centre)>abs(peak2[1]-centre) ):
		#gscript+="G="+str(peak1[2])+";h="+str(2*centre-peak1[1])+";i=50.;\n"
	#else:
		#gscript+="G="+str(peak2[2])+";h="+str(2*centre-peak2[1])+";i=50.;\n"
	#gscript+="set fit errorvariables\n"
	##gscript+="FIT_LIMIT=1.e-6\n"
	##gscript+="fit g(x) '"+datafilename+"' u 1:2:(1./(0.001+abs(1-$2)**2)) via A,c,D,e,f\n"
	##gscript+="fit g(x) '"+datafilename+"' u 1:2:(1./(0.001+abs(1-$2)**2)) via A,b,c,D,e,f\n"
	##gscript+="fit g(x) '"+datafilename+"' u 1:2:(1./(0.001+abs(1-$2)**2)) via A,c,D,e,f\n"
	#gscript+="fit g(x) '"+datafilename+"' u 1:2:((1.0001-$2)**(-2)) via A,b,c,D,e,f,G,h,i\n"
##	gscript+="fit g(x) '"+datafilename+"' u 1:2 via A,c,D,e,f\n"
##	gscript+="fit g(x) '"+datafilename+"' u 1:2 via A,b,c,D,e,f\n"
	
	#if ( plot_fit ):
		#gscript+="set output '"+datafilename+"_triple.png'\n"
		#gscript+="set term png enhanced crop font '../liberation-fonts-1.04/LiberationSans-Regular.ttf' 20\n"
		#gscript+="set border lw 5\n"
##		gscript+="set key bottom right\n"
		#gscript+="unset key\n"
		##gscript+="set title '"+str(int(datafilename[-8:-4]))+"0^o'\n"
		#gscript+="set xtics rotate by 270\n"		
##		gscript+="set yrange [:"+str(1+A/10.)+"]\n"
##		gscript+="plot '"+datafilename+"' u 1:2 w l title 'Line at "+str(int(datafilename[-8:-4]))+"0^o' lw 5, g(x) w l title 'Fit' lw 5\n"
		#gscript+="set y2tics border\n"
		#gscript+="set y2range[(-.5*A):(2.4*A)]\n"
		#gscript+="set x2zeroaxis lt -1\n"
		#gscript+="plot '"+datafilename+"' u 1:2 w l lw 5, g(x) w l lw 5,'"+datafilename+"' u 1:((g($1)-$2)) w p axes x1y2\n\n"
	
	#gscript+="set print 'fit_data.dat'\n"
##	gscript+="print A;print A_err;print D;print D_err;print b;print b_err;print e;print e_err;print c;print c_err;print f;print f_err;\n"
	#gscript+="print b,e,h,A,D,G,c,f,i;\n"
##	gscript+="print A,A_err,D,D_err,b,e,c,c_err,f,f_err;\n"
	
	#f = open("gnuplotscript",'w')
	#f.write(gscript)
	#f.close()
	
	#os.system("gnuplot gnuplotscript >& 1")
	
	#f = open("fit_data.dat",'r')
	#inlines = f.readlines()
	#f.close()
	
	##central_strength = float(inlines[0])
	##outflow_strength = float(inlines[2])
	##central_centre = float(inlines[4])
	##outflow_centre = float(inlines[6])
	##central_width = float(inlines[8])
	##outflow_width = float(inlines[10])
	##outflow_width_error = float(inlines[11])
	
	##print inlines
	##print "Outflow line-width=",outflow_width," +/- ",outflow_width_error," km/s"
	##return outflow_width,outflow_width_error
	#return inlines[0]


def double_gauss_fit(datafilename,plot_fit,suffix):
	# Guess the peaks!
	if ( suffix=="doublet" ):
		initfilename_splut = datafilename.split("doublet")
		print initfilename_splut
		#sys.exit()
		initfilename = initfilename_splut[0]+"derev"+initfilename_splut[1]
		f = open(initfilename)
	else:
		f = open(datafilename,'r')
	line_data = [[float(y) for y in x.split()] for x in f.readlines()]
	f.close()
	
	peak1 = [0,0.,0.2]
	peak2 = [0,200.,0.2]
	
	for ii in range(2,len(line_data)-2):
		d1 = line_data[ii+1][1]-line_data[ii-1][1]
		d2 = line_data[ii+1][1]-2*line_data[ii][1]+line_data[ii-1][1]
		if ( d1!=0 ):
			weight = (1.-line_data[ii][1])*d2/abs(d1)
			if (weight>peak1[0] ):
				peak2[0] = peak1[0]
				peak2[1] = peak1[1]
				peak2[2] = peak1[2]
				peak1[0] = weight
				peak1[1] = line_data[ii][0]
				peak1[2] = 1-line_data[ii][1]
			else:
				if ( weight>peak2[0] ):
					peak2[0] = weight
					peak2[1] = line_data[ii][0]
					peak2[2] = 1-line_data[ii][1]
	
	#print peak1
	#print peak2
	#sys.exit()
	
	if ( suffix=="doublet" ):
		f = open(datafilename,'r')
		line_data = [[float(y) for y in x.split()] for x in f.readlines()]
		f.close()
		gscript ="g(x)=1-(abs(A)*(exp(-(x-b)**2/c**2)+exp(-(x-b-(-305.4))**2/c**2))+abs(D)*(exp(-(x-e)**2/f**2)+exp(-(x-e-(-305.4))**2/f**2)))\n"
	else:
		#print "awwww shoot"
		#sys.exit()
		gscript ="g(x)=1-(abs(A)*exp(-(x-b)**2/c**2)+abs(D)*exp(-(x-e)**2/f**2))\n"
	gscript+="A="+str(peak1[2])+";D="+str(peak2[2])+";b="+str(peak1[1])+";e="+str(peak2[1])+";c=50.;f=50.;\n"
	gscript+="set fit errorvariables\n"
	#gscript+="FIT_LIMIT=1.e-6\n"
	#gscript+="fit g(x) '"+datafilename+"' u 1:2:(1./(0.001+abs(1-$2)**2)) via A,c,D,e,f\n"
	#gscript+="fit g(x) '"+datafilename+"' u 1:2:(1./(0.001+abs(1-$2)**2)) via A,b,c,D,e,f\n"
	#gscript+="fit g(x) '"+datafilename+"' u 1:2:(1./(0.001+abs(1-$2)**2)) via A,c,D,e,f\n"
	gscript+="fit g(x) '"+datafilename+"' u 1:2:((1.0001-$2)**(-2)) via A,b,c,D,e,f\n"
#	gscript+="fit g(x) '"+datafilename+"' u 1:2 via A,c,D,e,f\n"
#	gscript+="fit g(x) '"+datafilename+"' u 1:2 via A,b,c,D,e,f\n"
	
	if ( plot_fit ):
		gscript+="set output '"+datafilename+"_double.ps'\n"
#		gscript+="set term png enhanced crop font '../liberation-fonts-1.04/LiberationSans-Regular.ttf' 20\n"
		gscript+="set term postscript enhanced color solid 'Helvetica' 20 size 6,4\n"
		gscript+="set border lw 5\n"
#		gscript+="set key bottom right\n"
		gscript+="unset key\n"
		#gscript+="set title '"+str(int(datafilename[-8:-4]))+"0^o'\n"
		gscript+="set xtics rotate by 270\n"		
		gscript+="set xtics 250\n"		
		gscript+="set ylabel 'I/I_0'\n"
		gscript+="set y2label 'Residual'\n"
		gscript+="set xlabel 'v (km/s)'\n"
#		gscript+="set yrange [:"+str(1+A/10.)+"]\n"
#		gscript+="plot '"+datafilename+"' u 1:2 w l title 'Line at "+str(int(datafilename[-8:-4]))+"0^o' lw 5, g(x) w l title 'Fit' lw 5\n"
		gscript+="set y2tics border\n"
		gscript+="set y2range[(-.5*A):(2.4*A)]\n"
		gscript+="set x2zeroaxis lt -1\n"
		gscript+="plot '"+datafilename+"' u 1:2 w l lw 5, g(x) w l lw 5,'"+datafilename+"' u 1:((g($1)-$2)) w l lw 5 axes x1y2\n\n"
	
	gscript+="set print 'fit_data.dat'\n"
#	gscript+="print A;print A_err;print D;print D_err;print b;print b_err;print e;print e_err;print c;print c_err;print f;print f_err;\n"
	gscript+="print A,A_err,D,D_err,b,b_err,e,e_err,c,c_err,f,f_err,FIT_STDFIT;\n"
#	gscript+="print A,A_err,D,D_err,b,e,c,c_err,f,f_err;\n"
	
#	f = open("gnuplotscript",'w')
	f = open(datafilename+"_double.gnu",'w')
	f.write(gscript)
	f.close()
	
	os.system("gnuplot "+datafilename+"_double.gnu >& 1")
	
	f = open("fit_data.dat",'r')
	inlines = f.readlines()
	f.close()
	
	#central_strength = float(inlines[0])
	#outflow_strength = float(inlines[2])
	#central_centre = float(inlines[4])
	#outflow_centre = float(inlines[6])
	#central_width = float(inlines[8])
	#outflow_width = float(inlines[10])
	#outflow_width_error = float(inlines[11])
	
	#print inlines
	#print "Outflow line-width=",outflow_width," +/- ",outflow_width_error," km/s"
	#return outflow_width,outflow_width_error
	return inlines[0]

#datafilename_in = sys.argv[1]
#plot_fit_in = False

#outflow_width_out,outflow_width_error_out = double_gauss_fit(datafilename_in,plot_fit_in)

#print "Outflow line-width=",outflow_width_out," +/- ",outflow_width_error_out," km/s"

def single_gauss_fit(datafilename,plot_fit,suffix):
	# Guess the peaks!
	if ( suffix=="doublet" ):
		initfilename_splut = datafilename.split("doublet")
		print initfilename_splut
		#sys.exit()
		initfilename = initfilename_splut[0]+"derev"+initfilename_splut[1]
		f = open(initfilename)
	else:
		f = open(datafilename,'r')
	line_data = [[float(y) for y in x.split()] for x in f.readlines()]
	f.close()
	
	peak = [0,0.,0.2]
	
	for ii in range(2,len(line_data)-2):
		d1 = line_data[ii+1][1]-line_data[ii-1][1]
		d2 = line_data[ii+1][1]-2*line_data[ii][1]+line_data[ii-1][1]
		if ( d1!=0 ):
			weight = (1.-line_data[ii][1])*d2/abs(d1)
			if (weight>peak[0] ):
				peak[0] = weight
				peak[1] = line_data[ii][0]
				peak[2] = 1-line_data[ii][1]
	
	#print peak1
	#print peak2
	#sys.exit()
	
	if ( suffix=="doublet" ):
		f = open(datafilename,'r')
		line_data = [[float(y) for y in x.split()] for x in f.readlines()]
		f.close()
		gscript ="g(x)=1-(A*(exp(-(x-b)**2/c**2)+exp(-(x-b-(-305.4))**2/c**2)))\n"
	else:
		#print "awwww shoot"
		#sys.exit()
		gscript ="g(x)=1-(A*exp(-(x-b)**2/c**2))\n"
	gscript+="A="+str(peak[2])+";b="+str(peak[1])+";c=50.;\n"
	gscript+="set fit errorvariables\n"
	#gscript+="FIT_LIMIT=1.e-6\n"
	#gscript+="fit g(x) '"+datafilename+"' u 1:2:((1.0001-$2)**(-2)) via A,b,c\n"
	gscript+="fit g(x) '"+datafilename+"' u 1:2 via A,b,c\n"
	
	if ( plot_fit ):
		#gscript+="plot '"+datafilename+"' u 1:2 w l lw 5, g(x) w l lw 5\n"
		gscript+="set output '"+datafilename+"_single.ps'\n"
#		gscript+="set term png enhanced crop font '../liberation-fonts-1.04/LiberationSans-Regular.ttf' 20\n"
		gscript+="set term postscript enhanced color solid 'Helvetica' 20 size 6,4\n"
		gscript+="set border lw 5\n"
#		gscript+="set key bottom right\n"
		gscript+="unset key\n"
		#gscript+="set title '"+str(int(datafilename[-8:-4]))+"0^o'\n"
		gscript+="set xtics rotate by 270\n"		
		gscript+="set xtics 500\n"
		gscript+="set ylabel 'I/I_0'\n"
		gscript+="set y2label 'Residual'\n"
		gscript+="set xlabel 'v (km/s)'\n"
#		gscript+="set yrange [:"+str(1+A/10.)+"]\n"
		gscript+="set yrange [(1-2.*A):(1+A/10.)]\n"
#		gscript+="plot '"+datafilename+"' u 1:2 w l title 'Line at "+str(int(datafilename[-8:-4]))+"0^o' lw 5, g(x) w l title 'Fit' lw 5\n"
		gscript+="set y2tics border\n"
		gscript+="set y2range[(-.5*A):(2.4*A)]\n"
		gscript+="set x2zeroaxis lt -1\n"
		#gscript+="set y2label 'Residuals'\n"
		gscript+="plot '"+datafilename+"' u 1:2 w l lw 5, g(x) w l lw 5,'"+datafilename+"' u 1:((g($1)-$2)) w l lw 5 axes x1y2\n"
	
	gscript+="set print 'fit_data.dat'\n"
	gscript+="print A,A_err,b,b_err,c,c_err,FIT_STDFIT;\n"
	
#	f = open("gnuplotscript",'w')
	f = open(datafilename+"_single.gnu",'w')
	f.write(gscript)
	f.close()
	
	os.system("gnuplot "+datafilename+"_single.gnu >& 1")

	f = open("fit_data.dat",'r')
	inlines = f.readlines()
	f.close()
	
	#central_strength = float(inlines[0])
	#outflow_strength = float(inlines[2])
	#central_centre = float(inlines[4])
	#outflow_centre = float(inlines[6])
	#central_width = float(inlines[8])
	#outflow_width = float(inlines[10])
	#outflow_width_error = float(inlines[11])
	
	#print inlines
	#print "Outflow line-width=",outflow_width," +/- ",outflow_width_error," km/s"
	#return outflow_width,outflow_width_error
	return inlines[0]
