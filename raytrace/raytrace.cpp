// Read in a flash file & do raytracing on it
// Requires 3D data, blocks of ncellx*ncellx*ncellx cells (defined in raytrace_structs_consts.h)

#include <fftw3.h>
#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <ctime>
#include <omp.h>
#include <string.h>

using namespace std;

void writebox(ofstream *f, double cent[3], double dx[3]);

#include "raytrace_structs_consts.h"
#include "ray_integrator.h"
#include "ray_tracer.h"
#include "flash_reader.h"
#include "ray_generator.h"
#include "unrevolve.h"

///////////


void helpandquit()
{
	cout << "Printing help" << endl;
	cout << "______________________________________________" << endl;
	cout << endl;
	cout << "Simple raytrace routine" << endl;
	cout << "By David Williamson, St Mary's University 2011" << endl;
	cout << endl;
	cout << "-f          | input filename" << endl;
	cout << "-m          | minimum log density cut" << endl;
	cout << "-n          | gives NxN rays" << endl;
	cout << "-t -b -l -r | top/bottom/left/right truncation (0.-1.)" << endl;
	cout << "-x -y -z    | x,y,z for ray. Any unset are 0. except if none are set, then ray is (x,y,z)=(1.,0.,0.)" << endl;
	cout << "-o [S]      | output filename" << endl;
	cout << "-refl x/y/z | Reflective boundary counditions at the low x/y/z boundary" << endl;
	cout << "-refh x/y/z | Reflective boundary counditions at the high x/y/z boundary" << endl;
	cout << "-dens       | directly integrate column density" << endl;
	cout << "-dist       | directly integrate distance" << endl;
	cout << "-Na         | estimate NaI absorbtion spectrum" << endl;
	cout << "-test       | override ICs with constant density" << endl;
	cout << "-ix         | Only fire ray with at point X,Y in grid (integers)" << endl;
	cout << "-iy         |" << endl;
	cout << "-testp      | Set precision of data dumped to cout" << endl;
	cout << "-intersect  | Dump out where rays intersect (no density data)" << endl;
	cout << "And loads of undocumented features that you'll have to look through raytrace.cpp to understand" << endl;
	cout << "______________________________________________" << endl;
	exit(0);
}

///////////

//const int n_ray = 128;
//const double minlogdens = -50;

int main(int argc, char* argv[]) {
	flash_reader *lores_galaxy;
	ray_tracer *tracer;
	flash_data_t *flash_data;
	
	dens_integrator *dens_ray;
	distance_integrator *dist_ray;
	sodium_line_integrator *sodium_ray;
	vdel_integrator *vdel_ray;
	vave_integrator *mvel_ray;
	
	enum integrator_t {DENS_GRATOR,SODIUM_GRATOR,DIST_GRATOR,VDEL_GRATOR,MVEL_GRATOR};
	integrator_t this_integrator = DENS_GRATOR;
	
	ray_generator *ray_gen;
	ray_t *ray_in;
	
	clock_t start,stop,big_start,big_stop;
	clock_t integrate_time,raygen_time;
	
	double cropbox[2][2];
	int cropint[2][2];
	
	int ir;
	int ixo;
	int idir;
	int iib;
	int ii,jj,cc;
	
	int ix,iy;
	
	ofstream dens_map_f;
	ofstream line_map_f;
	stringstream dens_map_s;
	
	ofstream closest_blocks_f;
	ofstream intersect_blocks_f;
	
	ofstream line_f;
	
	double raydir[3];
	bool reflect[3][2];
	
	double out_dens;
	
	bool rays_done;
	bool first_ray;
	
	int nline;
	
	bool debug_output = false; 
	bool test_ics = false;
	bool ray_set = false;
	bool find_intersects = false;

	bool debug_mode = false;
	bool ndens = false;
	
	bool produce_doublet = false;
	bool calcturb = false;
	bool fit_turb = false;
	bool thermal_broad = true;
	
	double view_ang;
	double broaden_v;
	double dilute_factor;
	
	int testix,testiy;
	
	double rotphi_range[2],rottheta_range[2];
	double rotphi,rottheta;
	int rot_steps;
	bool rotating;

	double murderline;
	int murderdir;
	
	double gridsize;
	
	bool vel_reverse;
	bool only_cold;
	bool derevolve;
	bool optical_depth_only;
	
//	stringstream filename;
	
	// fftw junk
	t_darray *in,*out;
	t_cmparray *intermediate;
	fftw_complex *gausstrans;
	fftw_plan *p1,*p2;
	int nthreads,ithread;

	ofstream f_all_blocks;

	line_prof_t *line_prof;
	
	// Parse command line
	
	// Set defaults
	int n_ray = 128;
	double minlogdens = -100;
	testix = -1;
	testiy = -1;
	
	cropbox[0][0] = 0.0;
	cropbox[0][1] = 1.;
	cropbox[1][0] = 0.0;
	cropbox[1][1] = 1.;
	rotphi_range[0] = 0.;
	rotphi_range[1] = 0.;
	rottheta_range[0] = 0.;
	rottheta_range[1] = 0.;
				
	// No default filename
	char* inputfile;
	char* outfile_initial = "densmap.dat";
	char* outfile;
	bool file_set = false;
	rot_steps = 2;
	rotating = false;
	vel_reverse = false;
	
	cout << "test 1" << endl;
	closest_blocks_f.open("closestblocks.dat");
	closest_blocks_f.close();
	cout << "PASSED" << endl;
	
	murderdir = -1;
	gridsize = -1;
	only_cold = false;
	
	derevolve = false;
	optical_depth_only = false;

	if ( argc==1 )
	{
		cout << "No arguments detected" << endl;
		helpandquit();
	}
	
	for ( ii=0 ; ii<3 ; ii++ )
	{
		for ( jj=0 ; jj<2 ; jj++ )
		{
			reflect[ii][jj] = false;
		}
	}

	ii = 1;

	raydir[0] = 0.00000;
	raydir[1] = 0.00000;
	raydir[2] = 0.00000;
	broaden_v = 0.;
	dilute_factor = 1.;
	
	while ( ii<argc )
	{
		if ( strcmp(argv[ii],"-f")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				inputfile = argv[ii];
				file_set = true;
				cout << "Inputting from  file: " << inputfile << endl;
			}
		}
		else if ( strcmp(argv[ii],"-o")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				outfile_initial = argv[ii];
				file_set = true;
				cout << "Outputting to file: '" << outfile_initial << "'" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-phi")==0 )
		{
			ii++;
			if ( ii>=argc-1 )
			{
				cout << argv[ii] << " needs two arguments\n";
				exit(0);
			}
			else
			{
				rotphi_range[0] = atof(argv[ii])*0.0174532925;
				ii++;
				rotphi_range[1] = atof(argv[ii])*0.0174532925;
				cout << "Rotating in phi direction (y,x plane) from " << rotphi_range[0] << " to " << rotphi_range[1] << " radians" << endl;
			}
			rotating = true;
		}
		else if ( strcmp(argv[ii],"-theta")==0 )
		{
			ii++;
			if ( ii>=argc-1 )
			{
				cout << argv[ii] << " needs two arguments\n";
				exit(0);
			}
			else
			{
				rottheta_range[0] = atof(argv[ii])*0.0174532925;
				ii++;
				rottheta_range[1] = atof(argv[ii])*0.0174532925;
				cout << "Rotating in theta direction (x,z plane) from " << rottheta_range[0] << " to " << rottheta_range[1] << " radians" << endl;
			}
			rotating = true;
		}
		else if ( strcmp(argv[ii],"-frames")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an arguments\n";
				exit(0);
			}
			else
			{
				rot_steps = atoi(argv[ii]);
				cout << rot_steps << " frames in rotation" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-n")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				n_ray = atoi(argv[ii]);
				cout << "Generating " << n_ray << "x" << n_ray << " grid of rays" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-killx")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				murderline = atof(argv[ii])*pc_in_cm;
				murderdir = 0;
				cout << "Killing ray at x=" << murderline << "cm" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-killy")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				murderline = atof(argv[ii])*pc_in_cm;
				murderdir = 1;
				cout << "Killing ray at y=" << murderline << "cm" <<  endl;
			}
		}
		else if ( strcmp(argv[ii],"-killz")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				murderline = atof(argv[ii])*pc_in_cm;
				murderdir = 2;
				cout << "Killing ray at z=" << murderline << "cm" <<  endl;
			}
		}
		else if ( strcmp(argv[ii],"-voff")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				v_off = atof(argv[ii]);
				cout << "Centering velocity at " << v_off << " km/s" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-cold")==0 )
		{
			only_cold = true;
			cout << "Cutting out all but cold/dense gas" << endl;
		}
		else if ( strcmp(argv[ii],"-refl")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				if ( strcmp(argv[ii],"x")==0 )
				{
					reflect[0][0] = true;
				}
				else if ( strcmp(argv[ii],"y")==0 )
				{
					reflect[1][0] = true;
				}
				else if ( strcmp(argv[ii],"z")==0 )
				{
					reflect[2][0] = true;
				}
				cout << "Reflecting in the " << argv[ii] << " direction" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-refh")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				if ( strcmp(argv[ii],"x")==0 )
				{
					reflect[0][1] = true;
				}
				else if ( strcmp(argv[ii],"y")==0 )
				{
					reflect[1][1] = true;
				}
				else if ( strcmp(argv[ii],"z")==0 )
				{
					reflect[2][1] = true;
				}
			}
		}
		else if ( strcmp(argv[ii],"-m")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				minlogdens = atof(argv[ii]);
				cout << "Setting log low density cutoff to " << minlogdens << endl;
			}
		}
		else if ( strcmp(argv[ii],"-l")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				cropbox[0][0] = atof(argv[ii]);
				cout << "Setting left truncation to " << cropbox[0][0] << endl;
			}
		}
		else if ( strcmp(argv[ii],"-r")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				cropbox[0][1] = atof(argv[ii]);
				cout << "Setting right truncation to " << cropbox[0][1] << endl;
			}
		}
		else if ( strcmp(argv[ii],"-t")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				cropbox[1][0] = atof(argv[ii]);
				cout << "Setting top truncation " << cropbox[0][0] << endl;
			}
		}
		else if ( strcmp(argv[ii],"-b")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				cropbox[1][1] = atof(argv[ii]);
				cout << "Setting bottom truncation " << cropbox[1][1] << endl;
			}
		}
		else if ( strcmp(argv[ii],"-x")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				raydir[0] = atof(argv[ii]);
				ray_set = true;
			}
		}
		else if ( strcmp(argv[ii],"-y")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				raydir[1] = atof(argv[ii]);
				ray_set = true;
			}
		}
		else if ( strcmp(argv[ii],"-z")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				raydir[2] = atof(argv[ii]);
				ray_set = true;
			}
		}
		else if ( strcmp(argv[ii],"-ix")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				testix = atoi(argv[ii]);
			}
		}
		else if ( strcmp(argv[ii],"-iy")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				testiy = atoi(argv[ii]);
			}
		}
		else if ( strcmp(argv[ii],"-h")==0 || strcmp(argv[ii],"--help")==0 || strcmp(argv[ii],"-help")==0 )
		{
			helpandquit();
		}
		else if ( strcmp(argv[ii],"-derev")==0 )
		{
			cout << "Removing circular velocities" << endl;
			derevolve = true;
		}
		else if ( strcmp(argv[ii],"-test")==0 )
		{
			cout << "Using test ICs" << endl;
			test_ics = true;
		}
		else if ( strcmp(argv[ii],"-dens")==0 )
		{
			cout << "Performing a straight column mass density sum" << endl;
			this_integrator = DENS_GRATOR;
		}
		else if ( strcmp(argv[ii],"-ndens")==0 )
		{
			cout << "Performing a straight column number density sum" << endl;
			this_integrator = DENS_GRATOR;
			ndens = true;
		}
		else if ( strcmp(argv[ii],"-Na")==0 )
		{
			cout << "Integrating NaI spectrum" << endl;
			this_integrator = SODIUM_GRATOR;
			if ( strcmp(outfile_initial,"densmap.dat")==0 )
			{
				outfile_initial = "line.out";
			}
		}
		else if ( strcmp(argv[ii],"-dist")==0 )
		{
			cout << "Performing a straight distance sum" << endl;
			this_integrator = DIST_GRATOR;
		}
		else if ( strcmp(argv[ii],"-vdel")==0 )
		{
			cout << "Calculating max/min velocities in line-of-sight" << endl;
			this_integrator = VDEL_GRATOR;
		}
		else if ( strcmp(argv[ii],"-mvel")==0 )
		{
			cout << "Mass-weighted velocities in line-of-sight" << endl;
			this_integrator = MVEL_GRATOR;
		}
		else if ( strcmp(argv[ii],"-testp")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				cout.precision(atoi(argv[ii]));
			}
		}
		else if ( strcmp(argv[ii],"-intersect")==0 )
		{
			cout << "Only outputting intersections" << endl;
			find_intersects = true;
		}
		else if ( strcmp(argv[ii],"-doublet")==0 )
		{
			cout << "Producing a doublet" << endl;
			produce_doublet = true;
		}
		else if ( strcmp(argv[ii],"-notherm")==0 )
		{
			cout << "We don't need no thermal broadening" << endl;
			cout << "We don't need no thought control" << endl;
			thermal_broad = false;
		}
		else if ( strcmp(argv[ii],"-depth")==0 )
		{
			cout << "Average the optical depth, not the line intensity" << endl;
			optical_depth_only = true;
		}
		else if ( strcmp(argv[ii],"-turb")==0 )
		{
			cout << "Will calculate turbulent broadening & density range" << endl;
			calcturb = true;
		}
		else if ( strcmp(argv[ii],"-fturb")==0 )
		{
			cout << "Will calculate turbulent broadening using an empirical formula" << endl;
			fit_turb = true;
			calcturb = true;
		}
		else if ( strcmp(argv[ii],"-dilute")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				dilute_factor = atof(argv[ii]);
				cout << "Diluting densities by a factor of " << dilute_factor << endl;
			}
		}
		else if ( strcmp(argv[ii],"-broaden")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				broaden_v = atof(argv[ii]);
				cout << "Applying Gaussian broading with FWHM " << broaden_v << " km/s" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-gridsize")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				gridsize = atof(argv[ii]);
				cout << "Forcing generated grid of rays to be " << gridsize << "pc in size" << endl;
				gridsize = gridsize*pc_in_cm;
			}
		}
		else if ( strcmp(argv[ii],"-reverse")==0 )
		{
			cout << "Flipping velocities" << endl;
			vel_reverse = true;
		}
		else
		{
			cout << "Unknown argument: " << argv[ii] << endl;
			helpandquit();
		}
		ii++;
	}
    cout << "test 2" << endl;
	closest_blocks_f.open("closestblocks.dat");
	closest_blocks_f.close();
	cout << "PASSED" << endl;

	if ( !file_set )
	{
		cout << "You must set an input file-name\n";
		helpandquit();
	}
	if ( !ray_set )
	{
		raydir[0] = 1.; // Assume firing in x direction
	}
	
	if ( broaden_v>0. )
	{
		//cout << "generating arrays" << endl;
		// Set up input, intermediate and output data for fourier transforms,
		// plus two plans (forward and backward) for each transform
		nthreads = omp_get_num_procs();
		cout << "nthreads=" << nthreads << endl;
		in = new t_darray[nthreads];
		out = new t_darray[nthreads];
		intermediate = new t_cmparray[nthreads];
		p1 = new fftw_plan[nthreads];
		p2 = new fftw_plan[nthreads];
		//cout << "Filling arrays" << endl;
		for ( ithread = 0 ; ithread<nthreads ; ithread++ )
		{
			in[ithread].data = new double[n_vel];
			out[ithread].data = new double[n_vel];
			//cout << "test" << ithread << " :" << in[ithread].data[0] << endl;
			intermediate[ithread].data = new fftw_complex[n_vel];

			p1[ithread] = fftw_plan_dft_r2c_1d(n_vel,in[ithread].data,intermediate[ithread].data,FFTW_ESTIMATE);
			p2[ithread] = fftw_plan_dft_c2r_1d(n_vel,intermediate[ithread].data,out[ithread].data,FFTW_ESTIMATE);
		}
		
		//cout << "making gaussian" << endl;
		// Produce our beautiful gaussian
		double broaden_v_sd = broaden_v/2.3548200;
		for ( int ii = 0 ; ii<n_vel ; ii++ )
		{
			//cout << ii << endl;
			//in[0].data[ii] = 1.;
			in[0].data[ii] = 1./(broaden_v_sd*sqrt(PI)*sqrt(2.))*exp(-(ii*ii)*(vel_w*vel_w)/(2.*broaden_v_sd*broaden_v_sd))+1./(broaden_v_sd*sqrt(PI)*sqrt(2.))*exp(-((n_vel-ii)*(n_vel-ii))*(vel_w*vel_w)/(2.*broaden_v_sd*broaden_v_sd));
		}
		
		//cout << "transforming" << endl;
		fftw_execute(p1[0]);
		
		//cout << "plugging in" << endl;
		gausstrans = new fftw_complex[n_vel];
		for ( int i = 0 ; i<n_vel ; i++ )
		{
			gausstrans[i][0] = intermediate[0].data[i][0];
			gausstrans[i][1] = intermediate[0].data[i][1];
		}
		
		// Setup complete!
	}
	
	cropint[0][0] = (int)floor(cropbox[0][0]*n_ray);
	cropint[0][1] = (int)ceil(cropbox[0][1]*n_ray);
	cropint[1][0] = (int)floor(cropbox[1][0]*n_ray);
	cropint[1][1] = (int)ceil(cropbox[1][1]*n_ray);
	
	if ( (testix<0 && testiy>=0) || (testix>=0 && testiy<0 ) )
	{
		cout << "Need to set both ray coordinates or neither!" << endl;
		helpandquit();
	}
	if ( testix>=0 && testiy>=0 )
	{
		cropint[0][0]=testix;
		cropint[0][1]=testix+1;
		cropint[1][0]=testiy;
		cropint[1][1]=testiy+1;
		//debug_mode = true;
		cout << "Only firing ray " << testix << "," << testiy << endl;
	}
    cout << "test 3" << endl;
	closest_blocks_f.open("closestblocks.dat");
	closest_blocks_f.close();
	cout << "PASSED" << endl;

	line_prof_t **line_data;;
	double **dens_map;
	double **dens_map2;
	double **dens_map3;
    
    line_data = new line_prof_t*[n_ray];
    dens_map = new double*[n_ray];
    dens_map2 = new double*[n_ray];
    dens_map3 = new double*[n_ray];
    
    for ( int i = 0 ; i<n_ray ; i++ )
    {
        line_data[i] = new line_prof_t[n_ray];
        dens_map[i] = new double[n_ray];
        dens_map2[i] = new double[n_ray];
        dens_map3[i] = new double[n_ray];
    }
    
    cout << "test 4" << endl;
	closest_blocks_f.open("closestblocks.dat");
	closest_blocks_f.close();
	cout << "PASSED" << endl;
	
	closest_blocks_f.open("closestblocks.dat");
	intersect_blocks_f.open("intersectblocks.dat");
	
	idir = 0;
	
	cout << "Reading" << endl;
	
	lores_galaxy = new flash_reader(inputfile);
	lores_galaxy->readDens();
	lores_galaxy->readTemp();
	if ( this_integrator==SODIUM_GRATOR
	  || this_integrator==VDEL_GRATOR 
	  || this_integrator==MVEL_GRATOR )
	{
		lores_galaxy->readVels();
		if ( calcturb )
		{
			lores_galaxy->readKFrac();
			lores_galaxy->readEint();
		}
	}
	lores_galaxy->setBoundaries(reflect);
//	lores_galaxy->readVels();
//	lores_galaxy->cm_to_pc();
	flash_data = lores_galaxy->getData();
	
	if ( derevolve )
	{
		unrevolve(flash_data);
	}
	
	if ( test_ics )
	{
		cout << "Engaging test ICs" << endl;
	// Abritrary ICs!
		for ( iib = 0 ; iib<flash_data->nb ; iib++ )
		{
			for ( ix = 0 ; ix < ncellx*ncellx*ncellx ; ix++ )
			{
				cc = iib*ncellx*ncellx*ncellx + ix;
				flash_data->dens[cc] = pow(10.,ix%ncellx+ix/(ncellx*ncellx)+(ix/ncellx)%ncellx);
				//flash_data->dens[cc] = 1.;
				flash_data->temp[cc] = 1.;
				
				flash_data->velx[cc] = 0.1;
				flash_data->vely[cc] = 0.1;
				flash_data->velz[cc] = 0.1;
				
				
				/*if ( flash_data->coordinates[iib*3+0]>1000 && flash_data->coordinates[iib*3+0]<2000
				&&   flash_data->coordinates[iib*3+1]>1000 && flash_data->coordinates[iib*3+1]<2000
				)
				{
					flash_data->velz[cc] = -2.e3;
					flash_data->velx[cc] = -2.e3;
					flash_data->dens[cc] = 500.e3;
					flash_data->temp[cc] = 1.e6;
				}
				if ( flash_data->coordinates[iib*3+2]>-2000 && flash_data->coordinates[iib*3+2]<-1000 )
				{
					flash_data->velz[cc] = 5.e3;
					flash_data->velx[cc] = 5.e3;
					flash_data->dens[cc] = 1000.e3;
					flash_data->temp[cc] = 1.e6;
				}*/
			}
		}
	}
	
	cout << "nb= " << flash_data->nb << endl;
	

	tracer = new ray_tracer(flash_data);
	lores_galaxy->close();
	
	if ( debug_mode )
	{	
		tracer->set_trace();

		cout << "Outputing ALLBLOCKS" << endl;
		f_all_blocks.open("allblocks.dat");
		for ( iib = 0 ; iib<flash_data->nb ; iib++ )
		{
			f_all_blocks << flash_data->coordinates[iib*3] << " " << flash_data->coordinates[iib*3+1] << " " << flash_data->coordinates[iib*3+2] << endl;
	//		cout << iib << " " << flash_data->velx[iib*(nx*nx*nx)]/1.e5 << " "  << flash_data->vely[iib*(nx*nx*nx)]/1.e5 << " "  << flash_data->velz[iib*(nx*nx*nx)]/1.e5 << endl;
		}
		f_all_blocks.close();

	}
	
	if ( murderdir!=-1 )
	{
		tracer->set_murder(murderline,murderdir);
	}
	
/*	line_f.open("line_test.dat");

	ray_gen = new ray_generator(flash_data,raydir,n_ray);
	ray_in->ib = -1;
	ray_in->r[0] = 0.;
	ray_in->r[1] = 0.;
	ray_in->r[2] = -1.e4;
	ray_in.v[0] = 0.;
	ray_in.v[1] = 0.;
	ray_in.v[2] = 1.;
	ray_gen->find_intersect(&ray_in);
	cout << ray_in->r[0] << " " << ray_in->r[1] << " " << ray_in->r[2] << " " << endl;
	cout << ray_in.v[0] << " " << ray_in.v[1] << " " << ray_in.v[2] << " " << endl;
	cout << ray_in->ib << endl;
	sodium_ray = new sodium_line_integrator(flash_data);
	tracer->fire_ray(sodium_ray,&ray_in);
	line_prof = sodium_ray->getProfile();
	for ( ii = 0 ; ii < n_vel ; ii ++ )
	{
		line_f << ii << " " << line_prof->line_depth[ii] << endl;
	}
	line_f.close();
	exit(0);*/

/*	for ( ix = 0 ; ix < n_ray ; ix++ )
	{
		for ( iy = 0 ; iy < n_ray ; iy++ )
		{
			for ( ii = 0 ; ii < n_vel ; ii ++)
			{
				line_data[ix][iy].line_depth[ii]=0.;
			}
		}
	}*/


	rotphi = 0.;
	rottheta = 0.;
//	for ( raydir[2] = 0. ; raydir[2] < 2.1 ; raydir[1]+=0.1 )
//	for ( view_ang = 0. ; view_ang < 3.2 ; view_ang+=0.2 )
	
//		raydir[0] = cos(view_ang);
//		raydir[1] = sin(view_ang);
//		raydir[2] = 2.*sin(view_ang);
//		raydir[2] = 0.;

	 
	 int rot_loop_i = 0;
	 char tempout[256];
	 
	 do
	 {
		 
		 if ( rotating )
		 {
			if ( rot_steps>1 )
			{
				rotphi = (rot_loop_i*(rotphi_range[1] - rotphi_range[0]))/(rot_steps-1)+rotphi_range[0];
				rottheta = (rot_loop_i*(rottheta_range[1] - rottheta_range[0]))/(rot_steps-1)+rottheta_range[0];
				raydir[0] = cos(rotphi)*cos(rottheta);
				raydir[1] = sin(rotphi);
				raydir[2] = -cos(rotphi)*sin(rottheta);
				int len = strlen(outfile_initial);
				for ( int i = 0 ; i<len-4 ; i++ )
				{
					tempout[i] = outfile_initial[i];
				}
				tempout[len-4] = '\0';
				sprintf(tempout,"%s%04d.dat",tempout,rot_loop_i);
				outfile = tempout;
				cout << "Outfile=" << outfile << endl;
			}
			else
			{
				rotphi = rotphi_range[0];
				rottheta = rottheta_range[0];
				outfile = outfile_initial;
				raydir[0] = cos(rotphi)*cos(rottheta);
				raydir[1] = sin(rotphi);
				raydir[2] = -cos(rotphi)*sin(rottheta);
			}
			cout << "This ray (x,y,z) = " << raydir[0] << " " << raydir[1] << " " << raydir[2] << endl;
		 }
		 else
		 {
			 outfile = outfile_initial;
		 }
		 
		 ray_gen = new ray_generator(flash_data,raydir,n_ray,debug_mode,gridsize);
		 
		 //		exit(0);
	 
		 ir = 0;
		 rays_done = false;
		 
		 integrate_time = (clock_t)0.;
		 raygen_time = (clock_t)0.;
		 
		 big_start = clock();
	 
	 
		 cout << "Performing integrations" << endl;
		 first_ray = true;
		 ixo = -1;
		 nline = 0;
		 
		 switch(this_integrator)
		 {
			 case DENS_GRATOR:DVEL_GRATOR:
				 for ( ix = 0 ; ix<n_ray ; ix++ )
				 {
					 for ( iy = 0 ; iy<n_ray ; iy++ )
					 {
						 dens_map[ix][iy] = minlogdens;
					 }
				 }
				 break;
			 case DIST_GRATOR:MVEL_GRATOR:
				 for ( ix = 0 ; ix<n_ray ; ix++ )
				 {
					 for ( iy = 0 ; iy<n_ray ; iy++ )
					 {
						 dens_map[ix][iy] = 0.;
						 if ( this_integrator==MVEL_GRATOR )
						 {
							 dens_map2[ix][iy] = 0.;
							 dens_map3[ix][iy] = 0.;
						 }
					 }
				 }
				 break;
			 case SODIUM_GRATOR:
				 if ( testix<0 || testiy<0 )
				 {
					 for ( ix = 0 ; ix<n_ray ; ix++ )
					 {
						 for ( iy = 0 ; iy<n_ray ; iy++ )
						 {
							 for ( int ii =0 ; ii<n_vel ; ii++ )
							 {
								line_data[ix][iy].line_depth[ii] = 0.;
							 }
						 }
					 }
				 }
				 break;
		 }
		 
				 
	
#pragma omp parallel for schedule(dynamic) default(none) shared(ndens,debug_mode,cropint,ray_gen,flash_data,debug_output,tracer,dens_map,dens_map2,dens_map3,n_ray,cout,intersect_blocks_f,find_intersects,this_integrator,line_map_f,broaden_v,produce_doublet,dilute_factor,outfile,calcturb,testix,testiy,line_data,vel_reverse,p1,p2,in,out,intermediate,gausstrans,only_cold,thermal_broad,optical_depth_only) private(ray_in,dens_ray,out_dens,iy,dist_ray,sodium_ray,vdel_ray,mvel_ray,ithread)
		 for ( ix = cropint[0][0] ; ix<cropint[0][1] ; ix++ )
		 {
		     ithread = omp_get_thread_num();
			 if ( ithread==0 )
			 {
				 cout << ix-cropint[0][0] << "/" << cropint[0][1]-cropint[0][0] << endl;
			 }
			 for (iy = cropint[1][0] ; iy<cropint[1][1] ; iy++ )
			 {
	//				ix = 14;
	//				iy = 0;
			 
	//				if ( cropbox[0][0]*n_ray<=ix &&  cropbox[0][1]*n_ray>=ix && cropbox[1][0]*n_ray<=iy &&  cropbox[1][1]*n_ray>=iy)
	
					 ray_in = ray_gen->generate(ix,iy);
	 /*				if ( ir%1000==0 )
					 {
						 cout << ir << "/" << n_ray*n_ray << "=" << (ir*100.)/(n_ray*n_ray) << "\%" << endl;
					 }*/
	
					 if ( ray_in->ib!=-1 )
					 {
					 
						 if ( !find_intersects )
						 {
							 if ( debug_output )
							 {
							  ray_in->ib = tracer->find_child(ray_in->ib,ray_in->r);
							 }
	
							 switch(this_integrator)
							 {
								 case DENS_GRATOR:
									 dens_ray = new dens_integrator(flash_data);
									 if ( ndens )
									 {
										dens_ray->set_numberdens();
									 }
									 if ( debug_mode )
									 {
										 dens_ray->set_trace();
									 }
									 if ( dilute_factor!=1. )
									 {
										 dens_ray->dilute(dilute_factor);
									 }
									 if ( only_cold )
									 {
										 dens_ray->set_onlycold();
									 }
									 tracer->fire_ray(dens_ray,ray_in);
									 out_dens = dens_ray->getColumnDensity(); // units = (g/cm^3) * pc
									 //out_dens*=pc_in_cm;
									 
									 dens_map[ray_in->iix][ray_in->iiy] = log10(out_dens);
									 delete dens_ray;
									 break;
								 case DIST_GRATOR:
									 dist_ray = new distance_integrator();
									 tracer->fire_ray(dist_ray,ray_in);
									 dens_map[ray_in->iix][ray_in->iiy] = dist_ray->getDist();;
									 delete dist_ray;
									 break;
								 case VDEL_GRATOR:
									 vdel_ray = new vdel_integrator(flash_data);
									 if ( calcturb )
									 {
										 vdel_ray->doturb();
									 }
									 tracer->fire_ray(vdel_ray,ray_in);
									 dens_map[ray_in->iix][ray_in->iiy] = vdel_ray->getvdel();
									 delete vdel_ray;
									 break;
								 case MVEL_GRATOR:
									 mvel_ray = new vave_integrator(flash_data);
									 if ( only_cold )
									 {
										 mvel_ray->set_onlycold();
									 }
									 tracer->fire_ray(mvel_ray,ray_in);
									 dens_map[ray_in->iix][ray_in->iiy] = mvel_ray->getv();
									 dens_map2[ray_in->iix][ray_in->iiy] = mvel_ray->getp();
									 dens_map3[ray_in->iix][ray_in->iiy] = mvel_ray->getr();
									 delete mvel_ray;
									 break;
								 case SODIUM_GRATOR:
									 sodium_ray = new sodium_line_integrator(flash_data);
									 if ( produce_doublet )
									 {
										 sodium_ray->produceDoublets();
									 }
									 if ( dilute_factor!=1. )
									 {
										 //cout << "Diluting" << endl;
										 sodium_ray->dilute(dilute_factor);
									 }
									 if ( calcturb )
									 {
										 //cout << "Turbing" << endl;
										 sodium_ray->doturb();
									 }
									 if ( calcturb )
									 {
										 //cout << "Turbing" << endl;
										 sodium_ray->dofit_turb();
									 }
									 if ( vel_reverse )
									 {
										 //cout << "Reversing vel" << endl;
										 sodium_ray->reverseVels();
									 }
									 if ( !thermal_broad )
									 {
										 sodium_ray->noThermal();
									 }
									 tracer->fire_ray(sodium_ray,ray_in);
									 if ( broaden_v>0. )
									 {
										//sodium_ray->broadenfwhm(broaden_v);
										//cout << "Broadening" << endl;
										sodium_ray->broaden_fftw(ithread,p1,p2,in,intermediate,out,gausstrans);
									 }
									 if ( testix<0 || testiy<0 )
									 {
										 if ( !optical_depth_only )
										 {
											sodium_ray->convert_intensity();
										 }
										 for ( int ii = 0 ; ii < n_vel ; ii ++ )
										 {
											line_data[ix][iy].line_depth[ii] = sodium_ray->getProfile()->line_depth[ii];
											if ( sodium_ray->getProfile()->line_depth[ii]!=sodium_ray->getProfile()->line_depth[ii] )
											{
												cout << "NANNED" << endl;
											}
										 }
									 } else
									 {
										 if ( !optical_depth_only )
										 {
											sodium_ray->convert_intensity();
										 }
										 line_map_f.open(outfile);
										 sodium_ray->dumpProfileStr(line_map_f);
										 line_map_f.close();
									 }
									 delete sodium_ray;
									 break;
							 }
							 /*dist_ray = new distance_integrator();
							 tracer->fire_ray(dist_ray,ray_in);
							 out_dens = dist_ray->getDist();
							 
							 dens_map[ray_in->iix][ray_in->iiy] = out_dens;
							 delete dist_ray;*/
						 }
						 else
						 {
							 #pragma omp critical
							 {
								 // Only interested in intersections 
								 intersect_blocks_f << ray_in->r[0] << " " << ray_in->r[1] << " " << ray_in->r[2] << " " << ray_in->ib << endl;
							 }
						 }
	
				 
				 }
				 else
				 {
					 // Missed the box
					 if ( this_integrator==SODIUM_GRATOR )
					 {
						 for ( int ii = 0 ; ii < n_vel ; ii ++ )
						 {
							line_data[ix][iy].line_depth[ii] = 1.; // i.e. all intensity goes through, i.e. transparent
						 }
					 }
				 }
			 }
		 }
		 
		 if ( !find_intersects )
		 {
			 
			 
			 /*for ( int ix = 0 ; ix<n_ray ; ix++ )
			 {
				 if ( cropbox[0][0]*n_ray<=ix &&  cropbox[0][1]*n_ray>=ix )
				 {
					 for ( int iy = 0 ; iy<n_ray ; iy++ )
					 {
						 if ( cropbox[1][0]*n_ray<=iy &&  cropbox[1][1]*n_ray>=iy )
						 {
							 dens_map_s << ix << " " << iy << " " << max(dens_map[ix][iy],minlogdens) << endl;
						 }
					 }
					 dens_map_s << endl;
				 }
			 }*/
	
			 if ( 	this_integrator==DENS_GRATOR ||
					this_integrator==DIST_GRATOR ||
					this_integrator==VDEL_GRATOR )
			 {
	 
				 dens_map_s.str("");
				 for ( ix = cropint[0][0] ; ix<cropint[0][1] ; ix++ )
				 {
					 for (iy = cropint[1][0] ; iy<cropint[1][1] ; iy++ )
					 {
						 dens_map_s << ix << " " << iy << " " << max(dens_map[ix][iy],minlogdens) << endl;
					 }
					 dens_map_s << endl;
				 }
			 
				 dens_map_f.open(outfile);
				 dens_map_f << dens_map_s.str().data();
				 dens_map_f.close();
			 }
			 
			 if ( this_integrator==MVEL_GRATOR )
			 {
				 dens_map_s.str("");
				 for ( ix = cropint[0][0] ; ix<cropint[0][1] ; ix++ )
				 {
					 for (iy = cropint[1][0] ; iy<cropint[1][1] ; iy++ )
					 {
						 dens_map_s << ix << " " << iy << " " << dens_map[ix][iy] << " " << dens_map2[ix][iy] << " " << dens_map3[ix][iy] << endl;
					 }
					 dens_map_s << endl;
				 }
			 
				 dens_map_f.open(outfile);
				 dens_map_f << dens_map_s.str().data();
				 dens_map_f.close();
			 }
			 
			 if (	this_integrator==SODIUM_GRATOR &&
					(testix<0 || testiy<0 ) )
				{
					cout << "Consolodating and dumping line..." << endl;
					line_prof_t outline;
					int n_rays_actual;
					
					n_rays_actual = 0;
					
					for ( ix = cropint[0][0] ; ix<cropint[0][1] ; ix++ )
					{
						 for (iy = cropint[1][0] ; iy<cropint[1][1] ; iy++ )
						 {
							 n_rays_actual++;
						 }
					}
					//#pragma omp parallel for schedule(dynamic) default(none) shared(outline,line_data) private(ix,iy,ii)
					for ( int ii = 0 ; ii<n_vel ; ii++ )
					{
						outline.line_depth[ii] = 0.;
						for ( ix = cropint[0][0] ; ix<cropint[0][1] ; ix++ )
						{
							 for (iy = cropint[1][0] ; iy<cropint[1][1] ; iy++ )
							 {
								 outline.line_depth[ii]+=line_data[ix][iy].line_depth[ii];
							 }
						}
						outline.line_depth[ii]/=n_rays_actual;
						//outline.line_depth[ii]/=outline.line_depth[0]; // Only works if we assume line ->0 at edges
					}
					
					cout << "Outputting to file '" << outfile << "'" << endl;
					line_map_f.open(outfile);
					for ( int jj = 0 ;  jj<n_vel ; jj++ )
					{
						line_map_f << (jj-n_vel/2)*vel_w+v_off << " " << outline.line_depth[jj] << endl;
					}
					line_map_f.close();
				}
		 }
		
		
	/*	cout << "Dumping lines" << endl;
		
		line_f.open("line_map.dat"); //,ios::binary
		line_f << n_ray << endl;
		line_f << n_vel << endl;
		for ( ii = 0 ; ii < n_vel ; ii ++)
		{
			for ( ix = 0 ; ix < n_ray ; ix++ )
			{
				for ( iy = 0 ; iy < n_ray ; iy++ )
				{
					line_f << line_data[ix][iy].line_depth[ii] << endl;
				}
			}
		}
	//    line_f.write ((char*)&line_data, sizeof (line_data));
	
		line_f.close();
	
	
		line_f.open("line_mess.dat");
		for ( ix = 0 ; ix < n_ray ; ix++ )
		{
			for ( iy = 0 ; iy < n_ray ; iy++ )
			{
				for ( ii = 0 ; ii < n_vel ; ii ++)
				{
					line_f << ii-n_vel/2 << " " << line_data[ix][iy].line_depth[ii] << " " << ix+iy*n_ray << endl;
				}
			}
		}
		line_f.close();*/
	
		
	//	free(lores_galaxy);
		cout << "Closing files" << endl;
		closest_blocks_f.close();
		intersect_blocks_f.close();
		
		if ( rotating )
		{
			rot_loop_i++;
		}
	} while ( rotating && rot_loop_i<rot_steps );
	
}

void writebox(ofstream *f, double cent[3], double dx[3])
{
	double z;
	// Two faces
	//for ( int face = -1 ; face<=1 ; face+=2 )
	int face = 0;
	{
		z = cent[2]+face*dx[2]/2.;
		*f << cent[0]-dx[0]/2. << " " << cent[1]-dx[1]/2. << " " << z << endl;
		*f << cent[0]-dx[0]/2. << " " << cent[1]+dx[1]/2. << " " << z << endl;
		*f << cent[0]+dx[0]/2. << " " << cent[1]+dx[1]/2. << " " << z << endl;
		*f << cent[0]+dx[0]/2. << " " << cent[1]-dx[1]/2. << " " << z << endl;
		*f << cent[0]-dx[0]/2. << " " << cent[1]-dx[1]/2. << " " << z << endl;
		*f << endl << endl;
	}
	
	// "Rods" inbetween
	/*for ( int fx = -1 ; fx<=1 ; fx+=2 )
	{
		for ( int fy = -1 ; fy<=1 ; fy+=2 )
		{
			*f << cent[0]+fx*dx[0]/2. << " " << cent[1]+fy*dx[1]/2. << " " << cent[2]-dx[2]/2. << endl;
			*f << cent[0]+fx*dx[0]/2. << " " << cent[1]+fy*dx[1]/2. << " " << cent[2]+dx[2]/2. << endl;
			*f << endl << endl;
		}
	}*/
}
