import sys
import os

import fitgauss

#datafilename_in = sys.argv[1]
plot_fit_in = True
#suffix = "central" # 20 kpc instead of 100 kpc
#suffix = "doublet"
#suffix = "broaden"
suffix = ""
#suffix = "derev"

#os.system("./raytrace -f /disc42/williams/flash/galreal/0153/ULIRG_0153_hdf5_chk_0020 -Na -phi 0 350 -theta 0.001 0.001 -frames 36 -n 128 -o lineouts/fixedsizerot"+suffix+"A.dat -gridsize 100.e3 -reverse -killy 0. -broaden 65")
#os.system("./raytrace -f /disc42/williams/flash/galreal/0153/ULIRG_0153_hdf5_chk_0020 -Na -phi 0 350 -theta 45.001 45.001 -frames 36 -n 128 -o lineouts/fixedsizerot"+suffix+"B.dat -gridsize 100.e3 -reverse -killy 0. -broaden 65")
#os.system("./raytrace -f /disc42/williams/flash/galreal/0153/ULIRG_0153_hdf5_chk_0020 -Na -phi 0 0 -theta 45.001 45.001 -frames 1 -n 128 -o test1.dat -gridsize 100.e3 -reverse -killy 0.")
#os.system("./raytrace -f /disc42/williams/flash/galreal/0153/ULIRG_0153_hdf5_chk_0020 -Na -phi 0 0 -theta 45.001 45.001 -frames 1 -n 128 -o test2.dat -gridsize 100.e3 -reverse -killy 0. -broaden 65")
#os.system("./raytrace -f /disc42/williams/flash/galreal/0153/ULIRG_0153_hdf5_chk_0020 -Na -phi 0 350 -theta 90.001 90.001 -frames 36 -n 128 -o lineouts/fixedsizerot"+suffix+"C.dat -gridsize 100.e3 -reverse -killy 0. -broaden 65")


#def double_gauss_fit(datafilename,plot_fit):
	#return 1,2

#fitgauss.
#fit_prams = []
#datafilename_in = "lineouts/fixedsizerotC0020.dat"

#f = open(datafilename_in,'r')
#line_data = [[float(y) for y in x.split()] for x in f.readlines()]
#f.close()

#f = open("test.dat",'w')
#for ii in range(2,len(line_data)-2):
	#d1 = line_data[ii+1][1]-line_data[ii-1][1]
	#d2 = line_data[ii+1][1]-2*line_data[ii][1]+line_data[ii-1][1]
	#f.write(str(ii)+" "+str(line_data[ii][1])+" "+str(d1)+" " +str(d2)+'\n')
#f.close()


#fitgauss.double_gauss_fit(datafilename_in,plot_fit_in)
#sys.exit()

#for letter in ["A","B","C"]:
for letter in ["B"]:
#for letter in [""]:
	fit_prams_single = []
	fit_prams_double = []
	fit_prams_triple = []
	fit_prams_selected = []
	for ii in [0,3,6,9,12,15,18,21]:
#	for ii in range(0,36,1): #36
		datafilename_in = "lineouts/fixedsizerot"+suffix+letter+str(ii).rjust(4,'0')+".dat"
		#datafilename_in = "lineouts/0148_0020_turb_"+str(ii).rjust(4,'0')+".dat"
		single_data_str = fitgauss.single_gauss_fit(datafilename_in,plot_fit_in,suffix)
		fit_prams_single.append(single_data_str)
		double_data_str = fitgauss.double_gauss_fit(datafilename_in,plot_fit_in,suffix)
		fit_prams_double.append(double_data_str)
		#triple_data_str = fitgauss.symmetric_triple_gauss_fit(datafilename_in,plot_fit_in)
		#fit_prams_triple.append(triple_data_str)
		# Make the selection!
		double_data = [float(x) for x in double_data_str.split(" ")]
		#print double_data
		v1 = double_data[4]
		v2 = double_data[6]
		#print "***************************"
		#print ii,v1,v2,abs((v1+v2)/(v1-v2))
		#print "***************************"
		#print v1,v2,abs((v1+v2)/(v1-v2))
		if ( abs((v1+v2)/(v1-v2))<0.5 ):
			# I'm a double-line!
			fit_prams_selected.append(str((float(double_data[0])+float(double_data[2]))/2.)+" "+str(abs(v1-v2)/2.)+" "+str(abs((float(double_data[8])+float(double_data[10]))/2.))+" -1\n")
			#print str((float(double_data[2])+float(double_data[4]))/2.)+" "+str(abs(v1-v2)/2.)+" "+str(abs((float(double_data[10])+float(double_data[12]))/2.))
		else:
			# Just fit one line to everything
			single_data = single_data_str.split(" ")
			fit_prams_selected.append(single_data[0]+" "+single_data[2]+" "+single_data[4]+" +1\n")
	ofile_single = open("width_angle"+suffix+letter+"_single.dat",'w')
	ofile_double = open("width_angle"+suffix+letter+"_double.dat",'w')
	#ofile_triple = open("width_angle"+suffix+letter+"_triple.dat",'w')
	ofile_selected = open("width_angle"+suffix+letter+"_selected.dat",'w')
	jj = 0
	#for ii in range(0,36,1): #36
	for ii in [0,3,6,9,12,15,18,21]:
		#ofile.write(str(ii*10)+" "+str(fit_prams[ii][0])+" "+str(fit_prams[ii][1])+'\n')
		ofile_single.write(str(ii*10)+" "+fit_prams_single[jj])
		ofile_double.write(str(ii*10)+" "+fit_prams_double[jj])
		#ofile_triple.write(str(ii*10)+" "+fit_prams_triple[jj])
		ofile_selected.write(str(ii*10)+" "+fit_prams_selected[jj])
		jj = jj + 1
	ofile_single.close()
	ofile_double.close()
	#ofile_triple.close()
	ofile_selected.close()

#print "Outflow line-width=",outflow_width_out," +/- ",outflow_width_error_out," km/s"
