# Plot slices in wavelength/velocity space
import os


file = open("line_map.dat",'r')
n_ray = int(file.readline())
n_vel = int(file.readline())
datastr = file.readlines()
file.close()

data = [float(x) for x in datastr]

names = ""

print "Building frames"

dmax = -1.
for kk in range(0,n_ray*n_ray*n_vel):
	if ( data[kk]>dmax ):
		dmax = data[kk]

print "Dmax=",dmax

#for ff in range (0,n_vel,100):
for ff in range (450,700):
	print str(ff)+"/"+str(n_vel)
	if ( True ):
		file = open("plotdata.dat",'w')
		for ii in range(0,n_ray):
			for jj in range(0,n_ray):
				file.write(str(ii)+" "+str(jj)+" "+str(data[ii*n_ray+jj+ff*n_ray*n_ray])+"\n")
			file.write("\n")
		file.close()
		file = open("plotscript.gnu",'w')
		file.write("set pm3d map\n")
		file.write("set term gif\n")
		file.write("set output 'frame"+str(ff)+".gif'\n")
#		file.write("set zrange[0:"+str(dmax)+"]\n")
#		file.write("set logscale z\n")
#		file.write("set zrange[1e-60:1e-20]\n")
		file.write("splot 'plotdata.dat' u 1:2:3\n")
		file.close()
		os.system("gnuplot plotscript.gnu\n")
	names = names + "frame"+str(ff)+".gif "

print "Building animation"
os.system("convert "+names+" -loop 0 slice.gif")
