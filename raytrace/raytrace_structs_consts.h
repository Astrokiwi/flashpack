#define PI 3.14159265

int const ncellx = 8; // Each block is 8x8x8 cells
//int const ncellx = 16; // Each block is 16x16x16 cells
int const bad_blocks = 10000000; // Max blocks a ray can propagate through before we decide this is broken
double const pc_in_cm = 3.08568025e18;
double const amu_in_g = 1.66053886e-24;
double const gcm3_in_Msunpc3 = 1.4771869e22;

int const n_vel = 2000; // +/- 1000 velocity bins in line profile
//double const vel_w = 1.; // Width of each velocity bin in km/s - big gal
double const vel_w = 2.; // Width of each velocity bin in km/s - central gal
double v_off = 0.;

double const sodium_per_H = 1.22e-6; // Fujita et al 2009, from Martin 2005
double const H_per_dens = .75/(1.66053886e-24); // Gas is 3/4 hydrogen by mass, assume mostly ionised and primordial (divide by 1 amu in cm)
double const komum = 88341259.4; // Bolztmann's constant over (mean molecular mass * 1 amu) - used for line profile width (in cgs)
double const sodium_wave_l = 5890.e-8; // Wavelength of the line in cm
double const sodium_s = 2.654e-2 * 0.6; // Sodium I cross-section, integrated over line, in cm^2 s

double const sodium_dens_cut = 1.e-21; // For turbulent gas, above 10^(-21) g/cm^3 cools very quickly, assume there is some Na in there
double const turb_b = 1./3.; // b=1/3 for solenoidal forcing, b=1 for compressive forcing
double const Na_doublet_dv = -305.4; // in km/s
double const doublet_str_ratio = 1./1.3; // Second line in doublet has 1.3 times lower equivalent width
double const t_const = 0.594*(5./3.-1.)/(8.3145e7); // Molecular mass*(gamma-1)/(gas constant)  (g/mol, unitless, erg/mol/K)

double const turb_fit_k = 1.1466747095;//1.13075312932;
double const turb_fit_m = 0.342745350663;//0.513985388966;
double const turb_fit_logT0 = 4.69897;

/////////////

#define MAX_STRING_LENGTH 80 // As defined in FLASH3.0
#define TRUE 1
#define FALSE 0

struct sim_info_t 
{
  int file_format_version;
  char setup_call[400];
  char file_creation_time[MAX_STRING_LENGTH];
  char flash_version[MAX_STRING_LENGTH];
  char build_date[MAX_STRING_LENGTH];
  char build_dir[MAX_STRING_LENGTH];
  char build_machine[MAX_STRING_LENGTH];
  char cflags[400];
  char fflags[400];
  char setup_time_stamp[MAX_STRING_LENGTH];
  char build_time_stamp[MAX_STRING_LENGTH];
};

struct flash_data_t {
	int nb;
	int *gid,*nodetype;
	int *iproc;
	double *blocksize,*coordinates;
	double *dens,*temp,*pres;
	double *gamc;
	double *eint,*ener;
	double *kturb,*lturb,*kfrac,*mut,*kint,*thfrac;
	double *velx,*vely,*velz;

	sim_info_t *sim_info;
		
	int *lrefine;
	bool reflect[3][2];
};


struct ray_t {
	int ib;
	double r[3];
	double v[3];
	int iix,iiy;
};

struct dimens_t {
	double rlow[3];
	double rhigh[3];
};

struct line_prof_t {
//	double intensity[n_vel];  // I_nu(0) normalised to 1 as in Fujita et al 2009
	long double line_depth[n_vel];
};

typedef struct t_darray
{
	double *data;
} t_darray;

typedef struct t_cmparray
{
	fftw_complex *data;
} t_cmparray;
