#include <cstdlib>

class flash_reader {
	public:
		flash_reader(char *inName);
//		void readCellScalars(char *data_name);
		void readDens();
		void readTemp();
		void readGamc();
		void readPres();
		void readVels();
		void readEint();
		void readEner();
		void readKTurb();
		void readKint();
		void readLTurb();
		void readMut();
		void readKFrac();
		void readThFrac();
		void readRefine();
		void readSimInfo();
		void readMetaArrays();
		void close();
		
		void cm_to_pc();
		void L_cm_to_pc();
		void to_msunpc3();
		
		dimens_t getSize(); // Get physical size of the dataset
		void setBoundaries(bool ref[3][2]);
		
		flash_data_t* getData();
		
	private:
		char *fileName;
		hid_t	flashFile; // FLASH3.0 format input file ID
		
		int*  readIntArray(char *data_name);
		double* readDoubleArray(char *data_name);
		
		void getDims();
		
		flash_data_t flash_data;
		
};

flash_reader::flash_reader(char *inName) {
//	printf("Attempting to open file '%s'\n",inName);
	flashFile = H5Fopen(inName,H5F_ACC_RDONLY,H5P_DEFAULT);
	
	if( (int) flashFile < 0)
	{
		printf("ERROR FILE NOT FOUND \n");
		exit(0);
	}
	
//	printf("Reading block data\n");

	getDims();
	
//	printf("File has correct dimension\n");

	if( (int) flashFile < 0)
	{
		printf("ERROR FILE HAS INCORRECT DIMENSION \n");
		exit(0);
	}
	
	flash_data.gid = readIntArray("gid");
	flash_data.nodetype = readIntArray("node type");
	flash_data.blocksize = readDoubleArray("block size");
	flash_data.coordinates = readDoubleArray("coordinates");
}

void flash_reader::setBoundaries(bool ref[3][2])
{
	for ( int ii=0 ; ii<3 ; ii++ )
	{
		for ( int jj = 0 ; jj<2 ; jj++ )
		{
			flash_data.reflect[ii][jj] = ref[ii][jj];
			//cout << ii << " " << jj << " " << flash_data.reflect[ii][jj] << endl;
		}
	}
}

dimens_t flash_reader::getSize()
{
	dimens_t size;
	double rl,rh;
	
	for ( int ii = 0 ; ii<3 ; ii++ )
	{
		size.rlow[ii] = flash_data.coordinates[0+ii*3]-flash_data.blocksize[0+ii*3]/2.;
		size.rhigh[ii] = flash_data.coordinates[0+ii*3]+flash_data.blocksize[0+ii*3]/2.;
	}
	
	for ( int ib = 1 ; ib<flash_data.nb ; ib++)
	{
		for ( int ii = 0 ; ii<3 ; ii++ )
		{
			rl = flash_data.coordinates[ib+ii*3]-flash_data.blocksize[ib+ii*3]/2.;
			rh = flash_data.coordinates[ib+ii*3]+flash_data.blocksize[ib+ii*3]/2.;
			
			if ( rl<size.rlow[ii] )
			{
				size.rlow[ii] = rl;
			}
			if ( rh>size.rhigh[ii] )
			{
				size.rhigh[ii] = rh;
			}
		}
	}
	return size;
}

void flash_reader::readSimInfo()
{
	hid_t idSimInfo;
	hid_t idSpace; // Space (i.e. dimensions) of last opened dataset
	hsize_t dimens_1d;
	hsize_t maxdimens_1d;
    hid_t smax,s400;
    hid_t memspace;
    hid_t status;
    hid_t idType;
    
    cout << "WARNING - readSimInfo() doesn't work yet!" << endl;

	// Read in simulation info struct
	idSimInfo = H5Dopen(flashFile,"sim info",H5P_DEFAULT);
	idSpace = H5Dget_space(idSimInfo);
//	memspace = H5Screate_simple(0,NULL,NULL);
	H5Sget_simple_extent_dims(idSpace,&dimens_1d,&maxdimens_1d);
//	memspace = H5Screate_simple(1,&scalarShape,NULL); // Probably wrong
// Create the "type" for the struct, so hdf5 knows how to read things in	
		idType = H5Tcreate(H5T_COMPOUND,sizeof(sim_info_t));
	memspace = H5Screate_simple(1,&dimens_1d,NULL);
    smax = H5Tcopy(H5T_C_S1);
    s400 = H5Tcopy(H5T_C_S1);
	H5Tset_size(smax, MAX_STRING_LENGTH);
	H5Tset_size(s400, 400);
	H5Tinsert(idType,"file_format_version",HOFFSET(sim_info_t,file_format_version),H5T_NATIVE_INT);
	H5Tinsert(idType,"setup_call",HOFFSET(sim_info_t,setup_call),s400);
	H5Tinsert(idType,"file_creation_time",HOFFSET(sim_info_t,file_creation_time),smax);
	H5Tinsert(idType,"flash_version",HOFFSET(sim_info_t,flash_version),smax);
	H5Tinsert(idType,"build_date",HOFFSET(sim_info_t,build_date),smax);
	H5Tinsert(idType,"build_dir",HOFFSET(sim_info_t,build_dir),smax);
	H5Tinsert(idType,"build_machine",HOFFSET(sim_info_t,build_machine),smax);
	H5Tinsert(idType,"cflags",HOFFSET(sim_info_t,cflags),s400);
	H5Tinsert(idType,"fflags",HOFFSET(sim_info_t,fflags),s400);
	H5Tinsert(idType,"setup_time_stamp",HOFFSET(sim_info_t,setup_time_stamp),smax);
	H5Tinsert(idType,"build_time_stamp",HOFFSET(sim_info_t,build_time_stamp),smax);
//	printf("sim_info struct type built!\n");
	cout << "Built the struct" << endl;
	status=H5Dread(idSimInfo,idType,memspace,idSpace,H5P_DEFAULT,&(flash_data.sim_info));
//	printf("This file was made with version %s\n",(*sim_info).flash_version);
	cout << "Version = " << flash_data.sim_info->flash_version << endl;
	H5Dclose(idSimInfo);
}

void flash_reader::readDens()
{
	printf("Reading density data\n");
	flash_data.dens = readDoubleArray("dens");
}

void flash_reader::readMetaArrays()
{
	printf("Reading processor number\n");
	flash_data.iproc = readIntArray("processor number");
}

void flash_reader::readPres()
{
	printf("Reading pressure data\n");
	flash_data.pres = readDoubleArray("pres");
}

void flash_reader::readTemp()
{
	printf("Reading temperature data\n");
	flash_data.temp = readDoubleArray("temp");
}

void flash_reader::readThFrac()
{
	printf("Reading thermal fraction\n");
	flash_data.thfrac = readDoubleArray("nkfc");
}
void flash_reader::readGamc()
{
	printf("Reading gamma data\n");
	flash_data.gamc = readDoubleArray("gamc");
}

void flash_reader::readEint()
{
	printf("Reading internal energy data\n");
	flash_data.eint = readDoubleArray("eint");
}

void flash_reader::readEner()
{
	printf("Reading total energy data\n");
	flash_data.ener = readDoubleArray("ener");
}

void flash_reader::readRefine()
{
	printf("Reading refinement data\n");
	flash_data.lrefine = readIntArray("refine level");
}

void flash_reader::readVels()
{
	printf("Reading velocity data\n");
	flash_data.velx = readDoubleArray("velx");
	flash_data.vely = readDoubleArray("vely");
	flash_data.velz = readDoubleArray("velz");
}

void flash_reader::readKTurb()
{
	printf("Reading turbulent K\n");
	flash_data.kturb = readDoubleArray("k   ");
}

void flash_reader::readKint()
{
	printf("Reading initial turbulent K\n");
	flash_data.kint = readDoubleArray("kint");
}

void flash_reader::readLTurb()
{
	printf("Reading turbulent L\n");
	flash_data.lturb = readDoubleArray("l   ");
}

void flash_reader::readMut()
{
	printf("Reading turbulent viscosity\n");
	flash_data.mut = readDoubleArray("mut ");
}

void flash_reader::readKFrac()
{
	printf("Reading turbulent K fraction\n");
	flash_data.kfrac = readDoubleArray("kfrc");
}


/*void flash_reader::readCellDens(char *data_name)
{
	printf("Reading %s data\n",data_name);
	flash_data.cellScalar = readDoubleArray(data_name);
}*/

void flash_reader::close() {
	if ( (int) flashFile>=0 )
	{
		H5Fclose(flashFile);
	}
}

void flash_reader::to_msunpc3()
{
	for ( int i=0 ; i<flash_data.nb*ncellx*ncellx*ncellx ; i++ )
	{
		flash_data.dens[i] = flash_data.dens[i]*gcm3_in_Msunpc3;
	}
}


void flash_reader::cm_to_pc()
{
	for ( int i=0 ; i<flash_data.nb*3 ; i++ )
	{
		flash_data.coordinates[i] = flash_data.coordinates[i]/pc_in_cm;
		flash_data.blocksize[i] = flash_data.blocksize[i]/pc_in_cm;
	}
}

void flash_reader::L_cm_to_pc()
{
	for ( int i=0 ; i<flash_data.nb*ncellx*ncellx*ncellx ; i++ )
	{
		flash_data.lturb[i] = flash_data.lturb[i]/pc_in_cm;
	}
}

flash_data_t* flash_reader::getData()
{
	return &flash_data;
}
	
double* flash_reader::readDoubleArray(char *data_name)
{
	hid_t dataset,dataspace,memspace,status;
	int rank;
	hsize_t *dims_out,*max_dims;
//	hid_t dapl_id;

	double* data;
	
	int data_size;

	if( (int) flashFile < 0)
	{
		printf("CAN NOT READ %s, FILE NOT OPEN\n",data_name);
		exit(0);
	}

//	dataset = H5Dopen(flashFile, data_name); // Works on some machines
	dataset = H5Dopen(flashFile, data_name,H5P_DEFAULT); // Works on others
	dataspace = H5Dget_space(dataset);
	rank = H5Sget_simple_extent_ndims(dataspace);
//	dims_out = (hsize_t *) (malloc(rank*sizeof(hsize_t)));
//	max_dims = (hsize_t *) (malloc(rank*sizeof(hsize_t)));
	dims_out = new hsize_t[rank];
	max_dims = new hsize_t[rank];
	
	status = H5Sget_simple_extent_dims(dataspace,dims_out,max_dims);
	
	data_size = 1;
	for ( int i=0; i<rank ; i++ )
	{
		data_size*=(int)dims_out[i];
	}
	
//	data = (double *) (malloc(data_size*sizeof(double)));
	data = new double[data_size];
	
	memspace = H5Screate_simple(rank,dims_out,NULL);
	status = H5Dread(dataset,H5T_NATIVE_DOUBLE,memspace,dataspace,H5P_DEFAULT,data);
	H5Sclose(memspace);
	H5Sclose(dataspace);
	H5Dclose(dataset);
	
	free(max_dims);
	free(dims_out);
	
	return data;
}

int* flash_reader::readIntArray(char *data_name)
{
	hid_t dataset,dataspace,memspace,status;
	int rank;
	hsize_t dims_out[4],max_dims[4];
//	hid_t dapl_id;
	
	int* data;
	
	int data_size;
	
	if( (int) flashFile < 0)
	{
		printf("CAN NOT READ %s, FILE NOT OPEN\n",data_name);
		exit(0);
	}

//	dataset = H5Dopen(flashFile, data_name); // Works on some machines
	dataset = H5Dopen(flashFile, data_name,H5P_DEFAULT); // Works on others
	dataspace = H5Dget_space(dataset);
	rank = H5Sget_simple_extent_ndims(dataspace);
	status = H5Sget_simple_extent_dims(dataspace,dims_out,max_dims);
	
	data_size = 1;
	for ( int i=0; i<rank ; i++ )
	{
		data_size*=dims_out[i];
	}
	
//	data = (int *) (malloc(data_size*sizeof(int)));
	data = new int[data_size];
	
	memspace = H5Screate_simple(rank,dims_out,NULL);
	status = H5Dread(dataset,H5T_NATIVE_INT,memspace,dataspace,H5P_DEFAULT,data);
	H5Sclose(memspace);
	H5Sclose(dataspace);
	H5Dclose(dataset);
	
	return data;
}

void flash_reader::getDims()
{
	hid_t dataset,dataspace,status;
	int rank;
	hsize_t dims_out[4],max_dims[4];
//	hid_t dapl_id;
	
	int data_size;

//	dataset = H5Dopen(flashFile, "gid"); // Works on some machines
	dataset = H5Dopen(flashFile, "gid",H5P_DEFAULT); // Works on others
	dataspace = H5Dget_space(dataset);
	rank = H5Sget_simple_extent_ndims(dataspace);
	if ( rank!=2 )
	{
		// gid should be a 2D array, otherwise something is wrong!
		printf("gid NOT A 2D ARRAY\n");
		exit(0);
	}
	status = H5Sget_simple_extent_dims(dataspace,dims_out,max_dims);
	if ( dims_out[1]!=15 )
	{
		// in 3D, each block has up to 15 relatives - 6 neighbours, 8 children, 1 parent
		printf("gid DATA NOT 3D\n");
		exit(0);
	}
	
	flash_data.nb = dims_out[0];
	
/*	cout << dims_out[0] << endl;
	cout << dims_out[1] << endl;
	cout << dims_out[2] << endl;
	cout << dims_out[3] << endl;
	exit(0);*/
	
	H5Sclose(dataspace);
	H5Dclose(dataset);
}
