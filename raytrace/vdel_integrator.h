class vdel_integrator : public ray_integrator {
	public:
		vdel_integrator(flash_data_t *indata);
	
		void integrate(int ib, int ic[3], double ray_start[3], double ray_end[3]);
		
		double getvdel();
		void reset();
		void set_trace();
		void doturb();
	
	private:
		double vmin;
		double vmax;
		bool calcturb;
		flash_data_t *flash_data;
};

vdel_integrator::vdel_integrator(flash_data_t *indata)
{
	flash_data = indata;
	calcturb = false;
	vmax = 0.;
	vmin = 0.;
	reset();
}

void vdel_integrator::doturb()
{
	calcturb = true;
}

void vdel_integrator::reset()
{
	vmin = 0.;
	vmax = 0.;
}

double vdel_integrator::getvdel()
{
	return vmin;
	if ( vmax!=vmin )
	{
		return vmax-vmin;
		//return log10(vmax-vmin);
	}
	else
	{
		return 0.;
	}
}

void vdel_integrator::set_trace()
{
	// Don't do anything yet
	return;
}

void vdel_integrator::integrate(int ib, int ic[3], double ray_start[3], double ray_end[3])
{
	double v_cell,dist,dd;
	int cc;
	
	dd = 0.;
	for ( int ii = 0 ; ii<3 ; ii++ )
	{
		dd+=pow(ray_start[ii]-ray_end[ii],2);
	}
	dist = sqrt(dd);
	
	cc = ib*(ncellx*ncellx*ncellx)+ic[2]*(ncellx*ncellx)+ic[1]*(ncellx)+ic[0];
	
	v_cell = ((ray_end[0]-ray_start[0])*flash_data->velx[cc] + (ray_end[1]-ray_start[1])*flash_data->vely[cc] + (ray_end[2]-ray_start[2])*flash_data->velz[cc]);
	v_cell = v_cell/dist; // v_cell is component of cell velocity in line of site (cm/s)

	if ( v_cell>vmax ) vmax = v_cell;
	if ( v_cell<vmin ) vmin = v_cell;

	return;
}
