class vave_integrator : public ray_integrator {
	public:
		vave_integrator(flash_data_t *indata);
	
		void integrate(int ib, int ic[3], double ray_start[3], double ray_end[3]);
		
		double getv();
		double getp();
		double getr();
		void reset();
		void set_trace();
		void set_onlycold();
	
	private:
		double vmass_sum;
		double mass_sum;
		double r_sum;
		bool onlycold;
		flash_data_t *flash_data;
};

vave_integrator::vave_integrator(flash_data_t *indata)
{
	flash_data = indata;
	mass_sum = 0.;
	vmass_sum = 0.;
	r_sum = 0.;
	onlycold = false;
	reset();
}

void vave_integrator::reset()
{
	mass_sum = 0.;
	vmass_sum = 0.;
	r_sum = 0.;
}

void vave_integrator::set_onlycold()
{
	onlycold = true;
}

double vave_integrator::getp()
{
	if ( mass_sum>0. )
	{
//		return vmass_sum/mass_sum;
		return vmass_sum;
	}
	else
	{
		return 0.;
	}
}

double vave_integrator::getv()
{
	if ( mass_sum>0. )
	{
		return vmass_sum/mass_sum;
	}
	else
	{
		return 0.;
	}
}

double vave_integrator::getr()
{
	if ( mass_sum>0. )
	{
		return r_sum/mass_sum;
	}
	else
	{
		return 0.;
	}
}

void vave_integrator::set_trace()
{
	// Don't do anything yet
	return;
}

void vave_integrator::integrate(int ib, int ic[3], double ray_start[3], double ray_end[3])
{
	double v_cell,dist,dd,weight;
	int cc;
	bool check_this_cell;
	double r_cell,r_c[3];
	
	dd = 0.;
	for ( int ii = 0 ; ii<3 ; ii++ )
	{
		dd+=pow(ray_start[ii]-ray_end[ii],2);
	}
	dist = sqrt(dd);
	
	cc = ib*(ncellx*ncellx*ncellx)+ic[2]*(ncellx*ncellx)+ic[1]*(ncellx)+ic[0];
	
	v_cell = ((ray_end[0]-ray_start[0])*flash_data->velx[cc] + (ray_end[1]-ray_start[1])*flash_data->vely[cc] + (ray_end[2]-ray_start[2])*flash_data->velz[cc]);
	v_cell = v_cell/dist; // v_cell is component of cell velocity in line of site (cm/s)
	
	weight = flash_data->dens[cc]*dist;
	
	
	r_c[0] = flash_data->coordinates[ib*3]+(ic[0]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
	r_c[1] = flash_data->coordinates[ib*3+1]+(ic[1]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
	r_c[2] = flash_data->coordinates[ib*3+2]+(ic[2]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
	
	r_cell = (ray_end[0]-ray_start[0])*r_c[0] + (ray_end[1]-ray_start[1])*r_c[1] + (ray_end[2]-ray_start[2])*r_c[2];
	r_cell = r_cell/dist;
	
	if ( onlycold )
	{
		check_this_cell = false;
		
		if ( flash_data->temp[cc]<5.e4 )
		{
			check_this_cell = TRUE;
		}
		if ( flash_data->dens[cc]>sodium_dens_cut ) // Assume dens gas cools quickly & contains Na absorbing gas
		{
			check_this_cell = TRUE;
		}
	}
	else
	{
		check_this_cell = true;
	}
	if ( check_this_cell )
	{
		//if ( v_cell<0. )
		{
			mass_sum+=weight;
			vmass_sum+=weight*v_cell;
			r_sum+=weight*r_cell;
		}
	}

	return;
}
