#include <cstdlib>

class sodium_line_integrator : public ray_integrator {
	public:
		sodium_line_integrator(flash_data_t *indata);
	
		void integrate(int ib, int ic[3], double ray_start[3], double ray_end[3]);
		
		line_prof_t *getProfile();
		void dumpProfileStr(ostream &os);
		void reset();
		void set_trace();
		
		void broaden(double vel_b);
		void broadenfwhm(double vel_b);
		void broaden_fftw(int ithread,fftw_plan *p1,fftw_plan *p2,t_darray *in,t_cmparray *intermediate,t_darray *out,fftw_complex *gausstrans);
		void dilute(double di);
		void doturb();
		void dofit_turb();
		void produceDoublets();
		void reverseVels();
		void noThermal();
		
		void convert_intensity();
		
		//void doublet();
	
	private:
		line_prof_t sodium_line;
		flash_data_t *flash_data;
		
		bool intensity_converted;
		bool doublet;
		bool calcturb;
		bool fit_turb;
		bool reverse_vel;
		bool thermal_broad;
		double dilute_factor;
		
		double cool_fraction(double this_dens, double this_kfrac);

};

sodium_line_integrator::sodium_line_integrator(flash_data_t *indata)
{
	flash_data = indata;

	doublet = false;
	dilute_factor = 1.;
	calcturb = false;
	reverse_vel = false;
	thermal_broad = true;
	fit_turb = false;
	
	reset();
}

void sodium_line_integrator::reset()
{
	int ii;
	
	for ( ii = 0 ; ii<n_vel ; ii++ )
	{
		sodium_line.line_depth[ii] = 0.;
	}
	intensity_converted = false;
}

void sodium_line_integrator::produceDoublets()
{
	doublet = true;
}

void sodium_line_integrator::noThermal()
{
	thermal_broad = false;
}

void sodium_line_integrator::dofit_turb()
{
	fit_turb = true;
}

void sodium_line_integrator::doturb()
{
	calcturb = true;
}

void sodium_line_integrator::reverseVels()
{
	reverse_vel = true;
}

void sodium_line_integrator::dilute(double di)
{
	//cout << "Diluting by " << di << endl;
	dilute_factor = di;
}

void sodium_line_integrator::dumpProfileStr(ostream &os)
{
	for ( int ii = 0 ;  ii<n_vel ; ii++ )
	{
		os << (ii-n_vel/2)*vel_w << " " << sodium_line.line_depth[ii] << endl;
	}
}

line_prof_t *sodium_line_integrator::getProfile()
{
	return &sodium_line;
}

void sodium_line_integrator::set_trace()
{
	cout << "SORRY - You can't trace the sodium line integrator yet. Go hassle Dave." << endl;
}

//void sodium_line_integrator::doublet()
//{
	//if ( intensity_converted )
	//{
		//cout << "Doublet is make in optical depth, not in intensity - don't call after converting to intensity!" << endl;
		//cout << "ABORTING" << endl;
		//exit(0);
	//}
	//else
	//{
		//line_prof_t shifted_line;
		//double new_v,old_v;
		//int iold_left;

//// DUMMY values for now!
		//double const delta_v = 100*vel_w;
		//double const width_ratio = 1.;
		//double const depth_ratio = 1.3;

		//for ( int inew = 0 ; inew<n_vel ; inew ++)
		//{
			//shifted_line.line_depth[inew] = 0.;
			//new_v = (inew-n_vel/2)*vel_w;
			//old_v = (new_v-delta_v)/width_ratio;
			//iold_left=(int)floor(old_v/vel_w)+n_vel/2;
			//if ( iold_left>=-1 && iold_left<n_vel )
			//{
				//double df = (old_v-(iold_left-n_vel/2)*vel_w);
				//if ( df<0. || df>1. )
				//{
					//cout << "df error!" << endl;
					//cout << "df=" << df << ", old_v=" << old_v << ", iold_left=" << iold_left << ", n_vel=" << n_vel << endl;
				//}
				//if ( iold_left==n_vel-1 )
				//{
					//shifted_line.line_depth[inew] = sodium_line.line_depth[n_vel-1]*(1.-df);
				//}
				//else if ( iold_left==-1 )
				//{
					//shifted_line.line_depth[inew] = sodium_line.line_depth[0]*df;
				//}
				//else
				//{
					//shifted_line.line_depth[inew] = sodium_line.line_depth[iold_left]*(1.-df)+sodium_line.line_depth[iold_left+1]*df;
				//}
			//}
		//}
		
		//// Add the shifted line!
		//for ( int ii = 0 ; ii<n_vel ; ii++ )
		//{
			//sodium_line.line_depth[ii] += shifted_line.line_depth[ii]/width_ratio/depth_ratio;
		//}
	//}
//}

void sodium_line_integrator::broadenfwhm(double vel_b)
{
	broaden(vel_b/2.3548200);
}

void sodium_line_integrator::broaden(double vel_b)
{
	line_prof_t newline;
	// Broaden - but we have to convert from  to exponential first, otherwise rounding will be a pain
	if ( !intensity_converted )
	{
		convert_intensity();
	}
	for ( int inew = 0 ; inew<n_vel ; inew ++)
	{
		newline.line_depth[inew] = 0.;
		for ( int iold = -n_vel ; iold<n_vel*2. ; iold ++ )
//		for ( int iold = 0 ; iold<n_vel ; iold ++ )
		{
			if ( iold>=0 && iold<n_vel )
			{
				newline.line_depth[inew] += exp(-pow(((inew-iold)*vel_w)/vel_b,2))*sodium_line.line_depth[iold];
			}
			else
			{
				newline.line_depth[inew] += exp(-pow(((inew-iold)*vel_w)/vel_b,2));
			}
		}
	}
	for ( int ii = 0 ; ii<n_vel ; ii++ )
	{
		sodium_line.line_depth[ii] = newline.line_depth[ii] /= (vel_b*sqrt(3.14159265));
	}
}

void sodium_line_integrator::broaden_fftw(int ithread,fftw_plan *p1,fftw_plan *p2,t_darray *in,t_cmparray *intermediate,t_darray *out,fftw_complex *gausstrans)
{
	double cx,cy;
	
	if ( !intensity_converted )
	{
		convert_intensity();
	}
	// Copy into input array
	//cout << "ithread = " << ithread << endl;
	//cout << "in[0].data[0]=" << in[0].data[0] << endl;
	//cout << "in[1].data[0]=" << in[1].data[0] << endl;
	for ( int ii = 0 ; ii<n_vel ; ii++ )
	{
		in[ithread].data[ii] = sodium_line.line_depth[ii];
	}
	//cout << "execute!" << endl;
	fftw_execute(p1[ithread]);
	//cout << "convolve!" << endl;

	for ( int i = 0 ; i<n_vel ; i++)
	{
		cx = intermediate[ithread].data[i][0] * gausstrans[i][0] - intermediate[ithread].data[i][1] * gausstrans[i][1];
		cy = intermediate[ithread].data[i][1] * gausstrans[i][0] + intermediate[ithread].data[i][0] * gausstrans[i][1];
		intermediate[ithread].data[i][0] = cx;
		intermediate[ithread].data[i][1] = cy;
	}
	//cout << "execute2!" << endl;
	fftw_execute(p2[ithread]);
	//cout << "output!" << endl;
	for ( int ii = 0 ; ii<n_vel ; ii++ )
	{
		sodium_line.line_depth[ii] = out[ithread].data[ii]/n_vel;
	}

}

void sodium_line_integrator::convert_intensity()
{
	if ( !intensity_converted )
	{
		intensity_converted = TRUE;
		for ( int ii = 0 ; ii <n_vel ; ii ++ )
		{
			sodium_line.line_depth[ii] = exp(-sodium_line.line_depth[ii]);
		}
	}
}

void sodium_line_integrator::integrate(int ib, int ic[3], double ray_start[3], double ray_end[3])
{
	double dd,dist;
	
	double Ndens; // column density
	long double prefactor;
	long double line_shape_ii;
	long double optical_depth_ii;
	long double b_factor;

	int ii,cc;
	long double v_ii,v_del,v_cell;
	long double v_doublet,b_doublet;
	
	bool check_this_cell;
	double cool_density; // How much density is in our range?
	double coolf;
	
	double t_eff,sdev;
	
	if ( intensity_converted )
	{
		cout << "Can not integrate sodium line in intensity mode!" << endl;
		abort();
	}
	
	cc = ib*(ncellx*ncellx*ncellx)+ic[2]*(ncellx*ncellx)+ic[1]*(ncellx)+ic[0];
	
	check_this_cell = FALSE;
	

	if ( flash_data->temp[cc]<5.e4 )
	{
		check_this_cell = TRUE;
	}
	if ( flash_data->dens[cc]>sodium_dens_cut ) // Assume dens gas cools quickly & contains Na absorbing gas
	{
		check_this_cell = TRUE;
	}
	if ( calcturb )
	{
		check_this_cell = TRUE;
	}

	// Only count cells with temperature below threshold
//	if ( flash_data->temp[cc]<5.e4 )
	if ( check_this_cell )
	{
		dd = 0.;
		for ( ii = 0 ; ii<3 ; ii++ )
		{
			dd+=pow(ray_start[ii]-ray_end[ii],2);
		}
		dist = sqrt(dd);
		
		if ( calcturb )
		{
			coolf = 1.;
			if ( fit_turb )
			{
				coolf = turb_fit_m*(log10(flash_data->temp[cc])-turb_fit_logT0);
				coolf = (coolf-flash_data->kfrac[cc])/(turb_fit_k*flash_data->kfrac[cc]);
				coolf = .5 * (1.-erf(coolf));
			}
			else
			{
				//sdev = sqrt(log(1+turb_b*turb_b*(3./sqrt(5.))*(flash_data->kfrac[cc]/(1-flash_data->kfrac[cc]))));
				sdev = sqrt(log(1.+turb_b*turb_b*(9./5.)*(flash_data->kfrac[cc]/(1-flash_data->kfrac[cc]))));
				//cool_density = flash_data->dens[cc] * 0.5 * (1.-erf(log(1.e-26/flash_data->dens[cc])/(sdev*sqrt(2.))));
				coolf = 0.5 * (1.-erf(log(sodium_dens_cut/flash_data->dens[cc])/(sdev*sqrt(2.))));
			}
			cool_density = flash_data->dens[cc] * coolf;
		}
		else
		{
			// Assume everything is cold enough
			cool_density = flash_data->dens[cc];
		}
	
		Ndens = dist * cool_density; // g/cm^2
		
		Ndens = Ndens * H_per_dens; // How many HI atoms per gram? Just a filler right now
		Ndens = Ndens * sodium_per_H;
		
		prefactor = Ndens * sodium_s * sodium_wave_l / dilute_factor;		
		
		//prefactor = 1.;
		
		//prefactor = prefactor * vel_w*1.e5; // Integrate vel across vel range
		
	//		prefactor = prefactor * 1.e1; // just boost it for the moment so it's not getting rounded off (proper units should fix this)
	//	cout << "prefactor=" << prefactor << endl;
	//	prefactor = 1.;
		
		// Technically I think velocity is face-centred, but in the meantime, let's just assume velx[i][j][k] is the x velocity of cell (i,j,k)
		
		v_cell = ((ray_end[0]-ray_start[0])*flash_data->velx[cc] + (ray_end[1]-ray_start[1])*flash_data->vely[cc] + (ray_end[2]-ray_start[2])*flash_data->velz[cc]);
		v_cell = v_cell/dist; // v_cell is component of cell velocity in line of site (cm/s)
//		v_cell = v_cell / 1.e5; // Convert to km/s

		if ( reverse_vel )
		{
			v_cell = - v_cell;
		}
		
	//	cout << flash_data->velx[cc] <<" " << flash_data->vely[cc] << " " << flash_data->velz[cc] << " " << v_cell << endl;
	
	//	cout << v_cell << " " << (n_vel/2 * vel_w)*1.e5 << " " << (n_vel/2 * vel_w)*1.e5-v_cell << endl;
		
		if ( calcturb )
		{
			t_eff = t_const*flash_data->eint[cc]*flash_data->kfrac[cc];
			
			b_factor = sqrt(2.*komum*t_eff);
		}
		else
		{
			b_factor = sqrt(2.*komum*flash_data->temp[cc]); // in cgs
		}
			
//		b_factor = b_factor * 1.e5; // in km/s
//		b_factor = 1.;
		
		v_doublet = v_cell + Na_doublet_dv*1.e5; // Convert from km/s to cgs
		b_doublet = b_factor; // Same width obviously
		
		//if ( v_cell>0. )
		//{
			//b_factor = 0.;
		//}
		
	//	cout << flash_data->temp[cc] << " " << flash_data->dens[cc] << " " << cc << endl;
	
//		cout << "b_factor=" << b_factor/(vel_w*1.e5) << endl;
//		b_factor = floor(b_factor/(vel_w*1.e5))*vel_w*1.e5;
//		cout << "b_factor corrected=" << b_factor/(vel_w*1.e5) << endl;
	
		for ( ii = 0 ; ii < n_vel ; ii++ )
		{
			v_ii = ((ii-n_vel/2) * vel_w+v_off)*1.e5; // In cm/s
			
			
			v_del = (v_cell-v_ii);
			
			//line_shape_ii = v_del;
			// Delta function!
			//line_shape_ii = 1./(sqrt(PI)*b_factor) * exp(-pow(v_del/b_factor,2)*1.e7);
			
			if ( thermal_broad )
			{
				line_shape_ii = 1./(sqrt(PI)*b_factor) * exp(-pow(v_del/b_factor,2));
			}
			else
			{
				if ( abs(v_del) <=vel_w*1.e5 )
				{
					//line_shape_ii = 1./(sqrt(PI)*b_factor);
					line_shape_ii = 1./(vel_w*1.e5); // Normalise it dude
				}
				else
				{
					line_shape_ii = 0.;
				}
			}

			if ( doublet )
			{
				v_del = (v_doublet-v_ii);
				line_shape_ii += doublet_str_ratio/(sqrt(PI)*b_doublet) * exp(-pow(v_del/b_doublet,2));
			}
			
			if ( prefactor==0 || b_factor==0 )
			{
				optical_depth_ii = 0.;
			}
			else
			{
				optical_depth_ii = prefactor * line_shape_ii;
				//optical_depth_ii = prefactor;
			}
			
			//sodium_line.intensity[ii] *= exp(-optical_depth_ii);
	//		sodium_line.line_depth[ii] *= exp(-optical_depth_ii);
	//		sodium_line.intensity[ii] *= exp(-pow(v_ii/(v_cell*n_vel),2)/1.e6);
			sodium_line.line_depth[ii] += optical_depth_ii;
//			sodium_line.line_depth[ii] = optical_depth_ii;
			
	//		cout << optical_depth_ii << endl;
		}
	}
	//exit(0);
	
	return;
}

double sodium_line_integrator::cool_fraction(double this_dens, double this_kfrac)
{
	double sdev = sqrt(log(1+turb_b*turb_b*(3./sqrt(5.))*(this_kfrac/(1-this_kfrac))));
	return 0.5 * (1.-erf(log(sodium_dens_cut/this_dens)/(sdev*sqrt(2.))));
}
