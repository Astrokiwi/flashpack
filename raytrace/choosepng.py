import shutil
import sys

#suffix = "broaden"

#print len(sys.argv)
if ( len(sys.argv)==1 ):
	suffix = ""
else:
	suffix = sys.argv[1]
f = open("width_angle"+suffix+"B_selected.dat")
choices = [int((x.split(" "))[4]) for x in f.readlines()]
f.close()

print choices

#sys.exit()

for count in range(0,len(choices)):
	if ( choices[count]==1 ):
		file_src = "lineouts/fixedsizerot"+suffix+"B"+str(count).rjust(4,'0')+".dat_single.png"
	else:
		file_src = "lineouts/fixedsizerot"+suffix+"B"+str(count).rjust(4,'0')+".dat_double.png"
	file_dest = "lineouts/fixedsizerot"+suffix+"B"+str(count).rjust(4,'0')+".dat_select.png"
	#print file_src,file_dest
	shutil.copyfile(file_src,file_dest)
