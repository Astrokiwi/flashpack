class ray_generator {
	public:
		ray_generator(flash_data_t *flash_data_in, double rayv_in[3], int nx_in, bool dbg, double gridsize); // simulation data, ray direction, ray direction, create nx*nx grid of rays
		
		ray_t* generate(int inx, int iny);
		
		void find_intersect(ray_t *ray_in);
	private:
		flash_data_t *flash_data;
		int nx;
		double rayv[3];
//		int ix,iy;
		
		double basis_x[3],basis_y[3]; // Basis vectors for generating the grid of rays
		double base_r[3]; // Origin of ray grid
		
		dimens_t nonrot_size; // Boundaries of unrotated flash grid
		
		bool debug_output;
};

//////////////////////////////////////////////////////////////


ray_generator::ray_generator(flash_data_t *flash_data_in, double rayv_in[3], int nx_in, bool dbg, double gridsize)
{
	double rl,rh;
	
	double theta1,theta2; // Rotation vectors
	double r1[3],r2[3];
	double unrotbase1[3],unrotbase2[3];
	double corncoords[3];
	int ib;
	dimens_t size;
	int ii,cc;
	double corn_sign[3];
	int iix,iiy;
	ofstream blockrot_f;
	
	flash_data = flash_data_in;
	nx = nx_in;
	for ( ii = 0 ; ii<3 ; ii++)
	{
		rayv[ii] = rayv_in[ii];
	}
	
//	debug_output = dbg;
	debug_output = false;
//	ix = -1; iy = 0;
	
	// Work out basis vectors etc
	// Rotate blocks so that our ray directions are (1,0,0)
	// Find min/max y&z to get dimensions of our grid of rays
	// Find min x to know where to fire rays from
	
// Calculate angles to rotate
	theta1 = atan2(rayv[2],rayv[0]);
	//theta1 = 0.;
	//if ( theta1>PI/2. )
	//{
		//theta1-=PI;
	//}
	//if ( theta1<-PI/2. )
	//{
		//theta1+=PI;
	//}

//	printf("%f %f %f\n",rayv[0],rayv[1],rayv[2]);
	r1[0] = rayv[0]*cos(theta1)+rayv[2]*sin(theta1);
	r1[1] = rayv[1];
	r1[2] = -rayv[0]*sin(theta1)+rayv[2]*cos(theta1);
//	printf("%f %f %f\n",r1[0],r1[1],r1[2]);
	
	theta2 = atan2(r1[1],r1[0]);

	r2[0] = r1[0]*cos(theta2)+r1[1]*sin(theta2);
	r2[1] = -r1[0]*sin(theta2)+r1[1]*cos(theta2);
	r2[2] = r1[2];
	
	if ( r2[0]<0. )
	{
		cout << "Ray vector not rotated to zero.\n";
		printf("%f %f %f\n",r2[0],r2[1],r2[2]);
		exit(0);
	}
	//cout.precision(40);
	//cout << "rotvec:" << r2[0] << " " << r2[1] << " " << r2[2] << endl;

	
	if ( debug_output )
	{
		blockrot_f.open("blockrot.dat");
	}
	
	// Find borders of non-reflected region
	for ( ii = 0 ; ii<3 ; ii++ )
	{
		nonrot_size.rlow[ii] = flash_data->coordinates[ii]-flash_data->blocksize[ii]/2.;
		nonrot_size.rhigh[ii] = flash_data->coordinates[ii]+flash_data->blocksize[ii]/2.;
	}
	
	for ( ib = 1 ; ib<flash_data->nb ; ib++ )
	{
		for ( ii = 0 ; ii<3 ; ii++ )
		{
			r1[ii] = flash_data->coordinates[ib*3+ii]-flash_data->blocksize[ib*3+ii]/2.;
			if ( r1[ii]<nonrot_size.rlow[ii] )
			{
				nonrot_size.rlow[ii] = r1[ii];
			}
			r1[ii] = flash_data->coordinates[ib*3+ii]+flash_data->blocksize[ib*3+ii]/2.;
			if ( r1[ii]>nonrot_size.rhigh[ii] )
			{
				nonrot_size.rhigh[ii] = r1[ii];
			}
		}
	}

	for ( ib = 0 ; ib<flash_data->nb ; ib++ )
	{
		// Loop over all reflections, plus unreflected
		for ( int refl = 0 ; refl< 9 ; refl++ )
		{
			int refl_dir = refl/2;
			int refl_highlow = refl%2;
			
			if ( refl==8 )
				{ refl_dir = 0;}
		
			if ( refl==8 || flash_data->reflect[refl_dir][refl_highlow] )
			{
			
				if ( debug_output )
				{
					// Perform rotation for dumping
					for ( ii = 0 ; ii<3 ; ii++ )
					{
						corncoords[ii] = flash_data->coordinates[ii+ib*3];
					}
					r1[0] = corncoords[0]*cos(theta1)+corncoords[2]*sin(theta1);
					r1[1] = corncoords[1];
					r1[2] = -corncoords[0]*sin(theta1)+corncoords[2]*cos(theta1);
			
					r2[0] = r1[0]*cos(theta2)+r1[1]*sin(theta2);
					r2[1] = -r1[0]*sin(theta2)+r1[1]*cos(theta2);
					r2[2] = r1[2];
					
					blockrot_f << r2[0] << " " << r2[1] << " " << r2[2] << endl;
				}


				for ( cc = 0 ; cc<8 ; cc++ )// 8 corners of cells
				{
					corn_sign[0] = pow(-1.,(cc/4));
					corn_sign[1] = pow(-1.,(cc/2));
					corn_sign[2] = pow(-1.,cc);
					for ( ii = 0 ; ii<3 ; ii++ )
					{
						corncoords[ii] = flash_data->coordinates[ii+ib*3]+corn_sign[ii]*flash_data->blocksize[ii+ib*3]*.5;
					}
					
					// Perform reflection
					if ( refl!=8 )
					{
						if ( refl_highlow==0 )
						{
							corncoords[refl_dir] = 2.*nonrot_size.rlow[refl_dir]-corncoords[refl_dir];
						}
						else
						{
							corncoords[refl_dir] = 2.*nonrot_size.rhigh[refl_dir]-corncoords[refl_dir];
						}
					}
					
					// Perform rotation	
					r1[0] = corncoords[0]*cos(theta1)+corncoords[2]*sin(theta1);
					r1[1] = corncoords[1];
					r1[2] = -corncoords[0]*sin(theta1)+corncoords[2]*cos(theta1);
			
					r2[0] = r1[0]*cos(theta2)+r1[1]*sin(theta2);
					r2[1] = -r1[0]*sin(theta2)+r1[1]*cos(theta2);
					r2[2] = r1[2];
					
					
					if ( ib==0 && cc==0 )
					{
						for ( ii = 0 ; ii<3 ; ii++ )
						{
							size.rlow[ii] = r2[ii];
							size.rhigh[ii] = r2[ii];
						}
					}
					else
					{
						for ( ii = 0 ; ii<3 ; ii++ )
						{
							if ( size.rlow[ii]>r2[ii] )
							{
								size.rlow[ii] = r2[ii];
							}
							if ( size.rhigh[ii]<r2[ii] )
							{
								size.rhigh[ii] = r2[ii];
							}
						}
					}
				}
			}
		}
	}
	
//	cout << size.rlow[0] << " " << size.rhigh[0] << endl;
//	cout << size.rlow[1] << " " << size.rhigh[1] << endl;
//	cout << size.rlow[2] << " " << size.rhigh[2] << endl;
	
	
	
	blockrot_f.close();

	if ( gridsize>0. )
	{
		double midy,midz;
		
		midy = (size.rhigh[1]+size.rlow[1])/2.;
		size.rlow[1] = midy - gridsize/2.;
		size.rhigh[1] = midy + gridsize/2.;
		
		midz = (size.rhigh[2]+size.rlow[2])/2.;
		size.rlow[2] = midz - gridsize/2.;
		size.rhigh[2] = midz + gridsize/2.;		
	}

	
	// Build basis vectors!
	unrotbase1[0] = 0.;
	unrotbase1[1] = (size.rhigh[1]-size.rlow[1])/(nx);
	unrotbase1[2] = 0.;
	
	unrotbase2[0] = 0.;
	unrotbase2[1] = 0.;
	unrotbase2[2] = (size.rhigh[2]-size.rlow[2])/(nx);
	
	
	//cout << "unrotbase:" << unrotbase1[1] << " " << unrotbase2[2] << endl;
	
	if ( debug_output )
	{
		ofstream rotbasis_f;
		rotbasis_f.open("rotbasis.dat");
	
		for ( iix = 0 ; iix<nx ; iix ++ )
		{
			for ( iiy = 0 ; iiy<nx ; iiy++ )
			{
				rotbasis_f << size.rlow[0] << " " << size.rlow[1]+unrotbase1[1]*iix << " " << size.rlow[2]+unrotbase2[2]*iiy << endl;
			}
		}
		rotbasis_f.close();
	}
	
	// rotate back into "real" coordinates
	r1[0] = unrotbase1[0]*cos(theta2)+unrotbase1[1]*-sin(theta2);
	r1[1] = -unrotbase1[0]*-sin(theta2)+unrotbase1[1]*cos(theta2);
	r1[2] = unrotbase1[2];
//	r1[2] = -unrotbase1[2];
	
	basis_x[0] = r1[0]*cos(theta1)+r1[2]*-sin(theta1);
	basis_x[1] = r1[1];
	basis_x[2] = -r1[0]*-sin(theta1)+r1[2]*cos(theta1);
	
	r1[0] = unrotbase2[0]*cos(theta2)+unrotbase2[1]*-sin(theta2);
	r1[1] = -unrotbase2[0]*-sin(theta2)+unrotbase2[1]*cos(theta2);
	r1[2] = unrotbase2[2];
//	r1[2] = -unrotbase2[2];
	
	basis_y[0] = r1[0]*cos(theta1)+r1[2]*-sin(theta1);
	basis_y[1] = r1[1];
	basis_y[2] = -r1[0]*-sin(theta1)+r1[2]*cos(theta1);
	
	// Build origin vector
		// shunt a little to make things centred
	size.rlow[0]*=1.2; // Step back so rounding does make you start inside a cell
	size.rlow[1] = size.rlow[1] + unrotbase1[1]/2.;
	size.rlow[2] = size.rlow[2] + unrotbase1[2]/2.;
		// Do rotations
	r1[0] = size.rlow[0]*cos(theta2)+size.rlow[1]*-sin(theta2);
	r1[1] = -size.rlow[0]*-sin(theta2)+size.rlow[1]*cos(theta2);
	r1[2] = size.rlow[2];
//	r1[2] = -size.rlow[2];
	
	base_r[0] = r1[0]*cos(theta1)+r1[2]*-sin(theta1);
	base_r[1] = r1[1];
	base_r[2] = -r1[0]*-sin(theta1)+r1[2]*cos(theta1);

	//cout << "Ray vector = " << rayv[0] << " " << rayv[1] << " " << rayv[2] << endl;
	//cout << "Angles = " << theta1 << " " << theta2 << endl;
	//cout << "origin vector:" << base_r[0] << " " << base_r[1] << " " << base_r[2] << endl;
	//cout << "basis vector 1:" << basis_x[0] << " " << basis_x[1] << " " << basis_x[2] << endl;
	//cout << "basis vector 2:" << basis_y[0] << " " << basis_y[1] << " " << basis_y[2] << endl;

	if ( debug_output )
	{
		ofstream basis_f;
		basis_f.open("basis.dat");
	
		for ( iix = 0 ; iix<nx ; iix ++ )
		{
			for ( iiy = 0 ; iiy<nx ; iiy++ )
			{
				basis_f << base_r[0]+basis_x[0]*iix+basis_y[0]*iiy << " " << base_r[1]+basis_x[1]*iix+basis_y[1]*iiy << " " << base_r[2]+basis_x[2]*iix+basis_y[2]*iiy << endl;
			}
		}
		
		basis_f.close();
	}
}

//////////////////////////////////////////////////////////////

void ray_generator::find_intersect(ray_t *ray_in)
{
	double ray_t_min;
	double ray_sec_r[3],boxr0[3];
	double ray_t_f,ray_t;
	double rayr0[3];
	int rayb,ib;
	int ff,ii;
	double facer,boxr;
	int refl_min,refl_f_min;
	
	bool inbox;
	

	ray_t_min = -1.;
	rayb = -1;
	
	for ( ib = 0 ; ib<flash_data->nb ; ib++ )
	{
		ray_t = -1.;
		// Check all reflections
		for ( int refl = 0 ; refl < 9 ; refl++ )
		{
			int refl_dir = refl/2;
			int refl_highlow = refl%2;
			if ( refl==8 )
				{ refl_dir = 0; }
			if ( refl==8 || flash_data->reflect[refl_dir][refl_highlow] )
			{
				// Check time for intersection on each face
				for ( ff = 0 ; ff<6 ; ff++ )
				{
					ray_t_f = -1.;
					//if ( -pow(-1.,ff)*ray_in->v[ff/2]<0. )
					{
						if ( ray_in->v[ff/2]!=0 )
						{
							facer = flash_data->coordinates[ib*3+ff/2]-pow(-1.,ff)*flash_data->blocksize[ib*3+ff/2]/2.;
							if ( refl!=8 && ff%2 == refl_dir )
							{
								if ( refl_highlow == 0 )
								{
									facer = 2.*nonrot_size.rlow[ff%2]-facer;
								}
								else
								{
									facer = 2.*nonrot_size.rhigh[ff%2]-facer;
								}
							}
							ray_t_f = (facer-ray_in->r[ff/2])/ray_in->v[ff/2];
							/*if ( ib==10 || ib==12 )
							{
								cout << ib << " " << ff << " " << ray_t_f << endl;
							}*/
							if ( ray_t_f>=0 )
							{
								inbox = true;
								for ( ii=0 ; ii<3 ; ii++ )
								{
									{
										ray_sec_r[ii] = ray_in->r[ii]+ray_t_f*ray_in->v[ii];
										boxr = flash_data->coordinates[ib*3+ii];
										
										if ( refl!=8  )
										{
											if ( refl_dir==ii )
											{
												if ( refl_highlow==0 )
												{
													ray_sec_r[ii] = 2.*nonrot_size.rlow[ii]-ray_sec_r[ii];
												}
												else
												{
													ray_sec_r[ii] = 2.*nonrot_size.rhigh[ii]-ray_sec_r[ii];
												}
											}
										}
										
										if ( ray_sec_r[ii]<boxr-flash_data->blocksize[ib*3+ii]/2. )
										{
											inbox = false;
										}
										if ( ray_sec_r[ii]>boxr+flash_data->blocksize[ib*3+ii]/2. )
										{
											inbox = false;
										}
									}
								}
								
								if ( inbox )
								{
									if ( ray_t==-1 || ray_t_f<ray_t )
									{
										ray_t = ray_t_f;
										refl_f_min = refl;
										for ( ii = 0 ; ii<3 ; ii++ )
										{
											boxr0[ii] = ray_sec_r[ii];
										}
									}
								}
							}
						}
					}	
				}
			}
		}

		if ( ray_t>=0 && (ray_t_min==-1 || ray_t<ray_t_min) )
		{
			ray_t_min = ray_t;
			for ( ii = 0 ; ii<3 ; ii++ )
			{
				rayr0[ii] = boxr0[ii];
			}
			rayb = ib;
			refl_min=refl_f_min;
		}
	}
	
	if ( ray_t_min>=0. )
	{
		ray_in->ib = rayb;
		for ( ii = 0 ; ii<3 ; ii++ )
		{
			ray_in->r[ii] = rayr0[ii];
		}
		if ( refl_min!=8 )
		{
			ray_in->v[refl_min/2] = -ray_in->v[refl_min/2];
		}
	}
}


//////////////////////////////////////////////////////////////

/*ray_t* ray_generator::generate()
{
	ray_t* out_ray;
	double r[3],ray_sec_r[3];
	double ray_t,ray_t_f,ray_t_min;
	int ii,ib;
	int ff,hitface,hitface_ib;
	bool ray_made;
	bool inbox = true;
	
	double rayr0[3],boxr0[3];
	int rayb;

	int death_count;
	
	out_ray = new struct ray_t;

	ray_made = false;

	out_ray->ib = -1;
	if ( iy>=nx )
	  {
	    return out_ray;
	  }
	
	
	
	while ( !ray_made )
	{
		ix++;

		if ( ix==nx )
		{

		  ix = 0;
		  iy++;

		  if ( iy==nx )
		    {
		      out_ray->ib = -1;
		      return out_ray;
		    }
		}
		
		out_ray->iix = ix;
		out_ray->iiy = iy;
		
		for ( ii=0 ; ii<3 ; ii++ )
		{
		  out_ray->r[ii] = base_r[ii]+basis_x[ii]*ix+basis_y[ii]*iy;
		  out_ray->v[ii] = rayv[ii];
		}
		
		find_intersect(out_ray);
		
		if ( out_ray->ib!=-1 )
		{
			ray_made = true;
		}
	}
	
	
/*	cout << out_ray.r[0] << " " << out_ray.r[1] << " " <<out_ray.r[2] << endl;
	cout << out_ray.v[0] << " " << out_ray.v[1] << " " <<out_ray.v[2] << endl;
	cout << out_ray.ib << endl;
	cout << flash_data->coordinates[out_ray.ib*3] << " " << flash_data->coordinates[out_ray.ib*3+1] << " " << flash_data->coordinates[out_ray.ib*3+2] << " " << endl;
	cout << flash_data->blocksize[out_ray.ib*3] << " " << flash_data->blocksize[out_ray.ib*3+1] << " " << flash_data->blocksize[out_ray.ib*3+2] << " " << endl;
	cout << flash_data->coordinates[out_ray.ib*3]-flash_data->blocksize[out_ray.ib*3]/2. << " " << flash_data->coordinates[out_ray.ib*3+1]-flash_data->blocksize[out_ray.ib*3]/2. << " " << flash_data->coordinates[out_ray.ib*3+2]-flash_data->blocksize[out_ray.ib*3]/2. << " " << endl;
	cout << flash_data->coordinates[out_ray.ib*3]+flash_data->blocksize[out_ray.ib*3]/2. << " " << flash_data->coordinates[out_ray.ib*3+1]+flash_data->blocksize[out_ray.ib*3]/2. << " " << flash_data->coordinates[out_ray.ib*3+2]+flash_data->blocksize[out_ray.ib*3]/2. << " " << endl;*/
/*	return out_ray;
}*/

ray_t* ray_generator::generate(int inx,int iny)
{
	ray_t* out_ray;
	double r[3],ray_sec_r[3];
	double ray_t,ray_t_f,ray_t_min;
	int ii,ib;
	int ff,hitface,hitface_ib;
	bool ray_made;
	bool inbox = true;
	
	double rayr0[3],boxr0[3];
	int rayb;

	int death_count;
	
	out_ray = new struct ray_t;

	ray_made = false;

	out_ray->ib = -1;
	
//	ix = ir%nx;
//	iy = ir/nx;
	
	if ( iny>=nx )
	  {
	    return out_ray;
	  }
	
	out_ray->iix = inx;
	out_ray->iiy = iny;
	
	for ( ii=0 ; ii<3 ; ii++ )
	{
	  out_ray->r[ii] = base_r[ii]+basis_x[ii]*inx+basis_y[ii]*iny;
	  out_ray->v[ii] = rayv[ii];
	}
	
	find_intersect(out_ray);
	
	return out_ray;
}
