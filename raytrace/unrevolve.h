void unrevolve(flash_data_t *flash_data)
{
	const int n_bins = 50;
	//double r_max = 3.08568025e18*1.e5;
	//double z_max = 3.08568025e18*1.e5;
	double r_max = 3.08568025e18*1.e2;
	double z_max = 3.08568025e18*2.e2;
	int cc,ibin,jbin;
	double rotv[n_bins][n_bins],rotmass[n_bins][n_bins];
	double vcirc,rad,mass,vol;
	double cell_r[3];
	double fac1,fac2;
	for ( ibin = 0 ; ibin<n_bins ; ibin++ )
	{
		for ( jbin = 0 ; jbin<n_bins ; jbin++ )
		{
			rotv[ibin][jbin] = 0.;
			rotmass[ibin][jbin] = 0.;
		}
	}
	for ( int ib = 0 ; ib<flash_data->nb ; ib++ )
	{
		vol = pow(flash_data->blocksize[ib*3]/ncellx/pc_in_cm,3); // pc^3 volume
		for ( int ix = 0 ; ix<ncellx ; ix++ )
		{
			for ( int iy = 0 ; iy<ncellx ; iy++ )
			{
				for ( int iz = 0 ; iz<ncellx ; iz++ )
				{
					cc = ib*(ncellx*ncellx*ncellx)+iz*(ncellx*ncellx)+iy*(ncellx)+ix;
					cell_r[0] = flash_data->coordinates[ib*3]+(ix-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
					cell_r[1] = flash_data->coordinates[ib*3+1]+(iy-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
					cell_r[2] = flash_data->coordinates[ib*3+2]+(iz-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;;
					
					//if ( abs(cell_r[1])<r_max/10. )
					{
						
						rad = sqrt(cell_r[0]*cell_r[0]+cell_r[2]*cell_r[2]);
						vcirc = (cell_r[0] * flash_data->velz[cc] - cell_r[2]*flash_data->velx[cc])/rad;
						
						ibin = (int)floor((rad*n_bins)/r_max);
						jbin = (int)floor(abs(cell_r[1]*n_bins)/z_max);
						if ( ibin<n_bins && jbin<n_bins)
						{
							mass = vol * flash_data->dens[cc]; // g/cm^3 * pc^3 - not consistent, but we're just using it for a weighting
							rotv[ibin][jbin]+=vcirc*mass;
							rotmass[ibin][jbin]+=mass;
							//if ( ibin==72 )
							//{
								//cout << vol << " " << mass << " " << " " << flash_data->dens[cc] << " " << vcirc << " " << rad << " " << ibin << " " << rotmass[ibin] << " " << rotv[ibin] << endl;
							//}
							//if ( rotmass[ibin][jbin]!=rotmass[ibin][jbin] )
							//{
								//cout << cc << endl;
								//cout << vol << " " << mass << " " << " " << flash_data->dens[cc] << " " << vcirc << " " << rad << " " << ibin << " " << rotmass[ibin] << " " << rotv[ibin] << endl;
								//exit(0);
							//}
						}
					}
				}
			}
		}
	}
	
	//ofstream rot_prof;

	//rot_prof.open("rot_prof1.dat");
	for ( ibin = 0 ; ibin<n_bins ; ibin++ )
	{
		for ( jbin = 0 ; jbin<n_bins ; jbin++ )
		{
			if ( rotmass[ibin][jbin]>0. )
			{
				rotv[ibin][jbin]/=rotmass[ibin][jbin];
			}
		}
		//rot_prof << ibin << " " << rotv[ibin] << " " << rotmass[ibin] << endl;
	}
	//rot_prof.close();
	//exit(0);
	
	// Look through cells, remove circular velocity
	for ( int ib = 0 ; ib<flash_data->nb ; ib++ )
	{
		vol = pow(flash_data->blocksize[ib*3]/ncellx/pc_in_cm,3); // pc^3 volume
		for ( int ix = 0 ; ix<ncellx ; ix++ )
		{
			for ( int iy = 0 ; iy<ncellx ; iy++ )
			{
				for ( int iz = 0 ; iz<ncellx ; iz++ )
				{
					cc = ib*(ncellx*ncellx*ncellx)+iz*(ncellx*ncellx)+iy*(ncellx)+ix;
					cell_r[0] = flash_data->coordinates[ib*3]+(ix-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
					cell_r[1] = flash_data->coordinates[ib*3+1]+(iy-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
					cell_r[2] = flash_data->coordinates[ib*3+2]+(iz-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;;
					
					//if ( abs(cell_r[1])<r_max/10. )
					{
						mass = vol * flash_data->dens[cc]; // g/cm^3 * pc^3 - not consistent, but we're just using it for a weighting

						rad = sqrt(cell_r[0]*cell_r[0]+cell_r[2]*cell_r[2]);
						
						ibin = (int)floor((rad*n_bins)/r_max-.5);
						jbin = (int)floor((abs(cell_r[1])*n_bins)/z_max-.5);
						
						if ( ibin<n_bins && jbin<n_bins)
						{
							if ( ibin==n_bins-1 )
							{
								if ( jbin<n_bins-1 )
								{
									fac2 = ((abs(cell_r[1])*n_bins)/z_max-jbin-.5);
									vcirc = (1-fac2)*rotv[ibin][jbin]+fac2*rotv[ibin][jbin+1];
								}
								else if ( jbin==-1 )
								{
									vcirc = rotv[ibin][0];
								}
								else if ( jbin==n_bins-1 )
								{
									vcirc = rotv[ibin][jbin];
								}
								//vcirc = vcirc * mass/rotmass[ibin];
							}
							else if ( ibin==-1 )
							{
								if ( jbin<n_bins-1 )
								{
									fac2 = ((abs(cell_r[1])*n_bins)/z_max-jbin-.5);
									vcirc = (1-fac2)*rotv[0][jbin]+fac2*rotv[ibin][jbin+1];
								}
								else if ( jbin==-1 )
								{
									vcirc = rotv[0][0];
								}
								else if ( jbin==n_bins-1 )
								{
									vcirc = rotv[0][jbin];
								}
								//vcirc = rotv[0];
								//vcirc = vcirc * mass/rotmass[0];
								
							}
							else if (jbin==-1 )
							{
								fac1 = ((rad*n_bins)/r_max-ibin-.5);
								vcirc = (1-fac1)*rotv[ibin][0]+fac1*rotv[ibin+1][0];
							}
							else if (jbin==n_bins-1)
							{
								fac1 = ((rad*n_bins)/r_max-ibin-.5);
								vcirc = (1-fac1)*rotv[ibin][jbin]+fac1*rotv[ibin+1][jbin];
							}
							else
							{
								fac1 = ((rad*n_bins)/r_max-ibin-.5);
								fac2 = ((abs(cell_r[1])*n_bins)/z_max-jbin-.5);
								//fac = 0.;
								
								//vcirc = (1-fac)*rotv[ibin] + fac*rotv[ibin+1];
								//vcirc = vcirc * mass/((1-fac)*rotmass[ibin]+fac*rotmass[ibin+1];
								vcirc = (1-fac1)*((1-fac2)*rotv[ibin][jbin]+fac2*rotv[ibin][jbin+1])+fac1*((1-fac2)*rotv[ibin+1][jbin]+fac2*rotv[ibin+1][jbin+1]);
							}
							
							flash_data->velx[cc]+=cell_r[2]*vcirc/rad;
							flash_data->velz[cc]-=cell_r[0]*vcirc/rad;
						}
						
					}
				}
			}
		}
	}
	
}
