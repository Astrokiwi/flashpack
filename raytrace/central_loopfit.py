import sys
import os

import fitgauss

plot_fit_in = True

fit_prams_single = []
suffix = ""
datafilename_in = "central_lines/0185_16_0000.dat"
single_data_str = fitgauss.single_gauss_fit(datafilename_in,plot_fit_in,suffix)
fit_prams_single.append(single_data_str)
datafilename_in = "central_lines/0185_13_0000.dat"
single_data_str = fitgauss.single_gauss_fit(datafilename_in,plot_fit_in,suffix)
fit_prams_single.append(single_data_str)
datafilename_in = "central_lines/0154_13_0000.dat"
single_data_str = fitgauss.single_gauss_fit(datafilename_in,plot_fit_in,suffix)
fit_prams_single.append(single_data_str)


ofile_single = open("twoprams.dat",'w')
for pram in fit_prams_single:
    ofile_single.write(pram)
ofile_single.close()



# fit_prams_single = []
# suffix = ""
# #for ii in range(0,11,1):
# for ii in [0,3,7,10]:
# 	datafilename_in = "central_lines/0185_16_"+str(ii).rjust(4,'0')+".dat"
# 	single_data_str = fitgauss.single_gauss_fit(datafilename_in,plot_fit_in,suffix)
# 	fit_prams_single.append(single_data_str)
# 
# ofile_single = open("width_angle_185.dat",'w')
# jj = 0
# #for ii in range(0,11,1):
# for ii in [0,3,7,10]:
# 	ofile_single.write(str(ii*2)+" "+fit_prams_single[jj])
# 	jj = jj + 1
# ofile_single.close()
