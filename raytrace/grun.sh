#!/bin/bash
#PBS -l nodes=1:ppn=12
#PBS -o dump_ray
#PBS -e err_ray
#PBS -l pmem=1700m
#PBS -l walltime=0:01:00
#PBS -V
#PBS -N raytrace
#PBS -A dhg-391-ac
ulimit -a
cd $PBS_O_WORKDIR
export OMP_NUM_THREADS=12
./raytrace -f /gs/scratch/dwilliam/flash/0203/ULIRG_0203_hdf5_chk_0000 -theta 0. 90. -phi 0. 0. -dens -n 128 -frames 5 -o outputs/0203dens.dat
#gdb ./raytrace --eval-command="run -f /gs/scratch/dwilliam/flash/0203/ULIRG_0203_hdf5_chk_0000 -theta 0. 0. -phi 0. 0. -dens -n 128 -frames 1 -o outputs/0203dens.dat"
