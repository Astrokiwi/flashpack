// Ray integrator interface
class ray_integrator {
	public:
		virtual void integrate(int ib, int ic[3], double ray_start[3], double ray_end[3]) = 0;
		virtual void set_trace() = 0;
};

#include "dens_integrator.h"
#include "dist_integrator.h"
#include "Na_integrator.h"
#include "vdel_integrator.h"
#include "vave_integrator.h"
