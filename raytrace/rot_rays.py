# Circle around an axis

import os
import math

frames = 90
angle = 0.
angle_max = 0.0174532925 * 90
in_file = "/disc42/williams/flash/galreal/ULIRG_0111_hdf5_chk_0022"

for frame in range(0,frames):
	angle = frame * angle_max / (frames-1)
	command = "./raytrace -f "+in_file+" -n 128 -y 0. -x "+str(math.sin(angle))+" -z "+str(math.cos(angle))+" -o map"+str(frame).rjust(4,"0")+".dat"
	print command
	os.system(command)
