class distance_integrator : public ray_integrator {
	public:
		distance_integrator();
	
		void integrate(int ib, int ic[3], double ray_start[3], double ray_end[3]);
		
		double getDist();
		void reset();
		void set_trace();
	
	private:
		double dist;
};

distance_integrator::distance_integrator()
{
	dist = 0.;
}

void distance_integrator::reset()
{
	dist = 0.;
}

double distance_integrator::getDist()
{
	return dist;
}

void distance_integrator::set_trace()
{
	// Don't do anything yet
	return;
}

void distance_integrator::integrate(int ib, int ic[3], double ray_start[3], double ray_end[3])
{
	double dd;

	int ii;
	
	dd = 0.;
	for ( ii = 0 ; ii<3 ; ii++ )
	{
		dd+=pow(ray_start[ii]-ray_end[ii],2);
	}
	dist += sqrt(dd);
	
	return;
}
