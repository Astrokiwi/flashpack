#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

typedef struct t_rad_index {
	double rad;
	int index;
};


typedef struct t_bin {
	double mass;
	double dens;
	double temp;
	double rad;
	double vol;
};

int compare_structs (const void *a, const void *b);

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data,*flash_data2;
//	char* inputfile = "/disc42/williams/flash/galreal/ULIRG_0016_hdf5_chk_0010";
	char* inputfile = argv[1];
	char* outfile = argv[2];
//	char* inputfile2 = argv[2];
	
	int n_bins = 50;
	
	my_reader = new flash_reader(inputfile);
//	my_reader->readSimInfo();
	my_reader->readDens();
//	my_reader->readPres();
	my_reader->readTemp();
	//my_reader->readEint();
	//my_reader->readKTurb();
//	my_reader->readEner();
	//my_reader->readLTurb();
	//my_reader->readMut();
	//my_reader->L_cm_to_pc();
	//my_reader->cm_to_pc();
	//my_reader->readMetaArrays();
//	my_reader->readEner();
//	my_reader->readGamc();
//	my_reader->readKFrac();
//	my_reader->readVels();
	flash_data = my_reader->getData();
	my_reader->close();

/*	my_reader = new flash_reader(inputfile2);
	my_reader->readDens();
	my_reader->readEint();
	flash_data2 = my_reader->getData();
	my_reader->close();*/


	//cout << "Converting distance to parsecs" << endl;
//	my_reader->cm_to_pc();
	//cout << "Converting densities to MSun/pc^3" << endl;
//	my_reader->to_msunpc3();

	// Determine total mass in gas
	
//	cout << "Finding total mass and energy in gas" << endl;
	
	double mass_sum = 0., en_sum = 0., heat_sum = 0.;
	double max_ei = 0.;
	double CoM[3];
	double cell_vol;
	int icb,ic;
	double x,y,z;
	double maxx,maxy,maxz,maxib,maxip;
	int maxix,maxiy,maxiz;
	int min_icb;
	double dt,dt_min,dx,cs;
	double maxcs,maxvel;
	
	double temp_min = 1.e20;
	
	double tmax=1.e-20,tmin=1.e20,rhomax=1.e-30,rhomin=1.e20;
	int itemp,irho;
	int n_cells = 32;
	double cell_weight[n_cells][n_cells];
	
	//for ( int i=0 ; i<3 ; i++ )
	//{
		//CoM[i] = 0.;
	//}
	
	//dt_min = -1;
	
	// Where are these boxes?
	
	int ib;
/*	ib = 84;
	cout << "Box" << " " << ib<< " " << flash_data->coordinates[ib*3]<< " " << flash_data->coordinates[ib*3+1]<< " " << flash_data->coordinates[ib*3+2] << endl;
	ib = 95;
	cout << "Box" << " " << ib<< " " << flash_data->coordinates[ib*3]<< " " << flash_data->coordinates[ib*3+1]<< " " << flash_data->coordinates[ib*3+2] << endl;
	ib = 3105;
	cout << "Box" << " " << ib<< " " << flash_data->coordinates[ib*3]<< " " << flash_data->coordinates[ib*3+1]<< " " << flash_data->coordinates[ib*3+2] << endl;*/
	
	for ( int itemp=0 ; itemp<n_cells; itemp ++ )
	{
		for ( int irho= 0 ; irho<n_cells; irho ++ )
		{
			cell_weight[itemp][irho] = 0.;
		}
	}
	// MAKE A DENSITY/TEMPERATURE PHASE PLOT
	for ( ib=0; ib<flash_data->nb; ib++ )
	{
		// Only do leaf blocks
		if ( flash_data->nodetype[ib]==1 )
		{
			//cell_vol = pow(flash_data->blocksize[ib*3]/ncellx,3);
			for ( int ix = 0 ; ix<ncellx ; ix++ )
			{
				for ( int iy = 0 ; iy<ncellx ; iy++ )
				{
					for ( int iz = 0 ; iz<ncellx ; iz++ )
					{
						ic = ix + iy*ncellx + iz*ncellx*ncellx;
						
						icb = ib*(ncellx*ncellx*ncellx) + ic;
						
						if ( flash_data->dens[icb]<rhomin )
						{
							rhomin = flash_data->dens[icb];
						}
						if ( flash_data->dens[icb]>rhomax )
						{
							rhomax = flash_data->dens[icb];
						}
						if ( flash_data->temp[icb]<tmin )
						{
							tmin = flash_data->temp[icb];
						}
						if ( flash_data->temp[icb]>tmax )
						{
							tmax = flash_data->temp[icb];
						}
						
						//eos_f << flash_data->dens[icb] << " " << flash_data->temp[icb] << endl;

						//x = flash_data->coordinates[ib*3]+(ix-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
						//y = flash_data->coordinates[ib*3+1]+(iy-ncellx/2+.5)*flash_data->blocksize[ib*3+1]/ncellx;
						//z = flash_data->coordinates[ib*3+2]+(iz-ncellx/2+.5)*flash_data->blocksize[ib*3+2]/ncellx;
						
						/*if ( flash_data->eint[icb]>flash_data->ener[icb] )
						{
							cout << "Internal energy overflow!" << endl;
						}*/
						
						/*dx = flash_data->blocksize[ib*3]/ncellx;
						cs = flash_data->gamc[icb]*flash_data->pres[icb]/flash_data->dens[icb];
						cs = sqrt(cs);
						
						dt = min( 	dx/(abs(flash_data->velx[icb])+cs),
									dx/(abs(flash_data->vely[icb])+cs) );
						dt = min(   dt,
									dx/(abs(flash_data->velz[icb])+cs) );
						
						if ( dt_min<0 || dt<dt_min )
						{
							dt_min = dt;
							min_icb = icb;
							maxx = x;
							maxy = y;
							maxz = z;
							maxvel = max(abs(flash_data->velx[icb]),abs(flash_data->vely[icb]));
							maxvel = max(maxvel,abs(flash_data->velz[icb]));
							maxcs = cs;
						}*/
						
						/*if ( flash_data->eint[icb]>3.e17 )
						{
							cout << "Eint>3.e17" << " " << flash_data->eint[icb] << " " << flash_data->iproc[ib] << " " << ib << " " << x << " " << y << " " << z << endl;
							cout << flash_data->ener[icb] << endl;
						}*/
						
						//if ( flash_data->temp[icb]<temp_min )
						//{
							//temp_min = flash_data->temp[icb];
							//maxx = x;
							//maxy = y;
							//maxz = z;
							//maxib = ib;
							//maxip = flash_data->iproc[ib];
							//maxix = ix;
							//maxiy = iy;
							//maxiz = iz;
							//min_icb = icb;
							////cout << temp_min << " " << ib << " " << x << " " << y << " " << z << " " << ix+1 << " " << iy+1 << " " << iz+1 << endl;
						//}
						
						//if ( flash_data->eint[icb]-flash_data->kturb[icb]<0. )
						//{
							//cout << "EINT-KTURB<0" << " " << icb << endl;
							//cout << flash_data->eint[icb] << " " << flash_data->kturb[icb] << endl;
							//cout << ix << " " << iy << " " << iz << endl;
							//cout << x << " " << y << " " << z << endl;
						//}
						
						//mass_sum += flash_data->dens[icb]*cell_vol;
/*						en_sum += flash_data->eint[icb]*cell_vol;
						heat_sum += flash_data->eint[icb]*cell_vol*(1.-flash_data->kfrac[icb]);*/
						/*if ( flash_data->eint[icb]>max_ei )
						{
							max_ei = flash_data->eint[icb];
							maxx = x;
							maxy = y;
							maxz = z;
						}*/
						
//						CoM[0] += x * flash_data->dens[icb]*cell_vol;
//						CoM[1] += y * flash_data->dens[icb]*cell_vol;
//						CoM[2] += z * flash_data->dens[icb]*cell_vol;
					}
				}
			}
		}
	}
	
	cout << "Max/min t, max/min rho=" << tmax << " " << tmin << " " << rhomax << " " << rhomin << endl;
	for ( ib=0; ib<flash_data->nb; ib++ )
	{
		// Only do leaf blocks
		if ( flash_data->nodetype[ib]==1 )
		{
			cell_vol = (flash_data->blocksize[ib*3]*flash_data->blocksize[ib*3+1]*flash_data->blocksize[ib*3+2])/(ncellx*ncellx*ncellx);
			for ( int ix = 0 ; ix<ncellx ; ix++ )
			{
				for ( int iy = 0 ; iy<ncellx ; iy++ )
				{
					for ( int iz = 0 ; iz<ncellx ; iz++ )
					{
						ic = ix + iy*ncellx + iz*ncellx*ncellx;
						
						icb = ib*(ncellx*ncellx*ncellx) + ic;
						
						itemp = (int)floor(((log(flash_data->temp[icb])-log(tmin))*n_cells)/(log(tmax)-log(tmin)));
						irho = (int)floor(((log(flash_data->dens[icb])-log(rhomin))*n_cells)/(log(rhomax)-log(rhomin)));
						
						if ( itemp>=0 && itemp<n_cells && irho>=0 && irho<n_cells )
						{
							cell_weight[itemp][irho]+= cell_vol*flash_data->dens[icb];
						}
					}
				}
			}
		}
	}
	
	
	
	ofstream eos_f;
	eos_f.open(outfile);
	
	double plotval;
	for ( int i = 0 ; i<n_cells ; i++ )
	{
		for ( int j = 0 ; j<n_cells ; j++ )
		{
			if ( cell_weight[i][j]>0. )
			{
				plotval = log(cell_weight[i][j])*0.434294482;
			}
			else
			{
				plotval = -90;
			}
			eos_f << (i*((log(tmax)-log(tmin))/n_cells)+log(tmin))*0.434294482 << " " << (j*((log(rhomax)-log(rhomin))/n_cells)+log(rhomin))*0.434294482 << " " << (plotval) << endl;
		}
		eos_f << endl;
	}
	
	eos_f.close();
	return 0;
	
/*	for ( int i=0 ; i<3 ; i++ )
	{
		CoM[i]/=mass_sum;
	}*/
	
	
//	cout << "Total gas mass = " << mass_sum << " solar masses" << endl;
	
	//maxx/=3.08568025e18;
	//maxy/=3.08568025e18;
	//maxz/=3.08568025e18;
	
	cout << "Min temp is " << temp_min << endl;
	cout << "Coords = " << maxib << " " << maxx << " " << maxy << " " << maxz << endl;
	cout << maxix << " " << maxiy << " " << maxiz << endl;
	cout << maxix+1 << " " << maxiy+1 << " " << maxiz+1 << endl;
	cout << maxix+5 << " " << maxiy+5 << " " << maxiz+5 << endl;
	cout << "Proc = " << maxip << endl;
	
	return 0;
	
	icb = min_icb;
	cout << "X turb:" << flash_data->lturb[icb-1] << " " << flash_data->lturb[icb] << " " << flash_data->lturb[icb+1] << endl;
	cout << "X mut:" << flash_data->mut[icb-1] << " " << flash_data->mut[icb] << " " << flash_data->mut[icb+1] << endl;
	cout << "Left, right:" << (flash_data->mut[icb-1]+flash_data->mut[icb])*(flash_data->lturb[icb]-flash_data->lturb[icb-1]) << " "
						   << (flash_data->mut[icb-1]+flash_data->mut[icb])*(flash_data->lturb[icb+1]-flash_data->lturb[icb]) << endl;
	cout << "Total:" << (flash_data->mut[icb-1]+flash_data->mut[icb])*(flash_data->lturb[icb+1]-flash_data->lturb[icb])-
						(flash_data->mut[icb-1]+flash_data->mut[icb])*(flash_data->lturb[icb]-flash_data->lturb[icb-1]) << endl;
	
	
	return 0;
//	cout << "Total energy, heat energy, turbulent energy = " << en_sum << " " << heat_sum << " " << en_sum-heat_sum << endl;
//	cout << "Max_ei = " << max_ei << " x,y,z=" << maxx << " " << maxy << " " << maxy << endl;
//	cout << "Centre of mass coordinates:" << CoM[0] << " " << CoM[1] << " " << CoM[2] << endl;

	cout << "Minimum dt is C*" << dt_min << " and at " << min_icb << " x,y,z=" << maxx << " " << maxy << " " << maxz << endl;
	cout << "Min dt dens/pres/temp " << flash_data->dens[min_icb] << " " << flash_data->pres[min_icb] << " " << flash_data->temp[min_icb] << endl;
	cout << "Min dt cs,vel " << maxcs << " " << maxvel << endl;
	
	return 0;
	// Temperature/density/velocity vs radius (2D)
	
	cout << "Calculating profiles" << endl;
	
	
	double rad;
//	double rads[flash_data->nb];
//	int index[flash_data->nb];
	t_rad_index *rad_indices;
//	rad_indices = new *t_rad_index[flash_data->nb*ncellx*ncellx*ncellx];
	rad_indices = (t_rad_index *) malloc(sizeof(t_rad_index) * flash_data->nb*ncellx*ncellx*ncellx);
	
	cout << "Blocks:" << flash_data->nb << endl;
	cout << "Cells:" << flash_data->nb*ncellx*ncellx*ncellx << endl;

	for ( icb = 0 ; icb<flash_data->nb*(ncellx*ncellx*ncellx) ; icb++ )
	{
		rad_indices[icb].rad = -2.;
		rad_indices[icb].index = -2;
	}
	
	for ( int ib=0; ib<flash_data->nb; ib++ )
	{
		// Only do leaf blocks
		if ( flash_data->nodetype[ib]==1 )
		{
			for ( int ix = 0 ; ix<ncellx ; ix++ )
			{
				for ( int iy = 0 ; iy<ncellx ; iy++ )
				{
					for ( int iz = 0 ; iz<ncellx ; iz++ )
					{
						x = flash_data->coordinates[ib*3]+(ix-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
						y = flash_data->coordinates[ib*3+1]+(iy-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
						z = flash_data->coordinates[ib*3+2]+(iz-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
						
						rad=sqrt(x*x+z*z);
						
						icb = ib*(ncellx*ncellx*ncellx)+iz*(ncellx*ncellx)+iy*ncellx+ix;
						
						if ( icb>flash_data->nb*ncellx*ncellx*ncellx )
						{
							cout << "Housten, j'ai une probleme" << endl;
							cout << icb << endl;
						}
						
						rad_indices[icb].rad = rad;
						rad_indices[icb].index = icb;
						
	//					if ( abs(y)<2000. )
						{
//							dens_prof_f << rad << " " << icb;
						}
						
					}
				}
			}
		}
		else
		{
			for ( int i = 0 ; i<ncellx*ncellx*ncellx ; i++ )
			{
				icb = ib*(ncellx*ncellx*ncellx)+i;
//				rad_indices[icb].rad = numeric_limits <double>::max();
				rad_indices[icb].rad = -1.;
				rad_indices[icb].index = -1;
				
			}
		}
	}
	
	for ( icb = 0 ; icb<flash_data->nb*(ncellx*ncellx*ncellx) ; icb++ )
	{
		if ( rad_indices[icb].index < -1 )
		{
			cout << icb << " " << rad_indices[icb].index << endl;
		}
	}
	
	cout << "Sorting by radius" << endl;

	qsort(rad_indices, flash_data->nb*ncellx*ncellx*ncellx, sizeof(t_rad_index), compare_structs);
	
	cout << "**** Sorted ****" << endl;
	for ( icb = 0 ; icb<flash_data->nb*(ncellx*ncellx*ncellx) ; icb++ )
	{
		if ( rad_indices[icb].index < -1 )
		{
			cout << icb << " " << rad_indices[icb].index << endl;
		}
	}
	
	
	double bin_mass = mass_sum/n_bins;	
	t_bin bins[n_bins];
	int ibin,lastbin;
	double mcell,vcell;

	cout << "Lagrangian binning data" << endl;
	
	for ( ibin = 0 ; ibin<n_bins ; ibin++ )
	{
		bins[ibin].mass = 0.;
		bins[ibin].vol = 0.;
		bins[ibin].rad = 0.;
		bins[ibin].dens = 0.;
		bins[ibin].temp = 0.;
	}
	
	ibin = 0;
	
/*	for ( int i = 0 ; i<20 ; i++ )
	{
		cout << rad_indices[i].rad << " " << rad_indices[i].index << endl;
	}
	for ( int i = 0 ; i<1e4 ; i+=100 )
	{
		cout << rad_indices[i].rad << " " << rad_indices[i].index << endl;
	}*/

	for ( int i = 0 ; i<flash_data->nb*ncellx*ncellx*ncellx ; i++ )
	{
		rad = rad_indices[i].rad;
		icb = rad_indices[i].index;
		if ( rad!=rad )
		{
			cout << i << " " << icb << " " << rad << " isNaN!" << endl;
		}
		if ( icb>=0 )
		{
			ib = icb/(ncellx*ncellx*ncellx);
			vcell = pow(flash_data->blocksize[ib*3]/ncellx,3.);
			if ( ib>flash_data->nb || ib<0 )
			{
				cout << i << " " << icb << endl;
				cout << ib << " vs " << flash_data->nb << endl;
			}
			mcell = flash_data->dens[icb] * vcell;
			bins[ibin].mass += mcell;
			bins[ibin].rad += vcell*rad;
			bins[ibin].dens += vcell*flash_data->dens[icb];
			bins[ibin].temp += vcell*flash_data->temp[icb];
			bins[ibin].vol += vcell;
			
			if ( bins[ibin].mass>=bin_mass && ibin<n_bins-1 )
			{
				//cout << bins[ibin].rad << " " << bins[ibin].vol << " " << bins[ibin].mass << " " << bins[ibin].dens << endl;
				bins[ibin].rad/=bins[ibin].vol;
				bins[ibin].dens/=bins[ibin].vol;
				bins[ibin].temp/=bins[ibin].vol;
				cout << bins[ibin].rad << " " << bins[ibin].vol << " " << bins[ibin].mass << " " << bins[ibin].dens << endl;
				ibin++;
			}
		}
	}
	
	cout << "Loop out" << endl;
	
	if ( ibin<n_bins-1 && bins[ibin].vol>0. )
	{
		bins[ibin].rad/=bins[ibin].vol;
		bins[ibin].dens/=bins[ibin].vol;
		bins[ibin].temp/=bins[ibin].vol;
		bins[ibin].rad/=bins[ibin].vol;
		cout << bins[ibin].rad << " " << " " << bins[ibin].mass << " " << bins[ibin].dens << endl;
		ibin++;
	}
	
	lastbin=ibin;
	
	ofstream dens_prof_f;
	dens_prof_f.open("dens_prof_cyl.dat");
	for ( int ibin = 0 ; ibin<lastbin ; ibin ++ )
	{
//		if ( bins[ibin].vol>0. )
		{
			dens_prof_f << ibin << " " << bins[ibin].rad << " " << bins[ibin].dens << " " << bins[ibin].temp << " " << bins[ibin].mass << " " << bins[ibin].vol << endl;
		}
	}

	dens_prof_f.close();
}

int compare_structs (const void *a, const void *b){
     
    t_rad_index *struct_a = (t_rad_index *) a;
    t_rad_index *struct_b = (t_rad_index *) b;
     if (struct_a->index==-1 && struct_b->index==-1 ) return 0;
     if (struct_a->index==-1) return 1;
     if (struct_b->index==-1) return -1;

  
     if(struct_a->rad < struct_b->rad) return -1;
     if(struct_a->rad == struct_b->rad) return 0;
     if(struct_a->rad > struct_b->rad) return 1;

}
