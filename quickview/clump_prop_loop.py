import os
import time
import sys

irun = int(sys.argv[1])
istop = int(sys.argv[2])

for ii in range(0,istop+1):
	cmd = "./clump_properties /disc42/williams/flash/galreal/"+str(irun).rjust(4,'0')+"/ULIRG_"+str(irun).rjust(4,'0')+"_hdf5_chk_"+str(ii).rjust(4,"0")+" clump_data/clumps_full_"+str(irun).rjust(4,'0')+"_"+str(ii).rjust(4,"0")+".dat clump_data/clump_prop_"+str(irun).rjust(4,'0')+"_"+str(ii).rjust(4,"0")+".dat"
	print cmd
	os.system(cmd)
