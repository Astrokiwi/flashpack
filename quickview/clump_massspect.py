import sys
import os
import csv
import math

def import_text(filename, separator):
    for line in csv.reader(open(filename), delimiter=separator, 
                           skipinitialspace=True):
        if line:
            yield line

filename = (sys.argv[1])

masses = []

for data in import_text(filename," "):
			masses = masses + [float(data[7])]

#print masses

masses.sort()
masses.reverse()

#print masses

ii = len(masses)
for m in masses:
	print ii,m
	ii = ii - 1
