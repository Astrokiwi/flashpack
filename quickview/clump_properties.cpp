#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
	char* inputfile = argv[1];
	char* clumpfile = argv[2];
	char* outfile = argv[3];
	
	my_reader = new flash_reader(inputfile);
	my_reader->readDens();
	my_reader->readVels();
	ifstream clump_file;
	clump_file.open(clumpfile);
	my_reader->readClumps(&clump_file);
	clump_file.close();
	my_reader->cm_to_pc();
	my_reader->to_msunpc3();
	my_reader->to_kms();
	flash_data = my_reader->getData();
	my_reader->close();
	
	int n_clumps = flash_data->n_clumps;
	int n_cells = flash_data->nb*ncellx*ncellx*ncellx;
	double mass[n_clumps],mom[n_clumps][3],vel[n_clumps][3],r[n_clumps][3],clump_cells[n_clumps],vol[n_clumps];
	double minr[n_clumps][3],maxr[n_clumps][3];
	
	for ( int ic = 0 ; ic<n_clumps ; ic++ )
	{
		mass[ic] = 0.;
		clump_cells[ic] = 0;
		for ( int ii = 0 ; ii<3 ; ii ++ )
		{
			mom[ic][ii] = 0.;
			vel[ic][ii] = 0.;
			minr[ic][ii] = 1.e20;
			maxr[ic][ii] = -1.e20;
		}
		vol[ic] = 0.;
	}
	
	for ( int cc = 0 ; cc<n_cells ; cc++ )
	{
		int icc = flash_data->iclump[cc];
		if ( icc>=0 )
		{
			if ( icc>=n_clumps )
			{
				cout << "Reprocessed sludge error" << endl;
				exit(0);
			}
			int ixyz[3];
			double cell_r[3],cell_v[3];
			int ib = cc/(ncellx*ncellx*ncellx);
			double cell_vol,cell_mass;
			
			ixyz[0] = cc%ncellx;
			ixyz[1] = (cc/ncellx)%ncellx;
			ixyz[2] = (cc/(ncellx*ncellx))%(ncellx);
			
			cell_r[0] = flash_data->coordinates[ib*3]+(ixyz[0]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
			cell_r[1] = flash_data->coordinates[ib*3+1]+(ixyz[1]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
			cell_r[2] = flash_data->coordinates[ib*3+2]+(ixyz[2]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
			
			cell_vol = pow(flash_data->blocksize[ib*3]/ncellx,3);
			cell_mass = cell_vol * flash_data->dens[cc];
			
			cell_v[0] = flash_data->velx[cc];
			cell_v[1] = flash_data->vely[cc];
			cell_v[2] = flash_data->velz[cc];
			
			for ( int ii = 0 ; ii<3 ; ii++ )
			{
				r[icc][ii]+=cell_r[ii]*cell_mass;
				mom[icc][ii]+=cell_v[ii]*cell_mass;
				minr[icc][ii] = min(minr[icc][ii],cell_r[ii]);
				maxr[icc][ii] = max(maxr[icc][ii],cell_r[ii]);
			}
			mass[icc]+=cell_mass;
			clump_cells[icc]++;
			vol[icc]+=cell_vol;
		}
	}
	
	cout << "Dumping to outfile " << outfile << endl;
	ofstream out_file;
	out_file.open(outfile);
	for ( int ic = 0 ; ic<n_clumps ; ic++ )
	{
		for ( int ii = 0 ; ii<3 ; ii ++ )
		{
			vel[ic][ii] = mom[ic][ii]/mass[ic];
			r[ic][ii] = r[ic][ii]/mass[ic];
		}
		
		out_file << ic << " " << r[ic][0] << " " << r[ic][1] << " " << r[ic][2] << " " << vel[ic][0] << " " << vel[ic][1] << " " << vel[ic][2] << " " << clump_cells[ic] << " " << mass[ic] << " " << vol[ic] << " " << minr[ic][0] <<" " << minr[ic][1] << " " << minr[ic][2] << " " << maxr[ic][0] << " " << maxr[ic][1] << " " << maxr[ic][2] << endl;
	}
	out_file.close();
	cout << "Work complete!" << endl;
}

