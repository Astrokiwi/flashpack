# SO MANY PHASEPLOTS
# nb does not actually do plots, just gets the data
import os
import sys

# iruns = [42, 43, 44, 48, 49, 50, 51, 52, 54, 57, 65, 66, 67]
iruns = [1, 2, 4, 5, 8, 9, 10, 18, 19, 20, 21, 22, 25, 26, 27, 28, 38, 39]
max_itimes = []

for irun in iruns:
	dir = "/disc42/williams/flash/ismbox_numberedruns/ism_"+str(irun).zfill(4)
	os.system("ls -sort "+dir+" | tail -1 > 1")
	f = open("1")
	filename = f.readlines()[0].split()[-1]
	f.close()
	max_itime = int(filename[-4:])
	max_itimes.append(max_itime)


for i in range(0,len(iruns)):
	irun = iruns[i]
	itime = max_itimes[i]
	str_irun = str(irun).zfill(4)
	str_itime = str(itime).zfill(4)
	command = "./densitypdf /disc42/williams/flash/ismbox_numberedruns/ism_"+str_irun+"/ISM_"+str_irun+"_hdf5_plt_cnt_"+str_itime+" rhopdfs/rhopdf_"+str_irun+".dat"
	print command
	os.system(command)


