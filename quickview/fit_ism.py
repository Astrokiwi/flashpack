import os
from math import log10,sqrt
import numpy
import scipy

print "Reading"

f = open("ismbox_summaries/summary_converged.dat")
data = [x.split() for x in f.readlines()]
f.close()

logt = [log10(float(x[0])) for x in data]
logrho = [log10(float(x[1])) for x in data]
turbf = [float(x[2]) for x in data]
coldf = [float(x[3]) for x in data]
id = [int(x[4]) for x in data]

N = len(id)

print "Working"

# 
# mint = 2.
# maxt = 6.3
# mintb = 0.
# maxtb = 1.
# 
# L = 128
# 
# coldf_grid = [[0.]*L for x in range(0,L)]
# 
# # Vorinoi "fitting"
# for ix in range(0,L):
# 	for iy in range(0,L):
# 		my_logt = ((maxt-mint)*ix)/L+mint
# 		my_turbf = ((maxtb-mintb)*iy)/L+mintb
# 		my_coldf = 0.
# 		min_dist = 1.e20
# 		for i in range(0,N):
# 			dist = sqrt(((my_logt-logt[i])/(maxt-mint))**2+((my_turbf-turbf[i])/(maxtb-mintb))**2)
# 			if ( dist<min_dist ):
# 				min_dist = dist
# 				my_coldf = coldf[i]
# 		coldf_grid[ix][iy] = my_coldf
# 
# # Make gnuplot output
# 
# f = open("ismbox_summaries/vorinoi.dat",'w')
# for ix in range(0,L):
# 	for iy in range(0,L):
# 		my_logt = ((maxt-mint)*ix)/L+mint
# 		my_turbf = ((maxtb-mintb)*iy)/L+mintb
# 		f.write(str(my_logt)+" "+str(my_turbf)+" "+str(coldf_grid[ix][iy])+"\n")
# 	f.write('\n')
# f.close()
# 
# # weighted "fitting"
# for ix in range(0,L):
# 	for iy in range(0,L):
# 		my_logt = ((maxt-mint)*ix)/L+mint
# 		my_turbf = ((maxtb-mintb)*iy)/L+mintb
# 		my_coldf = 0.
# 		weight_sum = 0.
# 		for i in range(0,N):
# 			dist = sqrt(((my_logt-logt[i])/(maxt-mint))**2+((my_turbf-turbf[i])/(maxtb-mintb))**2)
# 			weight = 1./(dist+0.001)**4
# 			my_coldf = my_coldf + weight*coldf[i]
# 			weight_sum = weight_sum + weight
# 		coldf_grid[ix][iy] = my_coldf/weight_sum
# 
# # Make gnuplot output
# 
# f = open("ismbox_summaries/weighted.dat",'w')
# for ix in range(0,L):
# 	for iy in range(0,L):
# 		my_logt = ((maxt-mint)*ix)/L+mint
# 		my_turbf = ((maxtb-mintb)*iy)/L+mintb
# 		f.write(str(my_logt)+" "+str(my_turbf)+" "+str(coldf_grid[ix][iy])+"\n")
# 	f.write('\n')
# f.close()
