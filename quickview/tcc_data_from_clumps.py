import sys
import os
from math import sqrt

infile = sys.argv[1]

f = open(infile)
data = [[float(y) for y in x.split()] for x in f.readlines()]
f.close()

outdata = []

#rho_background = 10**(-28.6)
#v_background = 330.

rho_background = 10**(-23.6)
v_background = 2000.


for line in data:
	rad = [(line[13+i]-line[10+i])*3.08567758e13 for i in [0,1,2]] # km
	mass = line[8]
	vol = line[9]
	rad2 = vol**0.333333*3.08567758e13 #km
	density = mass/vol # Msun/pc^3
	vel = abs(sqrt(line[4]**2 + line[5]**2 + line[6]**2))
	outdata.append([max(rad),rad2,density*6.7702543e-23,vel])
#	print max(rad),rad2
#	print vel
	print (sqrt(density*6.7702543e-23/rho_background)*rad2/(v_background-vel))/3.14e7/1e6

aves = map(list, zip(*outdata))
aves = [sum(x)/len(x) for x in aves]
#print aves


print "ave:"

#print aves[3]

#print (sqrt(aves[2]/rho_background)*aves[1]/v_background)/3.14e7/1e3
print (sqrt(aves[2]/rho_background)*aves[1]/(v_background-aves[3]))/3.14e7/1e6
