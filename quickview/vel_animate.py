#!/bin/python
# Python script to convert a bunch of profiles with gnuplot into a bunch of images

import sys
import os

# What iterations to get the work on
start = int(sys.argv[2])
finish = int(sys.argv[3])

# The run number
runnum = int(sys.argv[1])

for x in range(start,finish+1,1):
	outfilename = "/fastscratch/williams/flash_plots/temp_map"+str(runnum).rjust(4,"0")+"_"+str(x).rjust(4,"0")+".dat"
	infilename = "/disc42/williams/flash/galreal/"+str(runnum).rjust(4,"0")+"/ULIRG_"+str(runnum).rjust(4,"0")+"_hdf5_chk_"+str(x).rjust(4,"0")
	execflags = "-x 0. -velvec -step 10"
	out_exec = "./sliceplot -f "+infilename+" "+execflags+" -o "+outfilename
	os.system(out_exec)

command = "convert -delay 16 "

#print "Converting profiles to graphs with gnuplot"
for x in range(start,finish+1,1):
	filename = "/fastscratch/williams/flash_plots/temp_map"+str(runnum).rjust(4,"0")+"_"+str(x).rjust(4,"0")+".dat"
	print str(((x-start)*100.)/(finish-start+1))+"% of images made"
	f = open("gnuplotscript",'w')
	f.write("set term postscript color\n") 
	f.write("set output '/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps'\n")
	command+="/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png "

	f.write("set title '"+filename+"'\n")
	f.write("plot '"+filename+"' u 1:2:($3/5.e6):($4/5.e6) w vec\n")
	f.close()
	os.system("gnuplot gnuplotscript")
	os.system("convert /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps -rotate 90 /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png")


print "ps/png files made"
print "Encoding movie"

command+= "velmovie"+str(runnum)+".gif"
os.system(command)

print "Complete."

