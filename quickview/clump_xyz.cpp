#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
	char* inputfile = argv[1];
	char* clumpfile = argv[2];
	char* outfile = argv[3];
	int clump_of_interest = atoi(argv[4]);
	
	my_reader = new flash_reader(inputfile);
	ifstream clump_file;
	clump_file.open(clumpfile);
	my_reader->readClumps(&clump_file);
	clump_file.close();
	my_reader->cm_to_pc();
	flash_data = my_reader->getData();
	my_reader->close();
	
	int n_cells = flash_data->nb*ncellx*ncellx*ncellx;
	
	ofstream out_file;
	out_file.open(outfile);

	cout << "Finding positions and dumping to outfile " << outfile << endl;
	for ( int cc = 0 ; cc<n_cells ; cc++ )
	{
		int icc = flash_data->iclump[cc];
		if ( icc==clump_of_interest || (clump_of_interest==-2 && icc>=0) )
		{
			int ixyz[3];
			double cell_r[3];
			int ib = cc/(ncellx*ncellx*ncellx);
			
			ixyz[0] = cc%ncellx;
			ixyz[1] = (cc/ncellx)%ncellx;
			ixyz[2] = (cc/(ncellx*ncellx))%(ncellx);
			
			cell_r[0] = flash_data->coordinates[ib*3]+(ixyz[0]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
			cell_r[1] = flash_data->coordinates[ib*3+1]+(ixyz[1]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
			cell_r[2] = flash_data->coordinates[ib*3+2]+(ixyz[2]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
			out_file << cell_r[0] << " " << cell_r[1] << " " << cell_r[2] << endl;
		}
	}
	
	out_file.close();
	cout << "Work complete!" << endl;
}

