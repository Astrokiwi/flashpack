#!/bin/python
# Python script to convert a bunch of profiles with gnuplot into a bunch of images

import sys
import os

class ClumpTrackEntry:
     def __init__(self, x, y):
         self.idump = x
         self.iclump = y

# The run number
runnum = int(sys.argv[1])

# What iterations to get the work on
#start = int(sys.argv[2])
#finish = int(sys.argv[3])
clump_last = int(sys.argv[2])

f = open("clump_data/clumptrack"+str(runnum).rjust(4,'0')+"_"+str(clump_last)+".dat")
indata = [x.split() for x in f.readlines()]
clumptrackentries = [ClumpTrackEntry(int(x[0]),int(x[1])) for x in indata]
f.close()

for clumpentry in clumptrackentries:
	# Find xyz
	f = open("clump_data/clump_prop_"+str(runnum).rjust(4,'0')+"_"+str(clumpentry.idump).rjust(4,'0')+".dat")
	indata = [x.split() for x in f.readlines()]
	f.close()
	clumpentry.pos = [float(x) for x in indata[clumpentry.iclump][1:4]]
	#print indata[clumpentry.iclump]

for clumpentry in clumptrackentries:
	outfilename = "/fastscratch/williams/flash_plots/temp_map"+str(runnum).rjust(4,"0")+"_"+str(clumpentry.idump).rjust(4,"0")+".dat"
	infilename = "/disc42/williams/flash/galreal/"+str(runnum).rjust(4,"0")+"/ULIRG_"+str(runnum).rjust(4,"0")+"_hdf5_chk_"+str(clumpentry.idump).rjust(4,"0")
	execflags = "-x "+str(clumpentry.pos[0])+" -dens"
	out_exec = "./sliceplot -f "+infilename+" "+execflags+" -o "+outfilename
	os.system(out_exec)

command = "convert -delay 16 "

#print "Converting profiles to graphs with gnuplot"
for clumpentry in reversed(clumptrackentries):
	filename = "/fastscratch/williams/flash_plots/temp_map"+str(runnum).rjust(4,"0")+"_"+str(clumpentry.idump).rjust(4,"0")+".dat"
	#print str(((x-start)*100.)/(finish-start+1))+"% of images made"
	f = open("gnuplotscript",'w')
#	f.write("set term postscript color\n") 
	f.write("set term png enhanced\n") 
#	f.write("set output '/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps'\n")
	f.write("set output '/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(clumpentry.idump).rjust(5,"0")+".png'\n")
	command+="/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(clumpentry.idump).rjust(5,"0")+".png "
	
	f.write("set title '"+filename+"'\n")
	f.write("set pm3d map corners2color c1\n")
	f.write("set cbrange [-29:-24]\n") # density
	f.write("set xrange ["+str(clumpentry.pos[1]-20000)+":"+str(clumpentry.pos[1]+20000)+"]\n")
	f.write("set yrange ["+str(clumpentry.pos[2]-20000)+":"+str(clumpentry.pos[2]+20000)+"]\n")
	#f.write("set cbrange [-26:-19]\n") # density central
	#f.write("set xrange[-50000:0];set yrange[-25000:25000]\n")
	f.write("splot '"+filename+"' u 1:2:3\n")
	f.close()
	os.system("gnuplot gnuplotscript")
#	os.system("convert /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps -rotate 90 /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png")


print "ps/png files made"
print "Encoding movie"

command+= "clumpmovie"+str(runnum)+"_"+str(clump_last)+".gif"
os.system(command)

print "Complete."

