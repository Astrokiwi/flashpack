#!/bin/python
# Python script to convert a bunch of profiles with gnuplot into a bunch of images

import sys
import os

# What iterations to get the work on
start = int(sys.argv[2])
finish = int(sys.argv[3])

# The run number
runname = sys.argv[1]

runnum = int(sys.argv[4])

for x in range(start,finish+1,1):
	outfilename = "/fastscratch/williams/flash_plots/temp_map"+str(runnum).rjust(4,"0")+"_"+str(x).rjust(4,"0")+".dat"
	#infilename = "/disc42/williams/flash/galreal/"+str(runnum).rjust(4,"0")+"/ULIRG_"+str(runnum).rjust(4,"0")+"_hdf5_chk_"+str(x).rjust(4,"0")
	infilename = runname+str(x).rjust(4,"0")
	execflags = "-x 50. -temp"
	#execflags = "-x -20000. -dens"
	out_exec = "./sliceplot -f "+infilename+" "+execflags+" -o "+outfilename
	os.system(out_exec)

command = "convert -delay 16 "

#print "Converting profiles to graphs with gnuplot"
for x in range(start,finish+1,1):
	filename = "/fastscratch/williams/flash_plots/temp_map"+str(runnum).rjust(4,"0")+"_"+str(x).rjust(4,"0")+".dat"
	print str(((x-start)*100.)/(finish-start+1))+"% of images made"
	f = open("gnuplotscript",'w')
#	f.write("set term postscript color\n") 
	f.write("set term png enhanced\n") 
#	f.write("set output '/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps'\n")
	f.write("set output '/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png'\n")
	command+="/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png "

	f.write("set title '"+filename+"'\n")
	f.write("set pm3d map corners2color c1\n")
	f.write("set cbrange [2.:7.]\n") # temperature
	#f.write("set cbrange [-26:-19]\n") # density central
	#f.write("set xrange[-50000:0];set yrange[-25000:25000]\n")
	#f.write("set xrange[-120:100];set yrange[-120:120]\n") # Large galaxy box
	f.write("set xlabel 'pc';set ylabel 'pc'\n")
	f.write("set cblabel 'K'\n")
	f.write("unset key;unset title\n")
	f.write("set size square\n")
	f.write("splot '"+filename+"' u 1:2:3\n")
#	f.write("splot '"+filename+"' u ($1/1.e3):($2/1.e3):3\n") # kpc
	f.close()
	os.system("gnuplot gnuplotscript")
#	os.system("convert /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps -rotate 90 /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png")


print "ps/png files made"
print "Encoding movie"

command+= "tempmovie"+str(runnum)+".gif"
#command+= "densmovie"+str(runnum)+".mpeg"
os.system(command)

print "Complete."

