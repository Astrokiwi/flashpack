#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

void unrevolve(flash_data_t *flash_data)
{
	const int n_bins = 50;
	//const int n_bins = 200;
	//double r_max = 3.08568025e18*1.e5;
	//double z_max = 3.08568025e18*1.e5;
	double r_max = 3.08568025e18*1.e2;
	double z_max = 3.08568025e18*2.e2;
	int cc,ibin,jbin;
	double rotv[n_bins][n_bins],rotmass[n_bins][n_bins];
	double vcirc,rad,mass,vol;
	double cell_r[3];
	double fac1,fac2;
	for ( ibin = 0 ; ibin<n_bins ; ibin++ )
	{
		for ( jbin = 0 ; jbin<n_bins ; jbin++ )
		{
			rotv[ibin][jbin] = 0.;
			rotmass[ibin][jbin] = 0.;
		}
	}
	for ( int ib = 0 ; ib<flash_data->nb ; ib++ )
	{
		vol = pow(flash_data->blocksize[ib*3]/ncellx/pc_in_cm,3); // pc^3 volume
		for ( int ix = 0 ; ix<ncellx ; ix++ )
		{
			for ( int iy = 0 ; iy<ncellx ; iy++ )
			{
				for ( int iz = 0 ; iz<ncellx ; iz++ )
				{
					cc = ib*(ncellx*ncellx*ncellx)+iz*(ncellx*ncellx)+iy*(ncellx)+ix;
					cell_r[0] = flash_data->coordinates[ib*3]+(ix-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
					cell_r[1] = flash_data->coordinates[ib*3+1]+(iy-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
					cell_r[2] = flash_data->coordinates[ib*3+2]+(iz-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;;
					
					//if ( abs(cell_r[1])<r_max/10. )
					{
						
						rad = sqrt(cell_r[0]*cell_r[0]+cell_r[2]*cell_r[2]);
						vcirc = (cell_r[0] * flash_data->velz[cc] - cell_r[2]*flash_data->velx[cc])/rad;
						
						ibin = (int)floor((rad*n_bins)/r_max);
						jbin = (int)floor(abs(cell_r[1]*n_bins)/z_max);
						if ( ibin<n_bins && jbin<n_bins)
						{
							mass = vol * flash_data->dens[cc]; // g/cm^3 * pc^3 - not consistent, but we're just using it for a weighting
							rotv[ibin][jbin]+=vcirc*mass;
							rotmass[ibin][jbin]+=mass;
							//if ( ibin==72 )
							//{
								//cout << vol << " " << mass << " " << " " << flash_data->dens[cc] << " " << vcirc << " " << rad << " " << ibin << " " << rotmass[ibin] << " " << rotv[ibin] << endl;
							//}
							//if ( rotmass[ibin][jbin]!=rotmass[ibin][jbin] )
							//{
								//cout << cc << endl;
								//cout << vol << " " << mass << " " << " " << flash_data->dens[cc] << " " << vcirc << " " << rad << " " << ibin << " " << rotmass[ibin] << " " << rotv[ibin] << endl;
								//exit(0);
							//}
						}
					}
				}
			}
		}
	}
	
	//ofstream rot_prof;

	//rot_prof.open("rot_prof1.dat");
	for ( ibin = 0 ; ibin<n_bins ; ibin++ )
	{
		for ( jbin = 0 ; jbin<n_bins ; jbin++ )
		{
			if ( rotmass[ibin][jbin]>0. )
			{
				rotv[ibin][jbin]/=rotmass[ibin][jbin];
			}
		}
		//rot_prof << ibin << " " << rotv[ibin] << " " << rotmass[ibin] << endl;
	}
	//rot_prof.close();
	//exit(0);
	
	// Look through cells, remove circular velocity
	for ( int ib = 0 ; ib<flash_data->nb ; ib++ )
	{
		vol = pow(flash_data->blocksize[ib*3]/ncellx/pc_in_cm,3); // pc^3 volume
		for ( int ix = 0 ; ix<ncellx ; ix++ )
		{
			for ( int iy = 0 ; iy<ncellx ; iy++ )
			{
				for ( int iz = 0 ; iz<ncellx ; iz++ )
				{
					cc = ib*(ncellx*ncellx*ncellx)+iz*(ncellx*ncellx)+iy*(ncellx)+ix;
					cell_r[0] = flash_data->coordinates[ib*3]+(ix-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
					cell_r[1] = flash_data->coordinates[ib*3+1]+(iy-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
					cell_r[2] = flash_data->coordinates[ib*3+2]+(iz-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;;
					
					//if ( abs(cell_r[1])<r_max/10. )
					{
						mass = vol * flash_data->dens[cc]; // g/cm^3 * pc^3 - not consistent, but we're just using it for a weighting

						rad = sqrt(cell_r[0]*cell_r[0]+cell_r[2]*cell_r[2]);
						
						ibin = (int)floor((rad*n_bins)/r_max-.5);
						jbin = (int)floor((abs(cell_r[1])*n_bins)/z_max-.5);
						
						if ( ibin<n_bins && jbin<n_bins)
						{
							if ( ibin==n_bins-1 )
							{
								if ( jbin<n_bins-1 )
								{
									fac2 = ((abs(cell_r[1])*n_bins)/z_max-jbin-.5);
									vcirc = (1-fac2)*rotv[ibin][jbin]+fac2*rotv[ibin][jbin+1];
								}
								else if ( jbin==-1 )
								{
									vcirc = rotv[ibin][0];
								}
								else if ( jbin==n_bins-1 )
								{
									vcirc = rotv[ibin][jbin];
								}
								//vcirc = vcirc * mass/rotmass[ibin];
							}
							else if ( ibin==-1 )
							{
								if ( jbin<n_bins-1 )
								{
									fac2 = ((abs(cell_r[1])*n_bins)/z_max-jbin-.5);
									vcirc = (1-fac2)*rotv[0][jbin]+fac2*rotv[ibin][jbin+1];
								}
								else if ( jbin==-1 )
								{
									vcirc = rotv[0][0];
								}
								else if ( jbin==n_bins-1 )
								{
									vcirc = rotv[0][jbin];
								}
								//vcirc = rotv[0];
								//vcirc = vcirc * mass/rotmass[0];
								
							}
							else if (jbin==-1 )
							{
								fac1 = ((rad*n_bins)/r_max-ibin-.5);
								vcirc = (1-fac1)*rotv[ibin][0]+fac1*rotv[ibin+1][0];
							}
							else if (jbin==n_bins-1)
							{
								fac1 = ((rad*n_bins)/r_max-ibin-.5);
								vcirc = (1-fac1)*rotv[ibin][jbin]+fac1*rotv[ibin+1][jbin];
							}
							else
							{
								fac1 = ((rad*n_bins)/r_max-ibin-.5);
								fac2 = ((abs(cell_r[1])*n_bins)/z_max-jbin-.5);
								//fac = 0.;
								
								//vcirc = (1-fac)*rotv[ibin] + fac*rotv[ibin+1];
								//vcirc = vcirc * mass/((1-fac)*rotmass[ibin]+fac*rotmass[ibin+1];
								vcirc = (1-fac1)*((1-fac2)*rotv[ibin][jbin]+fac2*rotv[ibin][jbin+1])+fac1*((1-fac2)*rotv[ibin+1][jbin]+fac2*rotv[ibin+1][jbin+1]);
							}
							
							flash_data->velx[cc]+=cell_r[2]*vcirc/rad;
							flash_data->velz[cc]-=cell_r[0]*vcirc/rad;
						}
						
					}
				}
			}
		}
	}
	
}

///////////

int ipow(int base, int exp)
{
    int result = 1;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }

    return result;
}

///////////

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
//	char* inputfile = "/disc42/williams/flash/galreal/ULIRG_0003_hdf5_chk_0010";
	char *inputfile,*outfile;
	char *clumpfile;
	int n_cell;
	double cell_w;
	double cell_r[3];
	int ijk[3];
	int ib,ix,iy,iz,icb;
	int ii;
	
	bool derevolve;
	bool flatten;

	double box_dims[3][2];
	double block_dims[3][2];
	
	int min_ref,max_ref;
	double min_size,max_size;
	
	int l_cells[3];
	double slice_coord;
	int slice_dim;
	
	int cd[2];
	int cx,cy,cxi,cxf,cyi,cyf;
	int id;
	
	int ref_mult;
	bool file_set;
	
	int scalar;
	
	bool dolog;
	bool vecplot;
	
	double logtrunc;
	
	double plotval,plotval2;
	
	double rot; // Rotate this much
	double unrot_r[3];
	int ir0,ir1;
	
	double rad;
	double vtot;
	
	double sdev;
	double const sodium_dens_cut = 1.e-21; // For turbulent gas, above 10^(-21) g/cm^3 cools very quickly, assume there is some Na in there
	double const turb_b = 1.; // b=1/3 for solenoidal forcing, b=1 for compressive forcing
	
	int deresfac;
	int nderes,deres_step;
	
	bool do_deres;
	
	int step;
	int smooth;
	
	double slice_min,slice_max,slice_step;
	
	// Set default parameters
	slice_coord = 0.;
	slice_dim = 0;
	file_set = false;
	outfile = "slice_map.dat";
	scalar = 0; // Assume density
	dolog = true;
	vecplot = false;
	do_deres = false;
	logtrunc = -90; // If doing a log scale & values are less than 10^(-logtrunc) then round up to 10^(-logtrunc). This is to deal with zeroes on a logplot.
	rot = 0.;
	step = 1;
	smooth = 0;
	derevolve = false;
	flatten = false;
	
	// Read input parameters
	
	ii = 1;
	
	while ( ii<argc )
	{ 
		if ( strcmp(argv[ii],"-f")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				inputfile = argv[ii];
				file_set = true;
				cout << "Inputting from  file: " << inputfile << endl;
			}
		}
		else if ( strcmp(argv[ii],"-o")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				outfile = argv[ii];
				file_set = true;
				cout << "Outputting to file: " << outfile << endl;
			}
		}
		else if ( strcmp(argv[ii],"-x")==0 )
		{
			slice_dim = 0;
			ir0 = 1;
			ir1 = 2;
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				slice_coord = atof(argv[ii]);
				cout << "Slicing x plane at " << slice_coord << " pc" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-y")==0 )
		{
			slice_dim = 1;
			ir0 = 0;
			ir1 = 2;
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				slice_coord = atof(argv[ii]);
				cout << "Slicing y plane at " << slice_coord << " pc" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-z")==0 )
		{
			slice_dim = 2;
			ir0 = 0;
			ir1 = 1;
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				slice_coord = atof(argv[ii]);
				cout << "Slicing z plane at " << slice_coord << " pc" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-logtrunc")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				logtrunc = atof(argv[ii]);
				cout << "Setting floor to 10^(" << logtrunc << ")" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-rot")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				rot = atof(argv[ii]);
				cout << "Rotating by " << rot << " degrees" << endl;
			}
		}
		else if ( strcmp(argv[ii],"-derev")==0 )
		{
			derevolve = true;
			cout << "Removing circular velocity - if velocities aren't loaded, this will crash" << endl;
		}
		else if ( strcmp(argv[ii],"-dens")==0 )
		{
			scalar = 0;
			cout << "Density slice" << endl;
		}
		else if ( strcmp(argv[ii],"-temp")==0 )
		{
			scalar = 1;
			cout << "Temperature slice" << endl;
		}
		else if ( strcmp(argv[ii],"-kturb")==0 )
		{
			scalar = 2;
			cout << "Turbulent kinetic energy slice" << endl;
		}
		else if ( strcmp(argv[ii],"-kfrac")==0 )
		{
			scalar = 3;
			cout << "Turbulent K fraction slice" << endl;
		}
		else if ( strcmp(argv[ii],"-lturb")==0 )
		{
			scalar = 4;
			cout << "Turbulent L fraction slice" << endl;
		}
		else if ( strcmp(argv[ii],"-ref")==0 )
		{
			scalar = 5;
			cout << "Refinement level slice" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-eint")==0 )
		{
			scalar = 6;
			cout << "Internal energy slice" << endl;
		}
		else if ( strcmp(argv[ii],"-velx")==0 )
		{
			scalar = 7;
			cout << "x velocity slice" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-vely")==0 )
		{
			scalar = 8;
			cout << "y velocity slice" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-velz")==0 )
		{
			scalar = 9;
			cout << "z velocity slice" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-tvisc")==0 )
		{
			scalar = 10;
			cout << "Colculate turbulent viscosity slice" << endl;
		}
		else if ( strcmp(argv[ii],"-pres")==0 )
		{
			scalar = 11;
			cout << "Pressure slice" << endl;
		}
		else if ( strcmp(argv[ii],"-mut")==0 )
		{
			scalar = 12;
			cout << "Stored turbulent viscosity slice" << endl;
		}
		else if ( strcmp(argv[ii],"-kint")==0 )
		{
			scalar = 13;
			cout << "Initial turbulent kinetic energy slice" << endl;
		}
		else if ( strcmp(argv[ii],"-velvec")==0 )
		{
			scalar = 14;
			cout << "Velocity vector slice" << endl;
			vecplot = true;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-kdel")==0 )
		{
			scalar = 15;
			cout << "Internal energy minus turbulent K slice" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-thfrac")==0 )
		{
			scalar = 16;
			cout << "Thermal internal energy fraction slice" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-coolf")==0 )
		{
			scalar = 17;
			cout << "Dens/cool gas fraction (from turbulence)" << endl;
		}
		else if ( strcmp(argv[ii],"-cooldens")==0 )
		{
			scalar = 18;
			cout << "Dens/cool gas fraction density (from turbulence)" << endl;
		}
		else if ( strcmp(argv[ii],"-kderes")==0 )
		{
			scalar = 19;
			do_deres = true;
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				deresfac = atoi(argv[ii]);
				cout << "Calculating K by reducing resolution by a factor or 2^" << deresfac << endl;
			}
		}
		else if ( strcmp(argv[ii],"-deres")==0 )
		{
			do_deres = true;
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				deresfac = atoi(argv[ii]);
				cout << "Reducing resolution by a factor of 2^" << deresfac << endl;
			}
		}
		else if ( strcmp(argv[ii],"-step")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				step = atoi(argv[ii]);
				cout << "Dump step=" << step << endl;
			}
		}
		else if ( strcmp(argv[ii],"-smooth")==0 )
		{
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				smooth = atoi(argv[ii]);
				cout << "Smooth length (In smallest-cell lengths):" << smooth << endl;
			}
		}
		else if ( strcmp(argv[ii],"-time")==0 )
		{
			scalar = 20;
			cout << "Getting time" << endl;
		}
		else if ( strcmp(argv[ii],"-calctemp")==0 )
		{
			scalar = 21;
			cout << "Calculating temperature from internal energy" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-vcirc")==0 )
		{
			scalar = 22;
			cout << "Calculating circular velocities" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-clump")==0 )
		{
			scalar = 23;
			cout << "Clump number slice" << endl;
			dolog = false;
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				clumpfile = argv[ii];
			}
		}
		else if ( strcmp(argv[ii],"-vplane")==0 )
		{
			scalar = 24;
			cout << "Calculating norm of planar velocities" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-ycoord")==0 )
		{
			scalar = 25;
			cout << "Giving exact y coordinates" << endl;
			dolog = false;
		}
		else if ( strcmp(argv[ii],"-clumpflat")==0 )
		{
			scalar = 26;
			cout << "Clump number slice" << endl;
			cout << "FLATTENING!" << endl;
			dolog = false;
			flatten = true;
			ii++;
			if ( ii==argc )
			{
				cout << argv[ii] << " needs an argument\n";
				exit(0);
			}
			else
			{
				clumpfile = argv[ii];
			}
			cout << "Clump file = " << clumpfile  << endl;
		}
		else if ( strcmp(argv[ii],"-denscolumn")==0 )
		{
			scalar = 0;
			flatten = true;
			cout << "Density slice" << endl;
		}
		else if ( strcmp(argv[ii],"-tempmin")==0 )
		{
			scalar = 1;
			flatten = true;
			cout << "Density slice" << endl;
		}
		else if ( strcmp(argv[ii],"-log")==0 )
		{
			dolog = true;
			cout << "Taking log10 of output" << endl;
		}
		else if ( strcmp(argv[ii],"-nolog")==0 )
		{
			dolog = false;
			cout << "Not taking log of output" << endl;
		}
		else
		{
			cout << "Unknown argument: " << argv[ii] << endl;
		}
		ii++;
	}
	
	if ( !file_set )
	{
		cout << "Need an input file-name!" << endl;
		exit(0);
	}

	id = 0;
	for ( int ii = 0 ; ii<3 ; ii++ )
	{
		if ( ii!=slice_dim )
		{
			cd[id] = ii;
			id++;
		}
	}
	
	my_reader = new flash_reader(inputfile);
	my_reader->readRealScalars();
	switch(scalar)
	{
		case 0:
			my_reader->readDens();
			break;
		case 1:
			my_reader->readTemp();
			break;
		case 2:
			my_reader->readKTurb();
			break;
		case 3:
			my_reader->readKFrac();
			break;
		case 4:
			my_reader->readLTurb();
			my_reader->L_cm_to_pc();
			break;
		case 6:
			my_reader->readEint();
			break;
		case 7:case 8:case 9:case 14:case 22:case 24:
			my_reader->readVels();
			break;
		case 10:
			my_reader->readKTurb();
			my_reader->readLTurb();
			//my_reader->readDens();
			break;
		case 11:
			my_reader->readPres();
			break;
		case 12:
			my_reader->readMut();
			break;
		case 13:
			my_reader->readKint();
			break;
		case 15:
			my_reader->readKTurb();
			my_reader->readEint();
			break;
		case 16:
			my_reader->readThFrac();
			break;
		case 17:
			my_reader->readKFrac();
			my_reader->readDens();
			break;
		case 18:
			my_reader->readKFrac();
			my_reader->readDens();
			break;
		case 20:
			break;
		case 21:
			my_reader->readEint();
			my_reader->readTemp();
			break;
		case 23:case 26:
			ifstream clump_file;
			clump_file.open(clumpfile);
			my_reader->readClumps(&clump_file);
			clump_file.close();
			break;
	}
	
	if ( do_deres )
	{
		deres_step = ipow(2,deresfac);
		nderes = ncellx/deresfac;
		if ( nderes<0 )
		{
			cout << "Can't deresolve more than blocksize! Blocksize=" << ncellx << endl;
			exit(0);
		}
	}
//	my_reader->readPres();
//	my_reader->readEint();
//	my_reader->readVels();
	my_reader->readRefine();
	
	flash_data = my_reader->getData();
	if ( derevolve && flash_data->dens==NULL )
	{
		my_reader->readDens();
		flash_data = my_reader->getData();
	}
	my_reader->close();
	
	cout << "Time=" << flash_data->time << " s dt=" << flash_data->dt << " s\n";
	cout << "Time=" << flash_data->time/31556926. << " yr dt=" << flash_data->dt/31556926. << " yr\n";
	if ( scalar==20 )
	{
		// Only wanted the time - let's quit now!
		exit(0);
	}
	
	n_cell = flash_data->nb * ncellx * ncellx * ncellx;
	cout << "Total cells:" << n_cell << endl;
	
	if ( derevolve )
	{
		unrevolve(flash_data);
	}
	
	cout << "Converting distance to parsecs" << endl;
	my_reader->cm_to_pc();
	
	rot = rot*0.0174532925; // Convert to radians
	
	min_ref = flash_data->lrefine[0];
	max_ref = flash_data->lrefine[0];
	min_size = flash_data->blocksize[0];
	max_size = flash_data->blocksize[0];

	for ( ib = 1 ; ib < flash_data->nb ; ib++ )
	{
		if ( flash_data->lrefine[ib]>max_ref )
		{
			// Highest refinement means smallest size
			max_ref = flash_data->lrefine[ib];
			min_size = flash_data->blocksize[ib*3];
		}
		if ( flash_data->lrefine[ib]<min_ref )
		{
			// Lowest refinement means largest size
			min_ref = flash_data->lrefine[ib];
			max_size = flash_data->blocksize[ib*3];
		}
		
		for ( int idim = 0 ; idim<3 ; idim ++ )
		{
			block_dims[idim][0] = flash_data->coordinates[ib*3+idim]-flash_data->blocksize[ib*3+idim]/2.;
			block_dims[idim][1] = flash_data->coordinates[ib*3+idim]+flash_data->blocksize[ib*3+idim]/2.;
			if ( ib==1 || block_dims[idim][0]<box_dims[idim][0] )
			{
				box_dims[idim][0] = block_dims[idim][0];
			}
			if ( ib==1 || block_dims[idim][1]>box_dims[idim][1] )
			{
				box_dims[idim][1] = block_dims[idim][1];
			}			
		}
	}
	
//	min_size = min_size/ncellx;
//	max_size = max_size/ncellx;
	
	for ( int idim = 0 ; idim<3 ; idim ++ )
	{
		l_cells[idim] = (int)((box_dims[idim][1]-box_dims[idim][0])/min_size+.5)*ncellx; // +0.5 for better rounding
	}
	
		
	cout << "Max/min refinements:" << max_ref << " " << min_ref << endl;
	cout << "Sizes:" << max_size << " " << min_size << endl;
	cout << "Box dims:" << endl;
	for ( int idim = 0 ; idim<3 ; idim ++ )
	{
		cout << box_dims[idim][0] << " " << box_dims[idim][1] << endl;
	}
	
	cout << "Total cells ixjxk:" << l_cells[0] << " " << l_cells[1] << " " << l_cells[2] << endl;

//	cout << "Converting densities to MSun/pc^3" << endl;
//	my_reader->to_msunpc3();
	
	n_cell = flash_data->nb * ncellx * ncellx * ncellx;
	
	double scalar_map[l_cells[cd[0]]][l_cells[cd[1]]];
	double scalar_map2[l_cells[cd[0]]][l_cells[cd[1]]];
	
	//int islice = 0.;
	//slice_min = 0.;
	//slice_max = 7.2e18;
	//slice_step = 3.69e23;
	
	//for ( slice_coord =slice_min ; slice_coord+=slice_step ; slice_coord<slice_max+slice_step )
	//{
		if ( !flatten || scalar==26 )
		{
			for ( int ix = 0 ; ix<l_cells[cd[0]] ; ix ++ )
			{
				for ( int iy = 0 ; iy<l_cells[cd[1]] ; iy++ )
				{
					scalar_map[ix][iy] = -1.;
				}
			}
		}
		else // i.e if ( flatten)
		{
			if ( scalar==0 )
			{
				for ( int ix = 0 ; ix<l_cells[cd[0]] ; ix ++ )
				{
					for ( int iy = 0 ; iy<l_cells[cd[1]] ; iy++ )
					{
						scalar_map[ix][iy] = 0.;
					}
				}
			}
			else if ( scalar==1 )
			{
				for ( int ix = 0 ; ix<l_cells[cd[0]] ; ix ++ )
				{
					for ( int iy = 0 ; iy<l_cells[cd[1]] ; iy++ )
					{
						scalar_map[ix][iy] = 1.e20;
					}
				}
			}
		}
	
		if ( vecplot )
		{
			for ( int ix = 0 ; ix<l_cells[cd[0]] ; ix ++ )
			{
				for ( int iy = 0 ; iy<l_cells[cd[1]] ; iy++ )
				{
					scalar_map2[ix][iy] = -1.;
				}
			}
		}
		
		cout << "Taking slice" << endl;
	
		for ( ib = 0 ; ib < flash_data->nb ; ib++ )
	//	ib = 69;
		{
			// Only leaf nodes
			if ( flash_data->nodetype[ib]==1 )
			{
				cell_w = flash_data->blocksize[ib*3]/ncellx;
				for ( ijk[0] = 0 ; ijk[0]<ncellx ; ijk[0]++ )
				{
					for ( ijk[1] = 0 ; ijk[1]<ncellx ; ijk[1]++ )
					{
						for ( ijk[2] = 0 ; ijk[2]<ncellx ; ijk[2]++ )
						{
							icb = ib*(ncellx*ncellx*ncellx)+ijk[2]*(ncellx*ncellx)+ijk[1]*ncellx+ijk[0];
	
							cell_r[0] = flash_data->coordinates[ib*3]+(ijk[0]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
							cell_r[1] = flash_data->coordinates[ib*3+1]+(ijk[1]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
							cell_r[2] = flash_data->coordinates[ib*3+2]+(ijk[2]-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
							
							//cell_r[slice_dim] = unrot_r[slice_dim];
							
							//cell_r[ir0] = unrot_r[ir0] * cos(rot) - unrot_r[ir1] * sin(rot);
							//cell_r[ir1] = unrot_r[ir0] * sin(rot) + unrot_r[ir1] * cos(rot);
							
	//						if ( slice_y<=x+cell_w/2. && slice_y>=x-cell_w/2. )
							if ( (slice_coord<=cell_r[slice_dim]+cell_w/2. && slice_coord>cell_r[slice_dim]-cell_w/2.) || flatten )
							{
								ref_mult = ipow(2,(max_ref-flash_data->lrefine[ib]));
							
	//							cxi = (int)((cell_r[cd[0]]-cell_w/2.-box_dims[cd[0]][0])/(min_size/ncellx));
								// +0.5 so we round to closest cell, don't just round down
								cxi = (int)(((flash_data->coordinates[ib*3+cd[0]]-flash_data->blocksize[ib*3+cd[0]]/2.-box_dims[cd[0]][0])/min_size)*ncellx+.5)+ijk[cd[0]]*ref_mult;
								cxf = cxi + ref_mult-1;
	//							cyi = (int)((cell_r[cd[1]]-cell_w/2.-box_dims[cd[1]][0])/(min_size/ncellx));
								cyi = (int)(((flash_data->coordinates[ib*3+cd[1]]-flash_data->blocksize[ib*3+cd[1]]/2.-box_dims[cd[1]][0])/min_size)*ncellx+.5)+ijk[cd[1]]*ref_mult;
								cyf = cyi + ref_mult-1;
	
	/*							if ( cxi>=0 && cxi<l_cells[cd[0]] && cyi>=0 && cyi<l_cells[cd[1]])
								{
									scalar_map[cxi][cyi]+=1.;
								}*/
	//							cout << cxi << " " << cyi << endl;
	
	
	//							cxi = (int)(cell_r[0]/min_size);
	//							cyi = (int)(cell_r[2]/min_size);
	//							scalar_map[cxi][cyi] = log(flash_data->dens[icb])/log(10.);
								for ( cx = cxi ; cx<=cxf ; cx++ )
								{
									for ( cy = cyi ; cy<=cyf ; cy++ )
									{
										switch(scalar)
										{
											case 0:
												if ( flatten )
												{
													scalar_map[cx][cy] += flash_data->dens[icb]*cell_w;
												}
												else
												{
													scalar_map[cx][cy] = flash_data->dens[icb];
												}
												break;
											case 1:
												if ( flatten )
												{
													scalar_map[cx][cy] = min(scalar_map[cx][cy],flash_data->temp[icb]);
												}
												else
												{
													scalar_map[cx][cy] = flash_data->temp[icb];
												}
												break;
											case 2:
												scalar_map[cx][cy] = flash_data->kturb[icb];
												break;
											case 3:
												scalar_map[cx][cy] = flash_data->kfrac[icb];
												break;
											case 4:
												scalar_map[cx][cy] = flash_data->lturb[icb];
												break;
											case 5:
												scalar_map[cx][cy] = flash_data->lrefine[ib];
												break;
											case 6:
												scalar_map[cx][cy] = flash_data->eint[icb];
												break;
											case 7:
												scalar_map[cx][cy] = flash_data->velx[icb];
												break;
											case 8:
												scalar_map[cx][cy] = flash_data->vely[icb];
												break;
											case 9:
												scalar_map[cx][cy] = flash_data->velz[icb];
												break;
											case 10:
												scalar_map[cx][cy] = 1.0 * flash_data->lturb[icb] * sqrt(2. * flash_data->kturb[icb]);
												break;
											case 11:
												scalar_map[cx][cy] = flash_data->pres[icb];
												break;
											case 12:
												scalar_map[cx][cy] = flash_data->mut[icb];
												break;
											case 13:
												scalar_map[cx][cy] = flash_data->kint[icb];
												break;
											case 14:
												switch(cd[0])
												{
													case 0:
														scalar_map[cx][cy] = flash_data->velx[icb];
														break;
													case 1:
														scalar_map[cx][cy] = flash_data->vely[icb];
														break;
													case 2:
														scalar_map[cx][cy] = flash_data->velz[icb];
														break;
												}
												switch(cd[1])
												{
													case 0:
														scalar_map2[cx][cy] = flash_data->velx[icb];
														break;
													case 1:
														scalar_map2[cx][cy] = flash_data->vely[icb];
														break;
													case 2:
														scalar_map2[cx][cy] = flash_data->velz[icb];
														break;
												}
												// Convert to km/s
												scalar_map[cx][cy]/=1.e5;
												scalar_map2[cx][cy]/=1.e5;
												break;
											case 15:
												scalar_map[cx][cy] = flash_data->eint[icb]-flash_data->kturb[icb];
												break;
											case 16:
												scalar_map[cx][cy] = flash_data->thfrac[icb];
												break;
											case 17:
												sdev = sqrt(log(1+turb_b*turb_b*(3./sqrt(5.))*(flash_data->kfrac[icb]/(1-flash_data->kfrac[icb]))));
												scalar_map[cx][cy] = 0.5 * (1.-erf(log(sodium_dens_cut/flash_data->dens[icb])/(sdev*sqrt(2.))));
												break;
											case 18:
												sdev = sqrt(log(1+turb_b*turb_b*(3./sqrt(5.))*(flash_data->kfrac[icb]/(1-flash_data->kfrac[icb]))));
												scalar_map[cx][cy] = flash_data->dens[icb] * 0.5 * (1.-erf(log(sodium_dens_cut/flash_data->dens[icb])/(sdev*sqrt(2.))));
												break;
											case 21:
												scalar_map[cx][cy] = flash_data->eint[icb]*(0.592*(5./3.-1))/(8.3145119843E7)/flash_data->temp[icb];
												break;
											case 22:
												rad=sqrt(cell_r[0]*cell_r[0]+cell_r[2]*cell_r[2]);
												//vtot = pow(flash_data->velx[icb],2)+pow(flash_data->vely[icb],2)+pow(flash_data->velz[icb],2);
												//vtot = pow(flash_data->velx[icb],2)+pow(flash_data->velz[icb],2);
												//vtot = sqrt(vtot);
												vtot = 1.;
												scalar_map[cx][cy] = (cell_r[0]*flash_data->velz[icb]-cell_r[2]*flash_data->velx[icb])/rad/vtot;
												break;
											case 23:
												scalar_map[cx][cy] = flash_data->iclump[icb];
												break;
											case 24:
												double v1,v2;
												switch(cd[0])
												{
													case 0:
														v1 = flash_data->velx[icb];
														break;
													case 1:
														v1 = flash_data->vely[icb];
														break;
													case 2:
														v1 = flash_data->velz[icb];
														break;
												}
												switch(cd[1])
												{
													case 0:
														v2 = flash_data->velx[icb];
														break;
													case 1:
														v2 = flash_data->vely[icb];
														break;
													case 2:
														v2 = flash_data->velz[icb];
														break;
												}
												scalar_map[cx][cy] = sqrt(pow(v1,2)+pow(v2,2))/1.e5;
												break;
											case 25:
												scalar_map[cx][cy] = cell_r[1];
												scalar_map2[cx][cy] = cell_w;
												break;
											case 26:
												if ( flash_data->iclump[icb]>=0 )
												{
														scalar_map[cx][cy] = flash_data->iclump[icb];
												}
												break;
										}
	/*									if ( cx>=0 && cx<l_cells[cd[0]] && cy>=0 && cy<l_cells[cd[1]])
										{
											scalar_map[cx][cy] += 1;
										}*/
									}
								}
							}
						}
					}
				}
			}
		}
		
		cout << "Sliced!" << endl;
		
		if ( smooth>0 )
		{
			// Apply a smoothing kernel! This would actually work faster by using the convolution theorem
			cout << "Smoothing!" << endl;
			
			// Start with a top hot Kernel
			double smooth_val;
			double smooth_weight;
			double this_dist2,this_weight;
			double smooth2 = smooth*smooth;
			double next_map[l_cells[cd[0]]][l_cells[cd[1]]];
			
			for ( int ix = 0 ; ix<l_cells[cd[0]] ; ix++ )
			{
				for ( int iy = 0 ; iy<l_cells[cd[1]] ; iy++ )
				{
					smooth_val = 0.;
					smooth_weight = 0.;
					for ( int jx = max(ix-smooth*2,0) ; jx<min(ix+smooth*2,l_cells[cd[0]]) ; jx++ )
					{
						for ( int jy = max(iy-smooth*2,0) ; jy<min(iy+smooth*2,l_cells[cd[1]]) ; jy++ )
						{
							this_dist2 = (jx-ix)*(jx-ix)+(jy-iy)*(jy-iy);
	
							// Top hat
							if ( this_dist2<=smooth2 )
							{
								this_weight = 1.;
							}
							else
							{
								this_weight = 0.;
							}
	
							smooth_val += scalar_map[jx][jy]*this_weight;
							smooth_weight += this_weight;
						}
					}
					next_map[ix][iy] = smooth_val/smooth_weight;
				}
			}
			
			for ( int ix = 0 ; ix<l_cells[cd[0]] ; ix++ )
			{
				for ( int iy = 0 ; iy<l_cells[cd[1]] ; iy++ )
				{
					scalar_map[ix][iy] = next_map[ix][iy];
				}
			}
			
		}
		
		cout << "Dumping" << endl;
		
		ofstream slice_f;
	//	slice_f.open("slice_map.dat");
		//if ( slice_max!=slice_min )
		//{
			//sprintf(outfile,"outanim%04d.dat",islice);
		//}
		slice_f.open(outfile);
		for ( int ix = 0 ; ix<l_cells[cd[0]] ; ix += step )
		{
			for ( int iy = 0 ; iy<l_cells[cd[1]] ; iy+= step )
			{
				if ( !do_deres || (ix%deres_step==0 && iy%deres_step==0 ) )
				{
					if ( do_deres )
					{
						double ave_val = 0.;
						for ( int ax = ix ; ax<ix+deres_step ; ax++ )
						{
							for ( int ay = iy ; ay<iy+deres_step ; ay++ )
							{
								ave_val = ave_val + scalar_map[ax][ay];
							}
						}
						ave_val = ave_val/(deres_step*deres_step);
						scalar_map[ix][iy] = ave_val;
					}
					if ( dolog )
					{
						if ( scalar_map[ix][iy]>0. )
						{
							plotval = max(log10(scalar_map[ix][iy]),logtrunc);
							if ( vecplot )
							{
								plotval2 = max(log10(scalar_map2[ix][iy]),logtrunc);
							}
						}
						else
						{
							plotval = logtrunc;
							if ( vecplot )
							{
								plotval2 = logtrunc;
							}
						}
					} else
					{
						plotval = scalar_map[ix][iy];
						if ( vecplot )
						{
							plotval2 = scalar_map2[ix][iy];
						}
					}
					if ( vecplot )
					{
						slice_f << ix*min_size/ncellx + box_dims[cd[0]][0] << " " << iy*min_size/ncellx + box_dims[cd[1]][0] << " " << plotval << " " << plotval2 << endl;
					}
					else
					{
						slice_f << ix*min_size/ncellx + box_dims[cd[0]][0] << " " << iy*min_size/ncellx + box_dims[cd[1]][0] << " " << plotval << endl;
					}
				}
	//			slice_f << ix << " " << iy << " " << scalar_map[ix][iy] << endl;
			}
			if ( !vecplot && (!do_deres || (ix%deres_step==0)) )
			{
				slice_f << endl;
			}
		}
			
		slice_f.close();
		//islice++;
	//}
	
	cout << "Dumped and closing!" << endl;

}
