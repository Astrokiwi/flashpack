#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

typedef struct t_rad_index {
	double rad;
	int index;
};


typedef struct t_bin {
	double mass;
	double dens;
	double temp;
	double rad;
	double vol;
};

int compare_structs (const void *a, const void *b);

void writebox(ofstream *f, double cent[3], double dx[3]);

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data,*flash_data2;
//	char* inputfile = "/disc42/williams/flash/galreal/ULIRG_0016_hdf5_chk_0010";
	char* inputfile = argv[1];
	int ib = atoi(argv[2]);
//	char* inputfile2 = argv[2];

	double r0[3],dx[3];
	double ri[3],dxi[3];
	
//	double r_test[3] = {0.5,0.5,0.5};
//	double dx_test[3] = {0.5,0.6,0.7};
	
	int n_bins = 50;
	
	int inb;
	
	ofstream f;
	
	my_reader = new flash_reader(inputfile);
	my_reader->readRefine();
	flash_data = my_reader->getData();
	my_reader->close();
	
	f.open("cubes.out");
	
//	writebox(&f,r_test,dx_test);
	
	//for ( int ib = 192 ; ib<=192 ; ib ++ )
	//{
		for ( int ii=0 ; ii<3 ; ii++ )
		{
			r0[ii] = flash_data->coordinates[ib*3+ii];
			dx[ii] = flash_data->blocksize[ib*3+ii];
			ri[ii] = 0.;
		}
		writebox(&f,ri,dx);
		for ( int dir = 0 ; dir<15 ; dir ++ )
		{
			inb = flash_data->gid[ib*15+dir] - 1; // Fortran to C numbering
			if ( inb>0 )
			{
//				cout << inb << " ";
				for ( int ii=0 ; ii<3 ; ii++ )
				{
//					cout << (flash_data->coordinates[inb*3+ii]-r0[ii])/dx[ii] << " ";
					ri[ii] = flash_data->coordinates[inb*3+ii] - r0[ii];
					dxi[ii] = flash_data->blocksize[inb*3+ii];
//					cout << ri[ii] << " " << dxi[ii] << " ";
				}
//				cout << flash_data->lrefine[ib]-flash_data->lrefine[inb] << endl;
				writebox(&f,ri,dxi);
			}
			else
			{
				//cout << inb << endl;
			}
		}
	//}
	
	f.close();

}

void writebox(ofstream *f, double cent[3], double dx[3])
{
	double z;
	// Two faces
	for ( int face = -1 ; face<=1 ; face+=2 )
	{
		z = cent[2]+face*dx[2]/2.;
		*f << cent[0]-dx[0]/2. << " " << cent[1]-dx[1]/2. << " " << z << endl;
		*f << cent[0]-dx[0]/2. << " " << cent[1]+dx[1]/2. << " " << z << endl;
		*f << cent[0]+dx[0]/2. << " " << cent[1]+dx[1]/2. << " " << z << endl;
		*f << cent[0]+dx[0]/2. << " " << cent[1]-dx[1]/2. << " " << z << endl;
		*f << cent[0]-dx[0]/2. << " " << cent[1]-dx[1]/2. << " " << z << endl;
		*f << endl << endl;
	}
	
	// "Rods" inbetween
	for ( int fx = -1 ; fx<=1 ; fx+=2 )
	{
		for ( int fy = -1 ; fy<=1 ; fy+=2 )
		{
			*f << cent[0]+fx*dx[0]/2. << " " << cent[1]+fy*dx[1]/2. << " " << cent[2]-dx[2]/2. << endl;
			*f << cent[0]+fx*dx[0]/2. << " " << cent[1]+fy*dx[1]/2. << " " << cent[2]+dx[2]/2. << endl;
			*f << endl << endl;
		}
	}
}
