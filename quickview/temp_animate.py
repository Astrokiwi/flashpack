#!/bin/python
# Python script to convert a bunch of profiles with gnuplot into a bunch of images

import sys
import os

# What iterations to get the work on
start = int(sys.argv[2])
finish = int(sys.argv[3])

# The run number
runnum = int(sys.argv[1])

for x in range(start,finish+1,1):
	outfilename = "movie_dat/temp_map"+str(runnum).rjust(4,"0")+"_"+str(x).rjust(4,"0")+".dat"
 	infilename = "/gs/scratch/dwilliam/flash/"+str(runnum).rjust(4,"0")+"/ULIRG_"+str(runnum).rjust(4,"0")+"_hdf5_chk_"+str(x).rjust(4,"0")
#	infilename = "/disc42/williams/flash/galreal/"+str(runnum).rjust(4,"0")+"/ULIRG_"+str(runnum).rjust(4,"0")+"_hdf5_plt_cnt_"+str(x).rjust(4,"0")
	execflags = "-x 0. -temp"
#	execflags = "-x 0. -tempmin"
	out_exec = "./sliceplot -f "+infilename+" "+execflags+" -o "+outfilename
	os.system(out_exec)

command = "convert -delay 16 "

#print "Converting profiles to graphs with gnuplot"
for x in range(start,finish+1,1):
	filename = "movie_dat/temp_map"+str(runnum).rjust(4,"0")+"_"+str(x).rjust(4,"0")+".dat"
	print str(((x-start)*100.)/(finish-start+1))+"% of images made"
	f = open("gnuplotscript1",'w')
#	f.write("set term postscript color\n") 
	f.write("set term png enhanced size 600,600\n") 
#	f.write("set output '/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps'\n")
	f.write("set output 'movie_pic/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png'\n")
	command+="movie_pic/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png "
	
	f.write("set title '"+filename+"'\n")
	f.write("set pm3d map corners2color c1\n")
#	f.write("set cbrange [3.5:9.5]\n") # temperature
	f.write("set cbrange [2.:6.5]\n") # temperature
	f.write("set xrange[-120:120];set yrange[-120:120]\n") # Large galaxy box
	f.write("set xlabel 'kpc';set ylabel 'kpc'\n") # Large galaxy box
	f.write("set cblabel 'K'\n")
	f.write("splot '"+filename+"' u ($2/1.e3):($1/1.e3):3\n")
	f.close()
	os.system("gnuplot gnuplotscript1")
#	os.system("convert /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps -rotate 90 /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png")


print "ps/png files made"
print "Encoding movie"

command+= "movie_out/tempmovie"+str(runnum)+".gif"
#command+= "tempminmovie"+str(runnum)+".gif"
os.system(command)

print "Complete."

