#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
#include <time.h>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
	char* inputfile = argv[1];
	char* outfile = argv[2];
	
	//cout << argc << endl;
	
	//exit();
	
	my_reader = new flash_reader(inputfile);
	my_reader->readDens();
	my_reader->readTemp();
	my_reader->readRefine();
	flash_data = my_reader->getData();
	my_reader->close();
	
	int n_clumpy_block = 0;
	int nclump_cell = 0;

	const int max_block_clump = 100000, n_per_block=ncellx*ncellx*ncellx;
	int block_clump_p[max_block_clump][n_per_block];
	int block_clump_n[max_block_clump];
	int block_clump_ib[max_block_clump];
	
	int clumpy_block[flash_data->nb];
	
	for ( int ii = 0 ; ii<flash_data->nb ; ii++ )
	{
		clumpy_block[ii] = -1;
	}
		
	int iclump[flash_data->nb*(ncellx*ncellx*ncellx)];
	int iclumpll[flash_data->nb*(ncellx*ncellx*ncellx)];

	for ( int cc = 0 ; cc<flash_data->nb*(ncellx*ncellx*ncellx) ; cc++ )
	{
		iclump[cc] = -1;
		iclumpll[cc] = -1;
	}

	cout << "Finding clumpy blocks. Total blocks=" << flash_data->nb << endl;
	
	cout << "***********************************" << endl;
	cout << "*******r CUT IS ACTIVE! ***********" << endl;
	cout << "***********************************" << endl;
	
	
	//cout << "***********************************" << endl;
	//cout << "*****WARNING - NODUMP TEST MODE****" << endl;
	//cout << "***********************************" << endl;
		
	for ( int cc = 0 ; cc<flash_data->nb*(ncellx*ncellx*ncellx) ; cc++ )
	{
		if ( cc%(flash_data->nb*(ncellx*ncellx*ncellx)/10)==0 )
		{
			cout << cc << "/" << flash_data->nb*(ncellx*ncellx*ncellx) << endl;
		}
		bool iscold = false;
		// No turbulence yet
		if ( flash_data->temp[cc]<5.e4 )
		{
			iscold = true;
		}
		if ( flash_data->dens[cc]>1.e-21 ) // Assume dens gas cools quickly & contains Na absorbing gas
		{
			iscold = true;
		}
		
		int ib = cc/(ncellx*ncellx*ncellx);
		
		
		int ixyz1[3];
		double r1[3];
		ixyz1[0] = cc%ncellx;
		ixyz1[1] = ((cc+1)/ncellx)%ncellx;
		ixyz1[2] = ((cc+1)/(ncellx*ncellx))%(ncellx);
		for ( int ii = 0 ; ii<3 ; ii++ )
		{
			r1[ii] = flash_data->coordinates[ib*3+ii] + (flash_data->blocksize[ib*3+ii]*ixyz1[ii])/ncellx;
			r1[ii] /=pc_in_cm;
		}
		
		//if ( 	r1[0]<-20750. || r1[0]>-11900. ||
				//r1[2]<3900.  || r1[2]>13160. ||
				//r1[1]<-25000. || r1[1]>-20000. )
		//{
			//iscold = false;
		//}

		 //cut - REMOVE THIS FOR BIG GALAXY RUNS
		//if ( r1[0]<-50. || r1[0]>50. ||
			 //r1[1]<80. || r1[1]>120. || 
			 //r1[2]<-50. || r1[2]>50.
			 //)
		//{
			//iscold = false;
		//}
		
		//if ( r1[0]<-44. || r1[0]>-32 ||
			 //r1[1]<90. || r1[1]>100. ||
			 //r1[2]<-75. || r1[2]>-55
			 //)
		//{
			//iscold = false;
		//}
		
		if ( flash_data->nodetype[ib]!=1 )
		{
			iscold = false;
		}
		
		if ( iscold )
		{
//			clump_cell_index[nclump_cell] = cc;
			nclump_cell++;
			int ib = cc/(ncellx*ncellx*ncellx);
			if ( clumpy_block[ib]==-1 )
			{
				if ( n_clumpy_block>=max_block_clump )
				{
					cout << "Too many clumpy blocks!" << endl;
					cout << max_block_clump << " " << n_clumpy_block << endl;
					exit(0);
				}
				clumpy_block[ib] = n_clumpy_block;
				block_clump_ib[n_clumpy_block] = ib;
				block_clump_n[n_clumpy_block] = 1;
				block_clump_p[n_clumpy_block][0] = cc;
				n_clumpy_block++;
				//iclump[cc] = 1;
			}
			else
			{
				if ( block_clump_n[clumpy_block[ib]]>=n_per_block )
				{
					cout << "Too many cells per block! (whaaaaat!?)" << endl;
					cout << block_clump_n[clumpy_block[ib]] << " " << n_per_block << endl;
					exit(0);
				}
				block_clump_p[clumpy_block[ib]][block_clump_n[clumpy_block[ib]]] = cc;
				block_clump_n[clumpy_block[ib]]++;
				//iclump[cc] = 1;
			}
		}
	}
	
	//int count_check = 0;
	
	//for ( int clump_block = 0 ; clump_block<n_clumpy_block ; clump_block ++ )
	//{
		//for ( int ic = 0 ; ic<block_clump_n[clump_block] ; ic++)
		//{
			//int cc = block_clump_p[clump_block][ic];
			//if ( iclump[cc]==1 )
			//{
				//count_check++;
			//}
		//}
	//}
	//cout << count_check << endl;


	cout << "nclump_cell = " << nclump_cell << endl;
	cout << "clump_blocks = " << n_clumpy_block << endl;
	
	

	time_t start = time(NULL);
	
	// Let's assign things to clumps!
	const int max_clumps = 100000;
	int clump_n[max_clumps],clumpllss[max_clumps][2];
	int n_clumps = 0;

	for ( int ii = 0 ; ii<max_clumps ; ii++ )
	{
		clump_n[ii] = 0;
		clumpllss[ii][0]=-1;
		clumpllss[ii][1]=-1;
	}
	
	cout << "Connect clumps within blocks" << endl;
	
	// Make assignments within each block
	for ( int clump_block = 0 ; clump_block<n_clumpy_block ; clump_block ++ )
	{
		if ( n_clumpy_block>10 )
		{
			if ( clump_block%(n_clumpy_block/10)==0 )
			{
				cout << clump_block << "/" << n_clumpy_block << endl;
			}
		}
		//cout << "||||||||||||| clump_block=" << clump_block << " " << n_clumpy_block <<  " " << n_clumps << endl;
		if ( block_clump_n[clump_block]>1 )
		{
			for ( int ic = 0 ; ic<block_clump_n[clump_block] ; ic ++ )
			{
				int cc = block_clump_p[clump_block][ic];
				int ix = cc%ncellx;
				int iy = ((cc)/ncellx)%ncellx;
				int iz = ((cc)/(ncellx*ncellx))%ncellx;
				int ccneigh[6];
				for ( int ii = 0 ; ii<6 ; ii++ )
				{
					ccneigh[ii] = -1;
				}
				if ( ix>0 )
				{
					ccneigh[0] = ix-1 + iy*ncellx + iz*ncellx*ncellx + block_clump_ib[clump_block]*ncellx*ncellx*ncellx;
				}
				if ( ix<7 )
				{
					ccneigh[1] = ix+1 + iy*ncellx + iz*ncellx*ncellx + block_clump_ib[clump_block]*ncellx*ncellx*ncellx;
				}
				if ( iy>0 )
				{
					ccneigh[2] = ix + (iy-1)*ncellx + iz*ncellx*ncellx + block_clump_ib[clump_block]*ncellx*ncellx*ncellx;
				}
				if ( iy<7 )
				{
					ccneigh[3] = ix + (iy+1)*ncellx + iz*ncellx*ncellx + block_clump_ib[clump_block]*ncellx*ncellx*ncellx;
				}
				if ( iz>0 )
				{
					ccneigh[4] = ix + iy*ncellx + (iz-1)*ncellx*ncellx + block_clump_ib[clump_block]*ncellx*ncellx*ncellx;
				}
				if ( iz<7 )
				{
					ccneigh[5] = ix + iy*ncellx + (iz+1)*ncellx*ncellx + block_clump_ib[clump_block]*ncellx*ncellx*ncellx;
				}
				//for ( int iface = 0 ; iface<6 ; iface ++ )
				//{
					//cout << ccneigh[ii] << endl;
				//}
				//cout << endl;
				int n_neigh_clump = 0;
				for ( int ic2 = ic+1 ; ic2<block_clump_n[clump_block] ; ic2 ++ )
				{
					bool clump_neighbour = false;
					int cc2 = block_clump_p[clump_block][ic2];
					if ( cc2<0 || cc2>=flash_data->nb*ncellx*ncellx*ncellx )
					{
						cout << "cc2=" << cc2 << " out of range 0," <<flash_data->nb*ncellx*ncellx*ncellx << endl;
						exit(0);
					}
					for ( int iface = 0 ; iface<6 ; iface ++ )
					{
						if ( cc2==ccneigh[iface] )
						{
							if ( !clump_neighbour )
							{
								clump_neighbour = true;
								//cout << "Neighbour!" << cc2 << " " << iface << endl;
							}
							else
							{
								cout << "Can't neighbour on two faces!" << endl;
								for ( int ii = 0 ; ii<6 ; ii++ )
								{
									cout << ccneigh[ii] << " " << cc2 << endl;
								}
								exit(0);
							}
						}
					}
					if ( clump_neighbour )
					{
//						if ( n_neigh_clump==0 )
						if ( iclump[cc]==-1 )
						{
							// This is my first clumpy neighbour - if he doesn't have a clump, we can make one together
							// If he does, then I'll just join him
							//cout << cc2 << " ";
							//cout << iclump[cc2] << endl;
							if ( iclump[cc2]>=0 )
							{
								int myclump = iclump[cc2];
								iclump[cc] = myclump;
								int last_cc = clumpllss[myclump][1];
								iclumpll[last_cc] = cc;
								iclumpll[cc] = -1;
								clumpllss[myclump][1] = cc;
								clump_n[myclump]++;
								//// Sanity check
								//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
								//{
									//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
									//{
										//int cc3 = block_clump_p[iib][ii];
										//if ( iclump[cc3]>=n_clumps )
										//{
											//cout << "Clump has been pulled out of range - one cell joing another group" << endl;
											//cout << cc3 << " " << iclump[cc3] << " " << n_clumps << endl;
											//exit(0);
										//}
									//}
								//}
							}
							else
							{
								iclump[cc] = n_clumps;
								iclump[cc2] = n_clumps;
								clumpllss[n_clumps][0] = cc;
								clumpllss[n_clumps][1] = cc2;
								iclumpll[cc] = cc2;
								iclumpll[cc2] = -1;
								clump_n[n_clumps] = 2;
								n_clumps++;
								//// Sanity check
								//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
								//{
									//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
									//{
										//int cc3 = block_clump_p[iib][ii];
										//if ( iclump[cc3]>=n_clumps )
										//{
											//cout << "Clump has been pulled out of range - two lone cells combining" << endl;
											//cout << cc3 << " " << iclump[cc3] << " " << n_clumps << endl;
											//exit(0);
										//}
									//}
								//}
							}
							n_neigh_clump++;
						}
						else
						{
							// I already have a neighbour! I'm already in a clump! This neighbouring clump must be assimilated into the collective
							// None shall be spared.
							n_neigh_clump++;
							
							
							if ( cc<0 || cc>=flash_data->nb*ncellx*ncellx*ncellx )
							{
								cout << cc << " out of range 0," << flash_data->nb*ncellx*ncellx*ncellx << endl;
								exit(0);
							}
							int myclump;
							myclump = iclump[cc];
							int deadclump = iclump[cc2];
							// Append neighbouring clump onto mine
							// UNLESS we are already in the same clump - then we're okay
							if ( myclump!=deadclump )
							{
								// If neighbour hasn't been clumped yet, then it can join _my_ clump
								if ( deadclump==-1 )
								{
									iclump[cc2] = myclump;
									int last_cc = clumpllss[myclump][1];
									iclumpll[last_cc] = cc2;
									iclumpll[cc2] = -1;
									clumpllss[myclump][1] = cc2;
									clump_n[myclump]++;
									
									
									//// Sanity check
									//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
									//{
										//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
										//{
											//int cc3 = block_clump_p[iib][ii];
											//if ( iclump[cc3]>=n_clumps )
											//{
												//cout << "Clump has been pulled out of range! deadclump==-1" << endl;
												//cout << cc3 << " " << iclump[cc3] << " " << n_clumps << endl;
												//exit(0);
											//}
										//}
									//}
								}
								else
								{
									
									//cout << "Old n_clumps=" << n_clumps << endl;
									//cout << "Joining clumps " << myclump << " (" << clump_n[myclump] << ") and " << deadclump << " (" << clump_n[deadclump] << ")" << endl;
									if ( myclump<0 || deadclump<0 || clump_n[myclump]<=0 || clump_n[deadclump]<=0 )
									{
										cout << "Now that ain't right (within block)" << endl;
										cout << cc << " " << cc2 << endl;
										cout << ic << " " << block_clump_n[clump_block] << endl;
										cout << myclump << " " << deadclump << " " << clump_n[myclump] << " " << clump_n[deadclump] << endl;
										cout << clump_block << endl;
										
										//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
										//{
											//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
											//{
												//int cc3 = block_clump_p[iib][ii];
												////if ( iib==clump_block )
												////{
													////cout << ii << " " << cc3 << " " << iclump[cc3] << endl;
												////}
												//if ( iclump[cc3]==deadclump )
												//{
													//cout << "Found dead clump" << endl;
												//}
												//if ( iclump[cc3]==myclump )
												//{
													//cout << "Found my clump" << endl;
												//}
												////if ( cc2==cc3 )
												////{
													////cout << "Found dead connection" << endl;
												////}
												////if ( cc==cc3 )
												////{
													////cout << "Found my connection" << endl;
												////}
											//}
										//}
										exit(0);
									}
									
									if ( myclump==n_clumps-1 )
									{
										// If I'm the last one, it's cleaner for me and my friends to join the other clump, rather than vice versa
										//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
										//{
											// Only linking within blocks, no need to look over *all* blocks
											//for ( int ii = 0 ; ii<block_clump_n[clump_block] ; ii ++ )
											//{
												//int cc3 = block_clump_p[clump_block][ii];
												//if ( iclump[cc3]==myclump )
												//{
													//iclump[cc3] = deadclump;
												//}
											//}
										//}
										//cout << "Merging " << myclump << " and " << deadclump << " n_clumps=" << n_clumps << endl;
										int cc3 = clumpllss[myclump][0];
										while ( cc3!=-1 )
										{
											iclump[cc3] = deadclump;
											cc3 = iclumpll[cc3];
										}
										
										iclumpll[clumpllss[deadclump][1]] = clumpllss[myclump][0];
										clumpllss[deadclump][1] = clumpllss[myclump][1];
										clumpllss[myclump][0] = -1;
										clumpllss[myclump][1] = -1;
										clump_n[deadclump] = clump_n[myclump]+clump_n[deadclump];
										clump_n[myclump] = -1;
										n_clumps--;
										
										//// Sanity check
										//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
										//{
											//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
											//{
												//int cc3 = block_clump_p[iib][ii];
												//if ( iclump[cc3]==n_clumps )
												//{
													//cout << "Not deleted right! alpha" << endl;
													//cout << iclumpll[cc3] << endl;
													//cout << myclump << endl;
													//for ( int jjb = 0 ; jjb<n_clumpy_block ; jjb ++ )
													//{
														//for ( int jj = 0 ; jj<block_clump_n[jjb] ; jj ++ )
														//{
															//int cc4 = block_clump_p[jjb][jj];
															//if ( iclumpll[cc4]==cc3 )
															//{
																//cout << cc4 << " points to " << cc3 << endl;
																//cout << iclump[cc4] << " " << iclump[cc3] << endl;
															//}
														//}
													//}
													
													//exit(0);
												//}
											//}
										//}
												//if ( iclump[cc3]>=n_clumps )
												//{
													//cout << "Clump has been pulled out of range - combining clumps generic" << endl;
													//cout << cc3 << " " << iclump[cc3] << " " << n_clumps << endl;
													//cout << myclump << " " << deadclump << endl;
													//exit(0);
												//}
											
										
										//// Sanity check
										//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
										//{
											//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
											//{
												//int cc3 = block_clump_p[iib][ii];
												//if ( iclump[cc3]>=n_clumps )
												//{
													//cout << "Clump has been pulled out of range - combining clumps n_clumps-1==myclump" << endl;
													//cout << cc3 << " " << iclump[cc3] << " " << n_clumps << endl;
													//cout << myclump << " " << deadclump << endl;
													//exit(0);
												//}
											//}
										//}
									}
									else
									{
										//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
										//{
											// Only linking within blocks, no need to look over *all* blocks
											//for ( int ii = 0 ; ii<block_clump_n[clump_block] ; ii ++ )
											//{
												//int cc3 = block_clump_p[clump_block][ii];
												//if ( iclump[cc3]==deadclump )
												//{
													//iclump[cc3] = myclump;
												//}
												//else if ( iclump[cc3]==n_clumps-1 && deadclump!=n_clumps-1 )
												//{
													//iclump[cc3] = deadclump;
												//}
											//}
										//}
										//cout << "Merging " << myclump << " and " << deadclump << " n_clumps=" << n_clumps << endl;
										
										int cc3 = clumpllss[deadclump][0];
										while ( cc3!=-1 )
										{
											iclump[cc3] = myclump;
											cc3 = iclumpll[cc3];
										}
										
										if ( deadclump!=n_clumps-1 )
										{
											cc3 = clumpllss[n_clumps-1][0];
											while ( cc3!=-1 )
											{
												iclump[cc3] = deadclump;
												cc3 = iclumpll[cc3];
											}
										}
										
										iclumpll[clumpllss[myclump][1]] = clumpllss[deadclump][0];
										clumpllss[myclump][1] = clumpllss[deadclump][1];
										clumpllss[deadclump][0] = clumpllss[n_clumps-1][0];
										clumpllss[deadclump][1] = clumpllss[n_clumps-1][1];
										clumpllss[n_clumps-1][0] = -1;
										clumpllss[n_clumps-1][1] = -1;
										clump_n[myclump] = clump_n[myclump]+clump_n[deadclump];
										clump_n[deadclump] = clump_n[n_clumps-1];
										clump_n[n_clumps-1] = -1;
										n_clumps--;
										
										//// Sanity check
										//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
										//{
											//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
											//{
												//int cc3 = block_clump_p[iib][ii];
												//if ( iclump[cc3]==n_clumps )
												//{
													//cout << "Not deleted right bravo!" << endl;
													//exit(0);
												//}
												////if ( iclump[cc3]>=n_clumps )
												////{
													////cout << "Clump has been pulled out of range - combining clumps generic" << endl;
													////cout << cc3 << " " << iclump[cc3] << " " << n_clumps << endl;
													////cout << myclump << " " << deadclump << endl;
													////exit(0);
												////}
											//}
										//}
									}
									//cout << "New n_clumps=" << n_clumps << endl;
									
								}
							}
						}
					}
					else // I have no friends: I am a clump unto myself
					{
						// do nothing - a single cell is below the threshold for being a "clump"
					}
				} 
			}
		}
		if ( block_clump_n[clump_block]==1 )
		{
			int cc = block_clump_p[clump_block][0];
			clump_n[n_clumps] = 1;
			iclump[cc] = n_clumps;
			clumpllss[n_clumps][0] = cc;
			clumpllss[n_clumps][1] = cc;
			iclumpll[cc] = -1;
			n_clumps++;
		}
		if ( block_clump_n[clump_block]<=0 )
		{
			cout << "block_clump_n==" << block_clump_n[clump_block] << " when it shouldn't be" << endl;
			exit(0);
		}
	}
	
	cout << "n_clumps=" << n_clumps << endl;
	
	
	//cout << "Dumping everything - first time" << endl;
	//ofstream clump_file0;
	//clump_file0.open("blockgroup.dat");
	//clump_file0 << flash_data->nb*(ncellx*ncellx*ncellx) << " " << n_clumps << endl;
	//for ( int ii = 0 ; ii < flash_data->nb*(ncellx*ncellx*ncellx) ; ii++ )
	//{
		//clump_file0 << iclump[ii] << endl;
	//}
	//clump_file0.close();
	//exit(0);
	
	cout << "Connect blocky clumps to each other" << endl;
	// Now, do it again on a block by block basis
	
	for ( int clump_block = 0 ; clump_block<n_clumpy_block ; clump_block ++ )
	{
		if ( n_clumpy_block>10 )
		{
			if ( clump_block%(n_clumpy_block/10)==0 )
			{
				cout << clump_block << "/" << n_clumpy_block << endl;
			}
		}
		else
		{
				cout << clump_block << "/" << n_clumpy_block << endl;
		}
		//cout << "************* clump_block=" << clump_block << " " << n_clumpy_block <<  " " << n_clumps << endl;
		 //Does this clumpy block neighbour any other clump blocks?
		for ( int clump_block2 = clump_block+1 ; clump_block2<n_clumpy_block ; clump_block2 ++ )
		{
			double dr[3],maxdr[3];
			double dr_norm2,maxdr_norm2;
			bool neighbouring = true;

			//for ( int ii = 0 ; ii<3 ; ii ++ )
			//{
				//dr[ii] = flash_data->coordinates[block_clump_ib[clump_block]*3+ii]-flash_data->coordinates[block_clump_ib[clump_block2]*3+ii];
				//maxdr[ii] = flash_data->blocksize[block_clump_ib[clump_block]*3+ii]+flash_data->blocksize[block_clump_ib[clump_block2]*3+ii];				
				//maxdr[ii]/=1.9; // allow for rounding (should be 2.0)
				////maxdr[ii]/=.5; // allow for rounding (should be 2.0)
				////cout << flash_data->coordinates[block_clump_ib[clump_block]*3+ii] << " " << flash_data->coordinates[block_clump_ib[clump_block2]*3+ii] << " " << dr[ii] << " " << maxdr[ii] << " " << flash_data->blocksize[block_clump_ib[clump_block]*3+ii] << " " << flash_data->blocksize[block_clump_ib[clump_block2]*3+ii] << endl;
				//if ( maxdr[ii]<abs(dr[ii]) )
				//{
					//neighbouring = false;
				//}
			//}
			
			
			// If both same refinement, then just check for adjacent blocks
			if ( flash_data->lrefine[block_clump_ib[clump_block]]==flash_data->lrefine[block_clump_ib[clump_block2]] )
			{
				// Just orthogonally adjacent blocks (assume vaugely square, within 2.0/1.9 ratio)
				maxdr[0] = flash_data->blocksize[block_clump_ib[clump_block]*3]+flash_data->blocksize[block_clump_ib[clump_block2]*3];
				maxdr[0]/=1.9; // allow for rounding (should be 2.0)
				maxdr[1] = 0.;
				maxdr[2] = 0.;
			}
			// If one is at a higher refinement, then range is bigger (four neighbours!)
			else
			{
				// Assume vaguely square (within factor of 2/1.9)
				
				// One long edge
				maxdr[0] = (flash_data->blocksize[block_clump_ib[clump_block]*3]+flash_data->blocksize[block_clump_ib[clump_block2]*3])/1.9;
				
				double minedge = min(flash_data->blocksize[block_clump_ib[clump_block]*3],flash_data->blocksize[block_clump_ib[clump_block2]*3]);
				maxdr[1] = minedge/1.9;
				maxdr[2] = minedge/1.9;
			}
			dr_norm2 = 0.;
			maxdr_norm2 = 0.;
			for ( int ii = 0 ; ii<3 ; ii ++ )
			{
				maxdr_norm2+=maxdr[ii]*maxdr[ii];
				dr[ii] = flash_data->coordinates[block_clump_ib[clump_block]*3+ii]-flash_data->coordinates[block_clump_ib[clump_block2]*3+ii];
				dr_norm2 = dr_norm2 + dr[ii]*dr[ii];
			}
			
			if ( dr_norm2>maxdr_norm2 )
			{
				neighbouring = false;
			}
			
			//cout << "***" << endl;
			if ( neighbouring )
			{
				//cout << "Neighbouring!" << clump_block << " " << clump_block2 << endl;
				//cout << dr[0] << " " << dr[1] << " " << dr[2] << endl;
				//cout << maxdr[0] << " " << maxdr[1] << " " << maxdr[2] << endl;
				
				
				// Compare all the cells!
				
				//// If both same refinement, then just check for adjacent blocks
				//if ( flash_data->lrefine[block_clump_ib[clump_block]]==flash_data->lrefine[block_clump_ib[clump_block2]] )
				//{
					//// Just orthogonally adjacent blocks (assume vaugely square, within 2.0/1.9 ratio)
					//maxdr[0] = flash_data->blocksize[block_clump_ib[clump_block]*3]+flash_data->blocksize[block_clump_ib[clump_block2]*3];
					//maxdr[0]/=1.9; // allow for rounding (should be 2.0)
					//maxdr[1] = 0.;
					//maxdr[2] = 0.;
				//}
				//// If one is at a higher refinement, then range is bigger (four neighbours!)
				//else
				//{
					//// Assume vaguely square (within factor of 2/1.9)
					
					//// One long edge
					//maxdr[0] = (flash_data->blocksize[block_clump_ib[clump_block]*3]+flash_data->blocksize[block_clump_ib[clump_block2]*3])/1.9;
					
					//double minedge = min(flash_data->blocksize[block_clump_ib[clump_block]*3],flash_data->blocksize[block_clump_ib[clump_block2]*3]);
					//maxdr[1] = minedge/1.9;
					//maxdr[2] = minedge/1.9;
				//}
				//maxdr_norm2 = 0.;
				//for ( int ii = 0 ; ii<3 ; ii++ )
				//{
					//maxdr[ii]/=ncellx; // Use this distance to compare betwen cells now (not between blocks)
					////maxdr_norm2+=maxdr[ii]*maxdr[ii];
				//}
				
				maxdr_norm2/=(ncellx*ncellx);
				
				for ( int ic1 = 0 ; ic1<block_clump_n[clump_block] ; ic1 ++ )
				{
					int cc1 = block_clump_p[clump_block][ic1];
					int ixyz1[3];
					double r1[3];
					int n_neigh_clump = 0;
					ixyz1[0] = cc1%ncellx;
					ixyz1[1] = ((cc1)/ncellx)%ncellx;
					ixyz1[2] = ((cc1)/(ncellx*ncellx))%(ncellx);
					if (	ixyz1[0]==0 || ixyz1[0] == 7 ||
							ixyz1[1]==0 || ixyz1[1] == 7 ||
							ixyz1[2]==0 || ixyz1[2] == 7 )
					{
						
						for ( int ii = 0 ; ii<3 ; ii++ )
						{
							r1[ii] = flash_data->coordinates[block_clump_ib[clump_block]*3+ii] + (flash_data->blocksize[block_clump_ib[clump_block]*3+ii]*(ixyz1[ii]-3.5))/ncellx;
						}
						
						for ( int ic2 = 0 ; ic2<block_clump_n[clump_block2] ; ic2 ++ )
						{
							int cc2 = block_clump_p[clump_block2][ic2];
							int ixyz2[3];
							double r2[3];
							ixyz2[0] = (cc2)%ncellx;
							ixyz2[1] = ((cc2)/ncellx)%ncellx;
							ixyz2[2] = ((cc2)/(ncellx*ncellx))%(ncellx);
							bool clump_neighbour = true;
							
							
							if (	ixyz2[0]==0 || ixyz2[0] == 7 ||
									ixyz2[1]==0 || ixyz2[1] == 7 ||
									ixyz2[2]==0 || ixyz2[2] == 7 )
							{
								
								dr_norm2 = 0.;
								for ( int ii = 0 ; ii<3 ; ii++ )
								{
									r2[ii] = flash_data->coordinates[block_clump_ib[clump_block2]*3+ii] + (flash_data->blocksize[block_clump_ib[clump_block2]*3+ii]*(ixyz2[ii]-3.5))/ncellx;
									dr_norm2+=pow(r2[ii]-r1[ii],2);
									//if ( abs(r2[ii]-r1[ii])>maxdr[ii] )
									//{
										//clump_neighbour = false;
									//}
									//cout << r1[ii] << " " << r2[ii] << " " << ixyz1[ii] << " " << ixyz2[ii] << endl;
								}
								if ( dr_norm2>maxdr_norm2 )
								{
									clump_neighbour = false;
								}
								//exit(0);
								
								if ( clump_neighbour )
								{
									// There's a neighbour - let our clumps combine, if they haven't already!
									int myclump = iclump[cc1];
									int deadclump = iclump[cc2];
									
									if ( myclump!=deadclump )
									{
										// If either of us are a single cell, just latch onto the other cell
										if ( deadclump==-1 )
										{ 
											if ( myclump==-1 )
											{
												// We shall forge a new clump, and rule the galaxy as father and son
												iclump[cc1] = n_clumps;
												iclump[cc2] = n_clumps;
												clump_n[n_clumps] = 2;
												clumpllss[n_clumps][0] = cc1;
												clumpllss[n_clumps][1] = cc2;
												iclumpll[cc1] = cc2;
												iclumpll[cc2] = -1;
												n_clumps++;
											}
											else
											{
												iclump[cc2] = myclump;
												clump_n[myclump]++;
												int last_cc = clumpllss[myclump][1];
												iclumpll[last_cc] = cc2;
												iclumpll[cc2] = -1;
												clumpllss[myclump][1] = cc2;
											}
										}
										else if ( myclump==-1 )
										{
											iclump[cc1] = deadclump;
											clump_n[deadclump]++;
											int last_cc = clumpllss[deadclump][1];
											iclumpll[last_cc] = cc1;
											iclumpll[cc1] = -1;
											clumpllss[deadclump][1] = cc1;
										}
										// Otherwise attach the entire other group to my group
										else
										{
											//cout << "Joining clumps " << myclump << " (" << clump_n[myclump] << ") and " << deadclump << " (" << clump_n[deadclump] << ")" << endl;
											if ( myclump<0 || deadclump<0 || clump_n[myclump]<=0 || clump_n[deadclump]<=0 )
											{
												cout << "Joining clumps " << myclump << " (" << clump_n[myclump] << ") and " << deadclump << " (" << clump_n[deadclump] << ")" << endl;
												cout << "Now that ain't right (between blocks)" << endl;
												cout << clump_block << " " << clump_block2 << endl;
												cout << ic1 << " " << ic2 << endl;
												cout << block_clump_n[clump_block] << " " << block_clump_n[clump_block2] << endl;
												cout << cc1 << " " << cc2 << endl;
		
												//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
												//{
													//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
													//{
														//int cc3 = block_clump_p[iib][ii];
														//if ( iclump[cc3]==deadclump )
														//{
															//cout << "Found dead clump" << endl;
														//}
														//if ( cc2==cc3 )
														//{
															//cout << "Found connection" << endl;
														//}
													//}
												//}
												exit(0);
											}
											if ( myclump==n_clumps-1 )
											{
												// If I'm the last one, it's cleaner for me to join the other clump, rather than vice versa
												//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
												//{
													//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
													//{
														//int cc3 = block_clump_p[iib][ii];
														//if ( iclump[cc3]==myclump )
														//{
															//iclump[cc3] = deadclump;
														//}
													//}
												//}
												int cc3 = clumpllss[myclump][0];
												while ( cc3!=-1 )
												{
													iclump[cc3] = deadclump;
													cc3 = iclumpll[cc3];
												}
												
												iclumpll[clumpllss[deadclump][1]] = clumpllss[myclump][0];
												clumpllss[deadclump][1] = clumpllss[myclump][1];
												clumpllss[myclump][0] = -1;
												clumpllss[myclump][1] = -1;
												clump_n[deadclump] = clump_n[myclump]+clump_n[deadclump];
												clump_n[myclump] = -1;
												n_clumps--;
											}
											else
											{
												//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
												//{
													//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
													//{
														//int cc3 = block_clump_p[iib][ii];
														//if ( iclump[cc3]==deadclump )
														//{
															//iclump[cc3] = myclump;
														//}
														//else if ( iclump[cc3]==n_clumps-1 && deadclump!=n_clumps-1 )
														//{
															//iclump[cc3] = deadclump;
														//}
													//}
												//}
												int cc3 = clumpllss[deadclump][0];
												while ( cc3!=-1 )
												{
													iclump[cc3] = myclump;
													cc3 = iclumpll[cc3];
												}
												
												if ( deadclump!=n_clumps-1 )
												{
													cc3 = clumpllss[n_clumps-1][0];
													while ( cc3!=-1 )
													{
														iclump[cc3] = deadclump;
														cc3 = iclumpll[cc3];
													}
												}
												
												iclumpll[clumpllss[myclump][1]] = clumpllss[deadclump][0];
												clumpllss[myclump][1] = clumpllss[deadclump][1];
												clumpllss[deadclump][0] = clumpllss[n_clumps-1][0];
												clumpllss[deadclump][1] = clumpllss[n_clumps-1][1];
												clumpllss[n_clumps-1][0] = -1;
												clumpllss[n_clumps-1][1] = -1;
												clump_n[myclump] = clump_n[myclump]+clump_n[deadclump];
												clump_n[deadclump] = clump_n[n_clumps-1];
												clump_n[n_clumps-1] = -1;
												n_clumps--;
											}
		
											
											//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
											//{
												//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
												//{
													//int cc3 = block_clump_p[iib][ii];
													//if ( iclump[cc3]==deadclump )
													//{
														//iclump[cc3] = myclump;
													//}
													//else if ( iclump[cc3]==n_clumps-1 && deadclump!=n_clumps-1)
													//{
														//iclump[cc3] = deadclump;
													//}
												//}
											//}
											
											//clump_n[myclump] = clump_n[myclump]+clump_n[deadclump];
											//clump_n[deadclump] = clump_n[n_clumps-1];
											//clump_n[n_clumps-1] = -1;
											//n_clumps--;
											//cout << "New n_clumps=" << n_clumps << endl;
											
											// Sanity check
											//for ( int iib = 0 ; iib<n_clumpy_block ; iib ++ )
											//{
												//for ( int ii = 0 ; ii<block_clump_n[iib] ; ii ++ )
												//{
													//int cc3 = block_clump_p[iib][ii];
													//if ( iclump[cc3]>=n_clumps )
													//{
														//cout << "Clump has been pulled out of range!" << endl;
														//cout << cc3 << " " << iclump[cc3] << " " << n_clumps << endl;
													//}
												//}
											//}
										}
									}
								}
							}
						}
					}
				}
			}
			
		}
	}
	
	cout << "Final n_clumps = " << n_clumps << endl;

	cout << "Dumping everything" << endl;

	time_t stop = time(NULL);
	cout << "Time taken = " << stop-start << endl;
	
	//exit(0);

	ofstream clump_file;
	clump_file.open(outfile);
	clump_file << flash_data->nb*(ncellx*ncellx*ncellx) << " " << n_clumps << endl;
	for ( int ii = 0 ; ii < flash_data->nb*(ncellx*ncellx*ncellx) ; ii++ )
	{
		clump_file << iclump[ii] << endl;
	}
	clump_file.close();
	
	cout << "dumped!" << endl;

}

