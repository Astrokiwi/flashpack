#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
#include <string>
#include <iomanip> 
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
	//string inputfile_basis = argv[1];
	if ( argc==1 )
	{
		cout << "Usage:" << endl;
		cout << "./averagestuff (irun) (start) (stop) [step]" << endl;
//		cout << "Loads from /disc42/williams/flash/ismbox_numberedruns/ism_????/ISM_????_hdf5_plt_cnt_????" << endl;
		cout << "Loads from /disc42/williams/flash/colosse_numberedruns/ism_????/ISM_????_hdf5_plt_cnt_????" << endl;
		cout << "Have fun!" << endl;
		exit(0);
	}
	int irun = atoi(argv[1]);
//	int irun = atoi(argv[1]);
	int start_dump = atoi(argv[2]);
	int stop_dump = atoi(argv[3]);
	int step_dump = 1;
	if ( argc>4 )
	{
		step_dump = atoi(argv[4]);
	}
	string inputfile_basis;
	string inputfile;
	string outfile;
	string outfile2;
	//stringstream irun_str;
		
	//irun_str << setw(4) << setfill('0') << irun;
	
	stringstream irun_strs (stringstream::in | stringstream::out);
	stringstream basis_stuff (stringstream::in | stringstream::out);
	irun_strs <<setw(4) << setfill('0') << irun;
//	basis_stuff << "/disc42/williams/flash/ismbox_numberedruns/ism_"<< irun_strs.str() << "/ISM_" << irun_strs.str() << "_hdf5_plt_cnt_";
//	basis_stuff << "/disc42/williams/flash/colosse_numberedruns/ism_"<< irun_strs.str() << "/ISM_" << irun_strs.str() << "_hdf5_plt_cnt_";
	basis_stuff << "/disc42/williams/flash/colosse_numberedruns/ism_"<< irun_strs.str() << "/ISM_" << irun_strs.str() << "_hdf5_chk_";
	inputfile_basis = basis_stuff.str();
	
	stringstream out1_strs (stringstream::in | stringstream::out);
	stringstream out2_strs (stringstream::in | stringstream::out);
	out1_strs << "ismbox_summaries/avedenstemp" << irun_strs.str() << ".dat";
	out2_strs << "ismbox_summaries/importantstuff" << irun_strs.str() << ".dat";
	outfile = out1_strs.str();
	outfile2 = out2_strs.str();
	ofstream f;
	ofstream f2;
	
	

	f.open((char*)outfile.c_str());
	f2.open((char*)outfile2.c_str());
	
	for ( int idump = start_dump ; idump<=stop_dump ; idump+=step_dump )
	{
		stringstream string_end (stringstream::in | stringstream::out);
		string_end << setw(4) << setfill('0') << idump;
//		inputfile="/disc42/williams/flash/galreal/"+irun_str.str()+"/ULIRG_"+irun_str.str()+"_hdf5_chk_"+string_end.str();
		inputfile=inputfile_basis+string_end.str();
		
		cout << inputfile << endl;
		
		my_reader = new flash_reader((char*)inputfile.c_str());
		my_reader->readTemp();
		my_reader->readEint();
		my_reader->readDens();
		my_reader->readVels();
		my_reader->readRealScalars();
		flash_data = my_reader->getData();
		my_reader->close();
	
		double totalvol=0.;
		double tempvol=0.,densvol=0.,kmass=0.,coldmass=0.,eintmass=0.;
		double cell_vol;
		double tempmass=0.;
		
		for ( int ib=0; ib<flash_data->nb; ib++ )
		{
			//cout << ib << "/" << flash_data->nb << endl;
			// Only do leaf blocks
			if ( flash_data->nodetype[ib]==1 )
			{
				cell_vol = (flash_data->blocksize[ib*3]*flash_data->blocksize[ib*3+1]*flash_data->blocksize[ib*3+2])/(ncellx*ncelly*ncellz);
				//cell_vol = pow(flash_data->blocksize[ib*3]/ncellx,3);
				for ( int ix = 0 ; ix<ncellx ; ix++ )
				{
					for ( int iy = 0 ; iy<ncelly ; iy++ )
					{
						for ( int iz = 0 ; iz<ncellz ; iz++ )
						{
							int ic = ix + iy*ncellx + iz*ncellx*ncelly;
							
							int icb = ib*(ncellx*ncelly*ncellz) + ic;
							
							totalvol += cell_vol;
							tempvol += flash_data->temp[icb]*cell_vol;
							tempmass += flash_data->temp[icb]*flash_data->dens[icb]*cell_vol;
//							eintmass += flash_data->eint[icb]*cell_vol*flash_data->dens[icb];
							eintmass += flash_data->eint[icb]*cell_vol*flash_data->dens[icb];
							densvol += flash_data->dens[icb]*cell_vol;
							kmass += (pow(flash_data->velx[icb],2)+pow(flash_data->vely[icb],2)+pow(flash_data->velz[icb],2))*cell_vol*flash_data->dens[icb]/2.;
							if ( flash_data->temp[icb]<5.e4 )
							{
								coldmass += flash_data->dens[icb]*cell_vol;
							}
						}
					}
				}
			}
		}
		
		kmass/=densvol; // Divide by total muss, Jon Snuuuu (erg/g)
		eintmass/=densvol;
		tempmass/=densvol;
		
		//tempmass *=(1.5)*(138064880); // multiply by 3/2 k_b/mu -> converting into erg/g. mu = 1.e-24 g.
		
		tempvol/=totalvol;
		densvol/=totalvol;
		
		double temprms = 0.,densrms=0.;

		for ( int ib=0; ib<flash_data->nb; ib++ )
		{
			// Only do leaf blocks
			if ( flash_data->nodetype[ib]==1 )
			{
				cell_vol = (flash_data->blocksize[ib*3]*flash_data->blocksize[ib*3+1]*flash_data->blocksize[ib*3+2])/(ncellx*ncelly*ncellz);
				//cell_vol = pow(flash_data->blocksize[ib*3]/ncellx,3);
				for ( int ix = 0 ; ix<ncellx ; ix++ )
				{
					for ( int iy = 0 ; iy<ncelly ; iy++ )
					{
						for ( int iz = 0 ; iz<ncellz ; iz++ )
						{
							int ic = ix + iy*ncellx + iz*ncellx*ncelly;
							
							int icb = ib*(ncellx*ncelly*ncellz) + ic;
							
							temprms += pow(flash_data->temp[icb]-tempvol,2)*cell_vol;
							densrms += pow(flash_data->dens[icb]-densvol,2)*cell_vol;
						}
					}
				}
			}
		}
		
		temprms/=totalvol;
		temprms = sqrt(temprms);
		densrms/=totalvol;
		densrms = sqrt(densrms);
				
		f << flash_data->time/31556926. << " " << tempvol << " " << densvol << " " << temprms << " " << densrms << " " << kmass << " " << coldmass << " " << coldmass/(densvol*totalvol) << " " << eintmass << " " << kmass/(eintmass+kmass) << " " << sqrt(2.*kmass) << endl;
//		f2 << tempvol << " " << kvol/(eintmass+kvol) << " " << coldmass/(densvol*totalvol) << endl;
//                f2 << tempvol << " " << densvol << " " << kvol/(eintmass+kvol) << " " << coldmass/(densvol*totalvol) << endl;
                f2 << tempmass << " " << densvol << " " << kmass/(eintmass+kmass) << " " << coldmass/(densvol*totalvol) << endl;

	}
	
	f2.close();
	f.close();
}
