#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
	char* inputfile = argv[1];
	
	my_reader = new flash_reader(inputfile);
	my_reader->readDens();
	my_reader->readTemp();
	flash_data = my_reader->getData();
	my_reader->close();
	
	int n_clumpy_block = 0;
	int nclump_cell = 0;

	const int max_block_clump = 3000, n_per_block=ncellx*ncellx*ncellx;
	int block_clump_p[max_block_clump][n_per_block];
	int block_clump_n[max_block_clump];
	int block_clump_ib[max_block_clump];
	
	int clumpy_block[flash_data->nb];
	
	for ( int ii = 0 ; ii<flash_data->nb ; ii++ )
	{
		clumpy_block[ii] = -1;
	}
		
	int iclump[flash_data->nb*(ncellx*ncellx*ncellx)];

	for ( int cc = 0 ; cc<flash_data->nb*(ncellx*ncellx*ncellx) ; cc++ )
	{
		iclump[cc] = -1;
	}
		
	for ( int cc = 0 ; cc<flash_data->nb*(ncellx*ncellx*ncellx) ; cc++ )
	{
		bool iscold = false;
		// No turbulence yet
		if ( flash_data->temp[cc]<5.e4 )
		{
			iscold = true;
		}
		if ( flash_data->dens[cc]>1.e-21 ) // Assume dens gas cools quickly & contains Na absorbing gas
		{
			iscold = true;
		}
		if ( iscold )
		{
//			clump_cell_index[nclump_cell] = cc;
			nclump_cell++;
			int ib = cc/(ncellx*ncellx*ncellx);
			if ( clumpy_block[ib]==-1 )
			{
				clumpy_block[ib] = n_clumpy_block;
				block_clump_ib[n_clumpy_block] = ib;
				block_clump_n[n_clumpy_block] = 1;
				block_clump_p[n_clumpy_block][0] = cc;
				n_clumpy_block++;
			}
			else
			{
				block_clump_p[clumpy_block[ib]][block_clump_n[clumpy_block[ib]]] = cc;
				block_clump_n[clumpy_block[ib]]++;
			}
		}
	}
	
	cout << "nclump_cell = " << nclump_cell << endl;
	cout << "clump_blocks = " << n_clumpy_block << endl;
	
	// Let's assign things to clumps!
	const int max_clumps = 20000, max_n_per_clump = 10000;
	int clump_pp[max_clumps][max_n_per_clump];
	int clump_n[max_clumps];
	int n_clumps = 0;

	for ( int ii = 0 ; ii<max_clumps ; ii++ )
	{
		clump_n[ii] = 0;
	}
	
	// Make assignments within each block
	for ( int clump_block = 0 ; clump_block<n_clumpy_block ; clump_block ++ )
	{
		if ( block_clump_n[clump_block]>1 )
		{
			for ( int ic = 0 ; ic<block_clump_n[clump_block] ; ic ++ )
			{
				int cc = block_clump_p[clump_block][ic];
				int ix = cc%ncellx;
				int iy = (cc/ncellx)%ncellx;
				int iz = cc/(ncellx*ncellx);
				int ccneigh[6];
				for ( int ii = 0 ; ii<6 ; ii++ )
				{
					ccneigh[ii] = -1;
				}
				if ( ix>0 )
				{
					ccneigh[0] = ix-1 + iy*ncellx + iz*ncellx*ncellx;
				}
				if ( ix<7 )
				{
					ccneigh[1] = ix+1 + iy*ncellx + iz*ncellx*ncellx;
				}
				if ( iy>0 )
				{
					ccneigh[2] = ix + (iy-1)*ncellx + iz*ncellx*ncellx;
				}
				if ( iy<7 )
				{
					ccneigh[3] = ix + (iy+1)*ncellx + iz*ncellx*ncellx;
				}
				if ( iz>0 )
				{
					ccneigh[4] = ix + iy*ncellx + (iz-1)*ncellx*ncellx;
				}
				if ( iz<7 )
				{
					ccneigh[5] = ix + iy*ncellx + (iz+1)*ncellx*ncellx;
				}
				int n_neigh_clump = 0;
				for ( int ic2 = ic+1 ; ic2<block_clump_n[clump_block] ; ic2 ++ )
				{
					bool clump_neighbour = false;
					int cc2 = block_clump_p[clump_block][ic2];
					if ( cc2<0 || cc2>=flash_data->nb*ncellx*ncellx*ncellx )
					{
						cout << "cc2=" << cc2 << " out of range 0," <<flash_data->nb*ncellx*ncellx*ncellx << endl;
						exit(0);
					}
					for ( int iface = 0 ; iface<6 ; iface ++ )
					{
						if ( cc2==ccneigh[iface] )
						{
							if ( !clump_neighbour )
							{
								clump_neighbour = true;
							}
							else
							{
								cout << "Can't neighbour on two faces!" << endl;
								for ( int ii = 0 ; ii<6 ; ii++ )
								{
									cout << ccneigh[ii] << " " << cc2 << endl;
								}
								exit(0);
							}
						}
					}
					if ( clump_neighbour )
					{
						if ( n_neigh_clump==0 )
						{
							// This is my first clumpy neighbour - if he doesn't have a clump, we can make one together
							// If he does, then I'll just join him
							//cout << cc2 << " ";
							//cout << iclump[cc2] << endl;
							if ( iclump[cc2]>=0 )
							{
								int myclump = iclump[cc2];
								iclump[cc] = myclump;
								clump_pp[myclump][clump_n[myclump]] = cc;
								clump_n[myclump]++;
							}
							else
							{
								iclump[cc] = n_clumps;
								iclump[cc2] = n_clumps;
								clump_pp[n_clumps][0] = cc;
								clump_pp[n_clumps][1] = cc2;
								clump_n[n_clumps] = 2;
								n_clumps++;
							}
							n_neigh_clump++;
						}
						else
						{
							if ( n_neigh_clump<0 )
							{
								cout << "that just ain't right" << endl;
								exit(0);
							}
							// I already have a neighbour! I'm already in a clump! This neighbouring clump must be assimilated into the collective
							// None shall be spared.
							n_neigh_clump++;
							
							
							// Append neighbouring clump onto mine
							if ( cc<0 || cc>=flash_data->nb*ncellx*ncellx*ncellx )
							{
								cout << cc << " out of range 0," << flash_data->nb*ncellx*ncellx*ncellx << endl;
								exit(0);
							}
							int myclump;
							myclump = iclump[cc];
							int deadclump = iclump[cc2];
							// UNLESS we are already in the same clump - then we're okay
							if ( myclump!=deadclump )
							{
								// If neighbour hasn't been clumped yet, then it can join _my_ clump
								
								if ( deadclump==-1 )
								{
									iclump[cc2] = myclump;
									clump_pp[myclump][clump_n[myclump]] = cc2;
									clump_n[myclump]++;
								}
								else
								{
									
									for ( int ii = 0 ; ii<clump_n[deadclump] ; ii ++ )
									{
										clump_pp[myclump][clump_n[myclump]] = clump_pp[deadclump][ii];
										clump_n[myclump]++;
										iclump[clump_pp[deadclump][ii]] = myclump;
									}
									
									// Delete final clump
									n_clumps--;
									if ( deadclump!=n_clumps )
									{
										// Copy the last entry over the dead clump, leaving the dead clump outside of the array
										for ( int ii = 0 ; ii<clump_n[n_clumps] ; ii ++ )
										{
											clump_pp[deadclump][ii] = clump_pp[n_clumps][ii];
											iclump[clump_pp[n_clumps][ii]] = deadclump;
										}
										clump_n[deadclump] = clump_n[n_clumps];
									}
								}
							}
						}
					}
					else // I have no friends: I am a clump unto myself
					{
						// do nothing - a single cell is below the threshold for being a "clump"
					}
				} 
			}
		}
		if ( block_clump_n[clump_block]==1 )
		{
			int cc = block_clump_p[clump_block][0];
			clump_n[n_clumps] = 1;
			iclump[cc] = n_clumps;
			clump_pp[n_clumps][clump_block] = cc;
			n_clumps++;
		}
		if ( block_clump_n[clump_block]<=0 )
		{
			cout << "block_clump_n==" << block_clump_n[clump_block] << " when it shouldn't be" << endl;
			exit(0);
		}
	}
	
	cout << "Connect blocky clumps to each other" << endl;
	// Now, do it again on a block by block basis
	
	for ( int clump_block = 0 ; clump_block<n_clumpy_block ; clump_block ++ )
	{
		// Does this clumpy block neighbour any other clump blocks?
		for ( int clump_block2 = clump_block+1 ; clump_block2<n_clumpy_block ; clump_block2 ++ )
		{
			double dr[3],maxdr[3];
			bool neighbouring = false;
			for ( int ii = 0 ; ii<3 ; ii ++ )
			{
				dr[ii] = flash_data->coordinates[block_clump_ib[clump_block]*3+ii]-flash_data->coordinates[block_clump_ib[clump_block2]*3+ii];
				maxdr[ii] = flash_data->blocksize[block_clump_ib[clump_block]*3+ii]+flash_data->blocksize[block_clump_ib[clump_block2]*3+ii];				
				maxdr[ii]/=1.9; // allow for rounding (should be 2.0)
				if ( maxdr[ii]<=abs(dr[ii]) )
				{
					neighbouring = true;
				}
			}
			if ( neighbouring )
			{
				// Compare all the cells!
				
				for ( int ii = 0 ; ii<3 ; ii++ )
				{
					maxdr[ii]/=ncellx; // Use this distance to compare betwen cells now (not between blocks)
				}
				
				for ( int ic1 = 0 ; ic1<block_clump_n[clump_block] ; ic1 ++ )
				{
					int cc1 = block_clump_p[clump_block][ic1];
					int ixyz1[3];
					double r1[3];
					int n_neigh_clump = 0;
					ixyz1[0] = cc1%ncellx;
					ixyz1[1] = (cc1/ncellx)%ncellx;
					ixyz1[2] = cc1/(ncellx*ncellx);
					
					
					for ( int ii = 0 ; ii<3 ; ii++ )
					{
						r1[ii] = flash_data->coordinates[block_clump_ib[clump_block]*3+ii] + (flash_data->blocksize[block_clump_ib[clump_block]*3+ii]*ixyz1[ii])/ncellx;
					}
					
					for ( int ic2 = 0 ; ic2<block_clump_n[clump_block2] ; ic2 ++ )
					{
						int cc2 = block_clump_p[clump_block2][ic2];
						int ixyz2[3];
						double r2[3];
						ixyz2[0] = cc2%ncellx;
						ixyz2[1] = (cc2/ncellx)%ncellx;
						ixyz2[2] = cc2/(ncellx*ncellx);
						bool clump_neighbour = false;
						
						for ( int ii = 0 ; ii<3 ; ii++ )
						{
							r2[ii] = flash_data->coordinates[block_clump_ib[clump_block2]*3+ii] + (flash_data->blocksize[block_clump_ib[clump_block2]*3+ii]*ixyz2[ii])/ncellx;
							if ( abs(r2[ii]-r1[ii])<maxdr[ii] )
							{
								clump_neighbour = true;
							}
						}
						
						if ( clump_neighbour )
						{
							// There's a neighbour - let our clumps combine, if they haven't already!
							int myclump = iclump[cc1];
							int deadclump = iclump[cc2];
							
							if ( myclump!=deadclump )
							{
								// If either of us are a single cell, just latch onto the other cell
								if ( deadclump==-1 )
								{ 
									if ( myclump==-1 )
									{
										// We shall forge a new clump, and rule the galaxy as father and son
										iclump[cc1] = n_clumps;
										iclump[cc2] = n_clumps;
										clump_pp[n_clumps][0] = cc1;
										clump_pp[n_clumps][1] = cc2;
										clump_n[n_clumps] = 2;
										n_clumps++;
									}
									else
									{
										iclump[cc2] = myclump;
										clump_pp[myclump][clump_n[myclump]] = cc2;
										clump_n[myclump]++;
									}
								}
								else if ( myclump==-1 )
								{
									iclump[cc1] = deadclump;
									clump_pp[deadclump][clump_n[deadclump]] = cc1;
									clump_n[deadclump]++;
								}
								// Otherwise attach the entire other group to my group
								else
								{
									// Append the other clump onto my particle list
									for ( int ii = 0 ; ii<clump_n[deadclump] ; ii ++ )
									{
										clump_pp[myclump][clump_n[myclump]] = clump_pp[deadclump][ii];
										clump_n[myclump]++;
										iclump[clump_pp[deadclump][ii]] = myclump;
									}
									
									// Delete final clump
									n_clumps--;
									if ( deadclump!=n_clumps )
									{
										// Copy the last entry over the dead clump, leaving the dead clump outside of the array
										for ( int ii = 0 ; ii<clump_n[n_clumps] ; ii ++ )
										{
											clump_pp[deadclump][ii] = clump_pp[n_clumps][ii];
											iclump[clump_pp[n_clumps][ii]] = deadclump;
										}
										clump_n[deadclump] = clump_n[n_clumps];
									}
								}
							}
						}
						
					}
				}
			}
			
		}
	}
	
	cout << "Final n_clumps = " << n_clumps << endl;

}

