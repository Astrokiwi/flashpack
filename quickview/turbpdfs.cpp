#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

typedef struct t_rad_index {
	double rad;
	int index;
};


typedef struct t_bin {
	double mass;
	double dens;
	double temp;
	double rad;
	double vol;
};

int compare_structs (const void *a, const void *b);

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
	char* inputfile = argv[1];
	char* outfile = argv[2];
	
	const int n_bins = 200;
	//const double low_dens = 1.e-29,high_dens = 1.e-24;
	const double low_dens = 1.e3,high_dens = 1.e8;
	const double low_temp = 1.e3,high_temp = 1.e8;
	const double low_dens = 1.e3,high_dens = 1.e8;
	double bin_vol[n_bins];
	double bin_mass[n_bins];
	
	my_reader = new flash_reader(inputfile);
	my_reader->readTemp();
	my_reader->readDens();
	my_reader->readKFrac();
	flash_data = my_reader->getData();
	my_reader->close();
	
	int ic,icb,ibin;
	double cell_mass,cell_vol;

	double extra_mass = 0.;
	
	for ( ibin = 0 ; ibin<n_bins ; ibin ++ )
	{
		bin_vol[ibin] = 0.;
		bin_mass[ibin] = 0.;
	}
	
	for ( int ib=0; ib<flash_data->nb; ib++ )
	{
		// Only do leaf blocks
		if ( flash_data->nodetype[ib]==1 )
		{
			cell_vol = flash_data->blocksize[ib*3]*flash_data->blocksize[ib*3+1]*flash_data->blocksize[ib*3+2];
			for ( int ix = 0 ; ix<ncellx ; ix++ )
			{
				for ( int iy = 0 ; iy<ncellx ; iy++ )
				{
					for ( int iz = 0 ; iz<ncellx ; iz++ )
					{
						ic = ix + iy*ncellx + iz*ncellx*ncellx;
						
						icb = ib*(ncellx*ncellx*ncellx) + ic;

						cell_mass = flash_data->dens[icb]*cell_vol;
						
						ibin = (int)floor(((n_bins-1)*log(flash_data->temp[icb]/low_dens))/log(high_dens/low_dens));
						if ( ibin>=0 && ibin<n_bins )
						{
							bin_vol[ibin]+=cell_vol;
							bin_mass[ibin]+=cell_mass;
						}
					}
				}
			}
		}
	}
	
	ofstream outpdf;
	outpdf.open("testpdf.dat");
	for ( ibin = 0 ; ibin<n_bins ; ibin++ )
	{
		outpdf << low_dens*pow(high_dens/low_dens,((double)(ibin))/((double)(n_bins-1))) << " " << bin_vol[ibin] << " " << bin_mass[ibin] << endl;
	}
	outpdf.close();
	
	
}
