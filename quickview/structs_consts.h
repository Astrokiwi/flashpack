#define PI 3.14159265

//int const ncellx = 32; // Each block is 8x8x8 cells
//int const ncelly = 32; // Each block is 8x8x8 cells
//int const ncellz = 64; // Each block is 8x8x8 cells
int const ncellx = 8; // Each block is 8x8x8 cells
int const ncelly = 8; // Each block is 8x8x8 cells
int const ncellz = 8; // Each block is 8x8x8 cells
//int const ncellx = 16; // Each block is 16x16x16 cells

double const pc_in_cm = 3.08568025e18;
double const gcm3_in_Msunpc3 = 1.4771869e22;
double const amu_in_g = 1.66053886e-24;

/////////////

#define MAX_STRING_LENGTH 80 // As defined in FLASH3.0

/////////////

struct real_list_t
{
	char name[MAX_STRING_LENGTH];
	double value;
};

struct sim_info_t 
{
  int file_format_version;
  char setup_call[400];
  char file_creation_time[MAX_STRING_LENGTH];
  char flash_version[MAX_STRING_LENGTH];
  char build_date[MAX_STRING_LENGTH];
  char build_dir[MAX_STRING_LENGTH];
  char build_machine[MAX_STRING_LENGTH];
  char cflags[400];
  char fflags[400];
  char setup_time_stamp[MAX_STRING_LENGTH];
  char build_time_stamp[MAX_STRING_LENGTH];
};

struct flash_data_t {
	int nb;
	int *gid,*nodetype;
	int *iproc;
	double *blocksize,*coordinates;
	double *dens,*temp,*pres;
	double *gamc;
	double *eint,*ener;
	double *kturb,*lturb,*kfrac,*mut,*kint,*thfrac;
	double *velx,*vely,*velz;

	sim_info_t *sim_info;
	real_list_t *real_scalars;
		
	int *lrefine;
	
	int *iclump;
	int n_clumps;
	
	double time,dt;
};

struct dimens_t {
	double rlow[3];
	double rhigh[3];
};
