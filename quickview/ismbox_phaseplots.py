# SO MANY PHASEPLOTS
# nb does not actually do plots, just gets the data
import os
import sys

iruns = [1, 2, 4, 5, 8, 9, 10, 18, 19, 20, 21, 22, 25, 26, 27, 28, 38, 39]
max_itimes = []

for irun in iruns:
	dir = "/disc42/williams/flash/ismbox_numberedruns/ism_"+str(irun).zfill(4)
	max_itime = 0
	for filename in os.listdir(dir):
		if ( "_hdf5_plt_cnt_" in filename ):
			itime = int(filename[-4:])
			if ( itime>max_itime ):
				max_itime = itime
	max_itimes.append(max_itime)

for i in range(0,len(iruns)):
	irun = iruns[i]
	istop = max_itimes[i]
	str_irun = str(irun).zfill(4)
	for itime in range(0,istop+1):
		str_itime = str(itime).zfill(4)
		command = "./flashsummary /disc42/williams/flash/ismbox_numberedruns/ism_"+str_irun+"/ISM_"+str_irun+"_hdf5_plt_cnt_"+str_itime+" phaseplots/ism_"+str_irun+"_"+str_itime+".phase"
		print command
		os.system(command)


