#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
#include <string>
#include <iomanip> 
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
//	string inputfile_basis = argv[1];
	int irun = atoi(argv[1]);
	int stop_dump = atoi(argv[2]);
	string inputfile;
	string outfile;
	stringstream irun_str;
	
	irun_str << setw(4) << setfill('0') << irun;
	
	outfile = "coldmass"+irun_str.str()+".dat";
	ofstream f;
	f.open((char*)outfile.c_str());
		
	for ( int idump = 0 ; idump<=stop_dump ; idump++ )
	{
		stringstream string_end (stringstream::in | stringstream::out);
		string_end << setw(4) << setfill('0') << idump;
		inputfile="/disc42/williams/flash/galreal/"+irun_str.str()+"/ULIRG_"+irun_str.str()+"_hdf5_chk_"+string_end.str();
		
		cout << inputfile << endl;
		
		my_reader = new flash_reader((char*)inputfile.c_str());
		my_reader->readTemp();
		my_reader->readDens();
		my_reader->readRealScalars();
		flash_data = my_reader->getData();
		my_reader->close();
	
		double coldvol=0.,totalvol=0.;
		double coldmass=0.,totalmass=0.;
		double cell_vol,cell_mass;
		
		for ( int ib=0; ib<flash_data->nb; ib++ )
		{
			// Only do leaf blocks
			if ( flash_data->nodetype[ib]==1 )
			{
				cell_vol = (flash_data->blocksize[ib*3]*flash_data->blocksize[ib*3+1]*flash_data->blocksize[ib*3+2])/(ncellx*ncellx*ncellx);
				//cell_vol = pow(flash_data->blocksize[ib*3]/ncellx,3);
				for ( int ix = 0 ; ix<ncellx ; ix++ )
				{
					for ( int iy = 0 ; iy<ncellx ; iy++ )
					{
						for ( int iz = 0 ; iz<ncellx ; iz++ )
						{
							int ic = ix + iy*ncellx + iz*ncellx*ncellx;
							
							int icb = ib*(ncellx*ncellx*ncellx) + ic;
							cell_mass = cell_vol * flash_data->dens[icb];
							
							totalvol += cell_vol;
							totalmass+= cell_mass;
							
							if ( flash_data->temp[icb]<5.e4 || flash_data->dens[icb]>1.e-21 )
							{
								coldvol += cell_vol;
								coldmass+= cell_mass;
							}
						}
					}
				}
			}
		}
		f << flash_data->time/31556926. << " " << coldvol << " " << coldmass << " " << totalvol << " " << totalmass << " " << coldvol/totalvol << " " << coldmass/totalmass << endl;
	}
	
	f.close();
}
