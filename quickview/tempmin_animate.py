#!/bin/python
# Python script to convert a bunch of profiles with gnuplot into a bunch of images

import sys
import os

# What iterations to get the work on
start = int(sys.argv[2])
finish = int(sys.argv[3])

# The run number
runnum = int(sys.argv[1])

dir = sys.argv[4]

print "Slicing in the '"+dir+"' plane"

for x in range(start,finish+1,1):
	outfilename = "/fastscratch/williams/flash_plots/temp_map"+dir+str(runnum).rjust(4,"0")+"_"+str(x).rjust(4,"0")+".dat"
	infilename = "/disc42/williams/flash/galreal/"+str(runnum).rjust(4,"0")+"/ULIRG_"+str(runnum).rjust(4,"0")+"_hdf5_chk_"+str(x).rjust(4,"0")
	execflags = "-"+dir+" 0. -tempmin"
	#execflags = "-x -20000. -dens"
	out_exec = "./sliceplot -f "+infilename+" "+execflags+" -o "+outfilename
	os.system(out_exec)

command = "convert -delay 16 "

#print "Converting profiles to graphs with gnuplot"
for x in range(start,finish+1,1):
	filename = "/fastscratch/williams/flash_plots/temp_map"+dir+str(runnum).rjust(4,"0")+"_"+str(x).rjust(4,"0")+".dat"
	print str(((x-start)*100.)/(finish-start+1))+"% of images made"
	f = open("gnuplotscript",'w')
#	f.write("set term postscript color\n") 
	f.write("set term png enhanced\n") 
#	f.write("set output '/fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps'\n")
	f.write("set output '/fastscratch/williams/tempimg/plot"+dir+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png'\n")
	command+="/fastscratch/williams/tempimg/plot"+dir+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png "
	f.write("set title '"+filename+"'\n")
	f.write("set pm3d map corners2color c1\n")
	#f.write("set cbrange [3.5:9.5]\n") # temperature
	f.write("set cbrange [3.5:7.]\n") # temperature
	f.write("set xlabel 'pc'\n")
	f.write("set ylabel 'pc'\n")
	f.write("set size square\n")
	f.write("set palette defined (3.5 'black', 4.7 'blue', 4.8 'red', 7 'yellow')\n")
	f.write("unset key\n")
	f.write("set cblabel 'T_{min} (K)'\n")
	#f.write("set xrange [-75000:75000];set yrange[-75000:75000]\n") 
	#f.write("set cbrange [-23:-20]\n")
	#f.write("set cbrange [-29:-24]\n") # density
	#f.write("set cbrange [-26:-19]\n") # density central
	#f.write("set xrange[-50000:0];set yrange[-25000:25000]\n")
	f.write("splot '"+filename+"' u 1:2:3\n")
	f.close()
	os.system("gnuplot gnuplotscript")
#	os.system("convert /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".ps -rotate 90 /fastscratch/williams/tempimg/plot"+str(runnum).rjust(4,"0")+str(x).rjust(5,"0")+".png")


print "ps/png files made"
print "Encoding movie"

command+= "tempminmovie"+dir+str(runnum)+".gif"
os.system(command)

command = "rawscp "+"tempminmovie"+dir+str(runnum)+".gif"
os.system(command)

print "Complete."

