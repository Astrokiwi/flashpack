#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
//	char* inputfile = "/disc42/williams/flash/galreal/ULIRG_0003_hdf5_chk_0010";
	char* inputfile = argv[1];
	int i_scal;
	double slice_y = 0.;
	int n_cell;
	double cell_w,x,y,z;
	int ib,ix,iy,iz,icb;
	
	int min_ref,max_ref;
	double min_size,max_size;
	
/*	if ( argc>2 )
	{
		i_scal = 1;
	}
	else
	{
		i_scal = 0;
	}*/
	
	my_reader = new flash_reader(inputfile);
	my_reader->readDens();
	my_reader->readTemp();
	my_reader->readPres();
	my_reader->readEint();
	my_reader->readVels();
	my_reader->readRefine();
	flash_data = my_reader->getData();
	my_reader->close();
	
	n_cell = flash_data->nb * ncellx * ncellx * ncellx;
	cout << "Total cells:" << n_cell << endl;
	
	min_ref = flash_data->lrefine[0];
	max_ref = flash_data->lrefine[0];
	min_size = flash_data->blocksize[0];
	max_size = flash_data->blocksize[0];
	
	for ( ib = 1 ; ib < flash_data->nb ; ib++ )
	{
		if ( flash_data->lrefine[ib]>max_ref )
		{
			max_ref = flash_data->lrefine[ib];
			max_size = flash_data->blocksize[ib*3];
		}
		if ( flash_data->lrefine[ib]<min_ref )
		{
			min_ref = flash_data->lrefine[ib];
			min_size = flash_data->blocksize[ib*3];
		}
	}
	
	cout << "Max/min refinements:" << max_ref << " " << min_ref << endl;
	cout << "Sizes:" << max_size << " " << min_size << endl;

	cout << "Converting distance to parsecs" << endl;
	my_reader->cm_to_pc();
//	cout << "Converting densities to MSun/pc^3" << endl;
//	my_reader->to_msunpc3();
	
	n_cell = flash_data->nb * ncellx * ncellx * ncellx;
	
	cout << "Taking slice" << endl;
	
	ofstream slice_f;
	slice_f.open("slice.dat");
	
	for ( ib = 0 ; ib < flash_data->nb ; ib++ )
//	ib = 69;
	{
		// Only leaf nodes
		if ( flash_data->nodetype[ib]==1 )
		{
			cell_w = flash_data->blocksize[ib*3]/ncellx;
			for ( int ix = 0 ; ix<ncellx ; ix++ )
			{
				for ( int iy = 0 ; iy<ncellx ; iy++ )
				{
					for ( int iz = 0 ; iz<ncellx ; iz++ )
					{
						icb = ib*(ncellx*ncellx*ncellx)+iz*(ncellx*ncellx)+iy*ncellx+ix;

						x = flash_data->coordinates[ib*3]+(ix-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
						y = flash_data->coordinates[ib*3+1]+(iy-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
						z = flash_data->coordinates[ib*3+2]+(iz-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
						
						if ( slice_y<=x+cell_w/2. && slice_y>=x-cell_w/2. )
//						if ( slice_y<=y+cell_w/2. && slice_y>y-cell_w/2. )
						{
							slice_f << ib << " " << flash_data->lrefine[ib] << " " << ix << " " << iy << " " << iz << " " << x << " " << y << " " << z << " " << flash_data->dens[icb] << " " << flash_data->temp[icb] << " " << flash_data->pres[icb] << " " << flash_data->eint[icb] << " " << flash_data->velx[icb] << " " << flash_data->vely[icb] << " " << flash_data->velz[icb] << " " << cell_w << endl;
						}
					}
				}
			}
		}
	}
	
	cout << "Sliced!" << endl;
	
	slice_f.close();

}
