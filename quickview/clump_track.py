import sys
import os
import csv
import math

def import_text(filename, separator):
    for line in csv.reader(open(filename), delimiter=separator, 
                           skipinitialspace=True):
        if line:
            yield line

dt = 5.0e6 # in years, large galaxy time between dumps
#dt = 10.e3 # in years, small disc time between dumps

irun = 153
irunstr = str(irun).rjust(4,'0')

#i_find = 16
for i_find in range(0,156+1,1):
#for i_find in range(0,26+1,1):
#for i_find in [50]:
	print i_find
	f=open("clump_data/clumptrack"+irunstr+"_"+str(i_find)+".dat","w")
	for it in range(20,0,-1):
		#print it
	#for it in range(15,0,-1):
		#print it
		r1 = []
		r2 = []
		v1 = []
		v2 = []
		m1 = []
		m2 = []
		vol1 = []
		vol2 = []
		
		for data in import_text("clump_data/clump_prop_"+irunstr+"_"+str(it-1).rjust(4,"0")+".dat"," "):
			r1 = r1 + [[float(x) for x in data[1:4]]]
			v1 = v1 + [[float(x)*1.02268944e-6 for x in data[4:7]]] # km/s to pc/yr
			m1 = m1 + [float(data[8])]
			vol1 = vol1 + [float(data[9])]
		
		for data in import_text("clump_data/clump_prop_"+irunstr+"_"+str(it).rjust(4,"0")+".dat"," "):
			r2 = r2 + [[float(x) for x in data[1:4]]]
			v2 = v2 + [[float(x)*1.02268944e-6 for x in data[4:7]]]
			m2 = m2 + [float(data[8])]
			vol2 = vol2 + [float(data[9])]
		
		if ( it==20 ):
			f.write(str(it)+" "+str(i_find)+" "+str(i_find)+" 0. "+str(m2[i_find])+" "+str(vol2[i_find])+"\n")
			
		r_predict = []
		#r_predict2 = []
		for jj in range(0,len(m1)):
			r_predict = r_predict + [[0,0,0]]
		
		#for kk in range(0,len(m2)):
			#r_predict2 = r_predict2 + [[0,0,0]]
		
		for jj in range(0,len(m1)):
			for ii in range(0,3):
				r_predict[jj][ii] = (r1[jj][ii])+dt*(v1[jj][ii])
				if ( abs(r_predict[jj][ii])>1.e25 ):
					# hmm, divide by zero-ish?
					r_predict[jj][ii] = 1.e25
					#print r1[jj][0],r1[jj][1],r1[jj][2],v1[jj][0],v1[jj][1],v1[jj][2]
					#print ii,jj
					#sys.exit()

		#for kk in range(0,len(m2)):
			#for ii in range(0,3):
				#r_predict2[kk][ii] = (r2[kk][ii])-dt*(v2[kk][ii])
				#if ( abs(r_predict2[kk][ii])>1.e25 ):
					## hmm, divide by zero-ish?
					#r_predict2[kk][ii] = 1.e25
		
		
		#print r1[0][0],v1[0][0],dt,r1[0][0]+v1[0][0]*dt,r_predict[0][0]
		#print r1[1][0],v1[1][0],dt,r1[1][0]+v1[1][0]*dt,r_predict[1][0]
		
		#sys.exit()
		
		results = []
		
		for jj in range(0,len(m1)):
		#for jj in range(0,2):
			mindr = -1.
			minii = 0
			for kk in range(0,len(m2)):
			#for kk in range(0,len(m1)):
				dr = 0.
				#dv = 0.
				#dm = math.pow(m1[jj]-m2[kk],2)
				for ii in range(0,3):
					#if ( it==2 ):
						#print dr,r_predict[jj][ii],r2[kk][ii]
					if ( abs(r2[kk][ii])>1.e20 ):
						r2[kk][ii] = 1.e20
					#if ( abs(r1[jj][ii])>1.e20 ):
						#r1[jj][ii] = 1.e20
					dr = dr + math.pow(r_predict[jj][ii]-r2[kk][ii],2)
					#dr = dr + math.pow(r_predict2[kk][ii]-r1[jj][ii],2)
					#dv = dv + math.pow(v1[jj][ii]-v2[kk][ii],2)
					#sys.exit()
					#dr = dr + math.pow(r1[jj][ii]-r1[kk][ii],2)
				#print kk,dr,mindr,r_predict[jj],r2[kk]
				#dr = dr*dm*dv
				if ( mindr<0. or mindr>dr ):
					mindr = dr
					minii = kk
			results = results + [[jj,minii,math.sqrt(mindr)]]
		
		result_min = []
		
		for x in results:
			#print x
			fail = False
			for y in results:
				if ( y!=x and y[1]==x[1] and y[2]<x[2] ):
					fail = True
			if ( not fail ):
				result_min = result_min + [x]
				#results.remove(x)
				#print "removed"
			#else:
				#print "removed",x
			#print x
		
		#for x in results:
			#print x
		
		old = -1
		for x in result_min:
			if ( x[1]==i_find ):
	#			print x[0],"to",i_find,"dist=",x[2],"mass=",m2[i_find]
				#print it,x[0],i_find,x[2],m2[i_find]
				#f.write(str(it-1)+" "+str(x[0])+" "+str(i_find)+" "+str(x[2])+" "+str(m2[i_find])+"\n")
				f.write(str(it-1)+" "+str(x[0])+" "+str(i_find)+" "+str(x[2])+" "+str(m1[x[0]])+" "+str(vol1[x[0]])+"\n")
				old = x[0]
		i_find = old
		if ( old==-1 ):
			#print "Not found"
			break
	f.close()

