import sys
import os
import csv
import math

irun = int(sys.argv[1])
istart = int(sys.argv[2])
istop = int(sys.argv[3])
dt = float(sys.argv[4])

fout = open("clump_data/nclump_"+str(irun).rjust(4,'0')+".dat",'w')

for itime in range(istart,istop+1,1):
	fin=open("clump_data/clumps_full_"+str(irun).rjust(4,'0')+"_"+str(itime).rjust(4,'0')+".dat","r")
	nc = (fin.readline().split(" "))[1];
	fout.write(str(itime)+" "+str(itime*dt)+" "+str(int(nc))+"\n")
	fin.close()

fout.close()
