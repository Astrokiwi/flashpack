#include "hdf5.h"
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
#include <limits>
//#include <ctime>
//#include <omp.h>

using namespace std;

#include "structs_consts.h"
#include "flash_reader.h"

///////////

typedef struct t_rad_index {
	double rad;
	int index;
};


typedef struct t_bin {
	double mass;
	double dens;
	double temp;
	double rad;
	double vol;
};

int compare_structs (const void *a, const void *b);

int main(int argc, char* argv[]) {
	flash_reader *my_reader;
	flash_data_t *flash_data;
	int irun = atoi(argv[1]);
	int istart = atoi(argv[2]);
	int istop = atoi(argv[3]);
	char input_file[256];
	char output_file[256];
	
	int ib,ic,icb;
	double x,y,z;
	ofstream shock_f;

	snprintf(output_file,255,"/disc42/williams/flash/summary_plots/shockheight%04d.dat",irun);
	
	shock_f.open(output_file);
	
	for ( int itime = istart ; itime<=istop ; itime++ )
	{
		double grid_maxheight = 0.;
		//double grid_maxdens = 0.;
		snprintf(input_file,255,"/disc42/williams/flash/galreal/%04d/ULIRG_%04d_hdf5_chk_%04d",irun,irun,itime);
		cout << input_file << endl;
		my_reader = new flash_reader(input_file);
		my_reader->readDens();
		my_reader->readRealScalars();
		flash_data = my_reader->getData();
		my_reader->close();
		
		for ( ib=0; ib<flash_data->nb; ib++ )
		{
			// Only do leaf blocks
			if ( flash_data->nodetype[ib]==1 )
			{
				//cell_vol = pow(flash_data->blocksize[ib*3]/ncellx,3);
				for ( int ix = 0 ; ix<ncellx ; ix++ )
				{
					for ( int iy = 0 ; iy<ncellx ; iy++ )
					{
						for ( int iz = 0 ; iz<ncellx ; iz++ )
						{
							ic = ix + iy*ncellx + iz*ncellx*ncellx;
							
							icb = ib*(ncellx*ncellx*ncellx) + ic;

							x = flash_data->coordinates[ib*3]+(ix-ncellx/2+.5)*flash_data->blocksize[ib*3]/ncellx;
							y = flash_data->coordinates[ib*3+1]+(iy-ncellx/2+.5)*flash_data->blocksize[ib*3+1]/ncellx;
							z = flash_data->coordinates[ib*3+2]+(iz-ncellx/2+.5)*flash_data->blocksize[ib*3+2]/ncellx;
							
							if ( flash_data->dens[icb]>1.e-21 && y>grid_maxheight )
							{
								grid_maxheight = y;
							}
						}
					}
				}
			}
		}
		//cout << itime << " " << grid_maxheight/3.08568025e18 << endl;
		shock_f << flash_data->time << " " << itime << " " << grid_maxheight/3.08568025e18 << endl;
	}
	shock_f.close();
}
